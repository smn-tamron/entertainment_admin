@extends('adminlte::page')
@section('content-header-title')
<i class="fa fa-users mr-2"></i>Client Users
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            @if (session('error'))
                <div class="alert alert-danger" role="alert" id="dialog">
                    {{ session('error') }}
                </div>
            @endif
            @if (session('info'))
                <div class="alert alert-success" role="alert" id="dialog">
                    {{ session('info') }}
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <form action="{{ route('client.users.index') }}" name="search-form">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control pull-right" placeholder="Search"
                                        value="{{ request('keyword') }}" autocomplete="off">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default akoneya-search-btn">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <select class="form-control" id="user_type" name="user_type">
                                        <option selected="" value="">Select User Type</option>
                                        @foreach ($userTypes as $as)
                                            <option value="{{ $as['value'] }}" @if (request('user_type') == $as['value']) selected @endif>{{ $as['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <!-- <th class="text-uppercase text-center">No.</th> -->
                                    <th class="text-uppercase text-center">User ID</th>
                                    <th class="text-uppercase text-center">Players</th>
                                    <!-- <th class="text-uppercase text-center">User Type</th> -->
                                    <th class="text-uppercase text-center">Login Type</th>
                                    <th class="text-uppercase text-center">Total Coins Usage</th>
                                    <th class="text-uppercase text-center">Win Rate</th>
                                    <th class="text-uppercase text-center">Device ID</th>
                                    <th class="text-uppercase text-center">Joined Date</th>
                                    <th class="text-uppercase text-center">{{config('enums.statusTableHeader.banStatus')}}</th>
                                    <th class="text-uppercase text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($clientUsers as $key => $clientUser)
                                    <tr>
                                        <!-- <td class="text-center">{{ $clientUsers->firstItem() + $key }}</td> -->
                                        <td class="text-center">{{ $clientUser->id }}</td>
                                        <td class="">
                                            <a href="{{ route('client.users.show', $clientUser) }}" class="">
                                                @if ($clientUser->userName)
                                                    {{ $clientUser->userName }}
                                                @else
                                                    {{ $clientUser->name }}
                                                @endif
                                                <i class="fa fa-link fa-sm ml-2" aria-hidden="true"></i>
                                            </a>
                                        </td>

                                        <!-- @if ($clientUser->user_type == config('enums.guestUser'))
                                            <td class="text-center">{{config('enums.gameUser.guest')}}</td>
                                        @else
                                            <td class="text-center">{{config('enums.gameUser.register')}}</td>
                                        @endif -->
                              
                                        <td class="text-center text-capitalize">
                                            <div class="col d-flex flex-row justify-content-center pt-2 text-bold">
                                                @if ($clientUser->login_type)
                                                    <i class="{{ $clientUser->login_type_label_icon }} pb-2 mb-1"></i>
                                                @else
                                                    <i class="fa fa-secrete pb-2 mb-1"></i>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="col d-flex flex-row justify-content-between pt-2 mx-1 slot-card">
                                                <i class="fa fa-coins fa-sm mx-1"></i>
                                                <p class="text-bold mb-1 mx-2">
                                                    @php
                                                        $winCoinUsage = 0; 
                                                        $comboRewardIds = $clientUser->slotResults->where('win_status',1)->pluck('slot_combo_reward_id');
                                                        foreach ($comboRewardIds as $value):
                                                            $comboReward = $comboRewards->where('id', $value)->first();
                                                            if ($comboReward){
                                                                $betType = $betTypes->where('id', $comboReward->slot_bet_type_id)->first();
                                                                $winCoinUsage += (@$betType->coin_usage ?? 0);
                                                            }
                                                        endforeach;
                                                    @endphp
                                                    {{ number_format(($clientUser->slotResults->where('win_status',0)->sum('amount')) + $winCoinUsage) }}
                                                </p>
                                            </div>
                                        </td>
                                        <td class="text-center text-capitalize">
                                            @php 
                                                $playerResultCount = $clientUser->slotResults->count();
                                                $winCount = $clientUser->slotResults->where('win_status', 1)->count();
                                                $winRate = $playerResultCount == 0 ? 0 : ($winCount / $playerResultCount) * 100;
                                            @endphp
                                            <p class="text-bold mb-1 mx-2">{{ number_format($winRate, 2) }} %</p>
                                        </td>
                                        <td class="text-center">
                                            @php 
                                            $deviceToken = $clientUser->deviceTokens->last() ?? null;
                                            @endphp
                                            {{ $deviceToken ? $deviceToken->device_id : '-' }}
                                        </td>
                                        <td class="text-center">
                                            <small>{{$clientUser->created_at ? diffForHumanFormat($clientUser->created_at) : ''}}</small>
                                            <br>
                                            <span class="text-bold">{{$clientUser->created_at ? clientDateFormat($clientUser->created_at) : ''}}</span>
                                        </td>
                                        <td class="text-center">
                                            <label class="switch ban-status">
                                                @if ($clientUser->banned_status == 1)
                                                    <input type="checkbox" class="togBtn" value="{{ $clientUser->banned_status }}"
                                                        name="status" checked data-id="{{ $clientUser->id }}">
                                                @else
                                                    <input type="checkbox" class="togBtn" value="{{ $clientUser->banned_status }}"
                                                        name="status" data-id="{{ $clientUser->id }}">
                                                @endif
                                                <span class="slider2 round"></span>
                                            </label>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('client.users.show', $clientUser) }}" class="btn btn-sm akoneya-detail-btn">
                                                <i class="fa fa-eye"></i><span class="pl-1"></span>{{config('enums.buttonLabel.viewDetail')}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="pagination">
                            {{ $clientUsers->appends($_GET)->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function() {

            $('#user_type').on('change', function() {
                document.forms["search-form"].submit();
            });

            //active & inactive for Ban Status
            $(".togBtn").on('change', function() {
                var bannedStatus = $(this).prop('checked') == true ? 1 : 0;
                var id = $(this).data('id');

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: "{{ route('client.users.statuschange') }}",
                    data: {
                        'bannedStatus': bannedStatus,
                        'id': id
                    },
                    success: function(data) {
                        toastr.options.closeButton = true;
                        toastr.options.closeMethod = 'fadeOut';
                        toastr.options.fadeOut = 100;
                        toastr.options.fadeIn = 100;
                        toastr.options.closeDuration = 100;
                        toastr.success(data.success);
                    }
                });
            });
        });

    </script>
@stop
