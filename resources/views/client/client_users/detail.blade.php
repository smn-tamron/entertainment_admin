@extends('adminlte::page')
@section('content-header-title')
  <i class="fa fa-user"></i>
  User Profile &nbsp;
  <a class="btn btn-default btn-sm akoneya-edit-btn" href="{{ route('client.users.index') }}">
      <i class="fas fa-reply-all" aria-hidden="true"></i>
      <span class="akoneya-back-btn-text"> Back</span>
  </a>
@stop
@section('content')
    <!-- <div class="row justify-content-start">
      <a class="btn btn-default akoneya-edit-btn ml-3 mb-3" href="{{ route('client.users.index') }}">
          <i class="fas fa-reply-all" aria-hidden="true"></i>
          <span class="akoneya-back-btn-text"> Back</span>
      </a>
    </div> -->
    <div class="row">
      <div class="col-md-6">
        <!-- Profile Image -->
        <div class="card card-outline">
          <div class="card-header">
            <h3 class="card-title mb-0 text-uppercase text-bold"><i class="fa fa-id-card fa-lg mr-2"></i> Personal Information</h3>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-3 d-flex flex-column align-self-center">
                <div class="d-flex flex-row justify-content-center">
                  <div class="text-center box-profile">
                    @if($clientPersonalInformation)
                    <img class="akoneya-client-img" src="{{getImageFromAkoneyaMedia($clientPersonalInformation->image)}}" alt="Profile picture">
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-md-9">
                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item py-2">
                    <span class="text-muted"><i class="fa fa-id-card mr-2"></i>User ID</span> <span class="float-right text-bold">{{$clientUser->id}}</span>
                  </li>
                  <li class="list-group-item py-2">
                    <span class="text-muted"><i class="fa fa-user mr-2"></i>Name</span> <span class="float-right text-bold">{{$clientUser->name}}</span>
                  </li>
                  <li class="list-group-item py-2">
                    @php 
                      $token = $clientUser->deviceTokens ? $clientUser->deviceTokens->last() : null;
                    @endphp
                    <span class="text-muted"><i class="fa fa-mobile mr-2"></i>Device ID</span> <span class="float-right text-bold">{{$token ? $token->device_id : '-'}}</span>
                  </li>
                  <li class="list-group-item py-2">
                    <span class="text-muted"><i class="fa fa-flag mr-2"></i>Login Type</span>
                      <span class="float-right text-bold text-capitalize">
                        @if ($clientUser->user_type == config('enums.guestUser'))
                          {{config('enums.gameUser.guest')}}
                        @else
                          {{$clientUser->login_type}} {{config('enums.gameUser.register')}}
                        @endif
                      </span>
                  </li>
                  @if($clientUser->email)
                    <li class="list-group-item py-2">
                      <span class="text-muted"><i class="fa fa-envelope mr-2"></i>Email</span> <span class="float-right text-bold">{{$clientUser->email}}</span>
                    </li>
                  @endif
                  @if($clientUser->phone)
                    <li class="list-group-item py-2">
                      <span class="text-muted"><i class="fa fa-phone mr-2"></i>Phone</span> <span class="float-right text-bold">{{$clientUser->phone}}</span>
                    </li>
                  @endif
                </ul>
              </div>
            </div>
                <!-- <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#deleteModal" data-id="{{ $clientUser->id }}">
                    <i class="fa fa-trash"></i><span class="pl-1">Delete<span>
                </button> -->
          </div>
              <!-- /.card-body -->
        </div>
            <!-- /.card -->
      </div>

      <div class="col-md-6">
        <!-- About Me Box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title mb-0 text-uppercase text-bold"><i class="fa fa-dice fa-lg mr-2"></i> Player Information</h3>
          </div>

          @if($clientPersonalInformation)
          <!-- /.card-header -->
          <div class="card-body">
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item py-2">
                @php 
                    $playerResultCount = $clientUser->slotResults->count();
                    $winCount = $clientUser->slotResults->where('win_status', 1)->count();
                    $winRate = $playerResultCount == 0 ? 0 : ($winCount / $playerResultCount) * 100;
                @endphp
                <span class="text-muted"><i class="fa fa-id-card mr-2"></i>Win Rate</span> <span class="float-right text-bold">{{ number_format($winRate, 2)}} %</span>
              </li>
              <li class="list-group-item py-2">
                <span class="text-muted"><i class="fa fa-dice mr-2"></i>Total Coin Usage</span>
                <span class="float-right text-bold">
                  @php
                      $winCoinUsage = 0; 
                      $comboRewardIds = $clientUser->slotResults->where('win_status',1)->pluck('slot_combo_reward_id');
                      foreach ($comboRewardIds as $value):
                          $comboReward = $comboRewards->where('id', $value)->first();
                          if ($comboReward){
                              $betType = $betTypes->where('id', $comboReward->slot_bet_type_id)->first();
                              $winCoinUsage += (@$betType->coin_usage ?? 0);
                          }
                      endforeach;
                  @endphp
                  {{number_format(( $clientUser->slotResults->where('win_status',0)->sum('amount') + $winCoinUsage ))}}
                </span>
              </li>
              <li class="list-group-item py-2">
                <span class="text-muted"><i class="fa fa-coins mr-2"></i>Current Coins</span> <span class="float-right text-bold">{{ number_format($clientUser->coin) }}</span>
              </li>
              <li class="list-group-item py-2">
                <span class="text-muted"><i class="fa fa-calendar mr-2"></i>Joined Date</span>
                <span class="float-right text-bold">
                  <span class="text-muted small">({{$clientUser->created_at->diffForHumans()}})</span>
                  {{ $clientUser->created_at ? clientDateFormat($clientUser->created_at) : ''}} 
                </span>
              </li>
              <li class="list-group-item py-2">
                <span class="text-muted"><i class="fa fa-ban mr-2"></i>Ban Status</span>
                  <span class="float-right text-bold text-capitalize">
                    @if ($clientUser->banned_status == 1)
                      Banned <i class="fa fa-ban text-danger"></i>
                    @else
                      Approved <i class="fa fa-check-circle text-success"></i>
                    @endif
                  </span>
              </li>
            </ul>
          </div>
          <!-- /.card-body -->
          @endif
        </div>
        <!-- /.card -->
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="row justify-content-between">
              <div class="d-flex flex-column align-self-center">
                <h3 class="card-title ml-2 mb-0 text-uppercase text-bold"><i class="fa fa-list mr-2"></i> Transaction History</h3>
              </div>
              <form action="{{ route('client.users.show', ['id' => $clientUser->id ]) }}" name="search-form" class="mr-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <input type="text" name="keyword" class="form-control pull-right" placeholder="Search"
                                value="{{ request('keyword') }}" autocomplete="off">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default akoneya-search-btn">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </form>
            </div>
          </div>
          <div class="card-body">
            @if (isset($results))
                <div class="table-responsive">
                    <table class="table table-striped table-bordered task-table">
                        <thead>
                            <tr>
                                <th class="text-center text-uppercase">Transaction ID</th>
                                <th class="text-center text-uppercase">Bet Type</th>
                                <th class="text-center text-uppercase">Win Status</th>
                                <th class="text-center text-uppercase">Player Win Reward</th>
                                <th class="text-center text-uppercase">Batch No</th>
                                <th class="text-center text-uppercase">Time</th>
                                <th class="text-center text-uppercase">Exchange</th>
                                <!-- <th class="text-center text-uppercase">Actions</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($results as $key => $result)
                                <tr>
                                    <td class="text-center">
                                      <p class="mx-2 mb-1 text-bold">#{{ $result->transaction_id }}</p>
                                    </td>
                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                            <i class="fa fa-coins mx-2"></i>
                                            <p class="mx-2 mb-1">
                                              @if ($result->win_status == 1)
                                                {{ $result->coin_usage ?? 'Unknown' }}
                                              @else
                                                {{ $result->amount }}
                                              @endif
                                            </p>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                            <p class="mx-2 mb-1 text-dark">
                                              @if ($result->win_status == 1)
                                                <i class="fa fa-check mx-2"></i>
                                              @else 
                                                <i class="fa fa-times mx-2"></i>
                                              @endif
                                            </p>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card text-danger">
                                            <i class="fa fa-coins mx-2"></i>
                                            <p class="mx-2 mb-1 text-bold">
                                              @if ($result->win_status == 1)
                                                {{ number_format($result->amount - ($result->coin_usage ?? 0)) }}
                                              @else 
                                                -{{ $result->amount }}
                                              @endif
                                            </p>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                            <p class="mx-2 mb-1 text-bold">{{ $result->batch_no }}</p>
                                        </div>
                                        <h5 class="mb-0"></h5>
                                    </td>
                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                            <i class="fa fa-calendar mx-2"></i><p class="mx-2 mb-1">{{ $result->created_at ? clientDateFormat($result->created_at) : ''}}</p>
                                        </div>
                                    </td>

                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card text-info">
                                          <i class="fa fa-gift mx-2"></i>
                                          <p class="mx-2 mb-1 text-bold">
                                            @if ($result->win_status == 1)
                                              {{ number_format($result->amount) }}
                                            @else 
                                              -
                                            @endif
                                          </p>
                                        </div>
                                    </td>
                                    <!-- <td class="text-center">
                                        <a href="{{ route('playerResult.detail', $result->batch_no) }}" class="btn btn-sm akoneya-detail-btn">
                                            <i class="fa fa-eye"></i><span class="pl-1">{{config('enums.buttonLabel.viewDetail')}}</span>
                                        </a>
                                    </td> -->
                                </tr>

                            @endforeach
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                {{ Form::hidden('id', 0) }}
                {!! Form::close() !!}
                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="pagination">
                            {{ $results->appends($_GET)->links() }}
                        </div>
                    </div>
                </div>
            @endif
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-start">
      <a class="btn btn-default akoneya-edit-btn ml-3 mb-3" href="{{ route('client.users.index') }}">
          <i class="fas fa-reply-all" aria-hidden="true"></i>
          <span class="akoneya-back-btn-text"> Back</span>
      </a>
    </div>
@stop

@section('js')
    <script>
        //Delete
        /*$('#deleteModal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            modal = $(this);
            modal.find('.modal-body #item_id').val(id);
        });
        $('#deletebtn').click(function() {
            $('#deleteFrom').submit();
            $('#deleteModal').modal('hide');
        });*/
    </script>
@endsection
