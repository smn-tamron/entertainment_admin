@extends('adminlte::page')
@section('content')
    <div class="content-header">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="text-dark">Permission List</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('permission.index') }}">Permission List</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert" id="dialog">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert" id="errordialog">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="col-12">
                <div class="row">
                    <div class="col-8"></div>

                    <div class="col-4">
                        <button data-toggle="modal" data-target="#createModal" class="btn btn-link role-create"><i
                                class="fa fa-plus-circle" aria-hidden="true"></i><span class="add-new-btn-text">{{config('enums.buttonLabel.addNewText')}}</span></button>
                    </div>
                </div>
            </div>

        </div>

        <div class="card-body">

            <div>

                @if (isset($permissions))
                    <table class="table table-striped table-bordered task-table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($permissions as $key => $item)
                                <tr>
                                    <td>{{ $permissions->firstItem() + $key }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <div class="row">
                                            @can('edit_permission')
                                                <div>
                                                    <button data-toggle="modal" data-target="#editModal"
                                                        data-id="{{ $item->id }}" data-name="{{ $item->name }}"
                                                        class="btn btn-info btn-sm action-btn">
                                                        <i class="fas fa-pencil-alt"></i>Edit
                                                    </button>
                                                </div>

                                            @endcan
                                            @can('delete_permission')
                                                <button data-toggle="modal" data-target="#deleteModal" data-id="{{ $item->id }}"
                                                    class="btn btn-danger btn-sm action-btn">
                                                    <i class="fa fa-trash"> Delete</i>

                                                </button>

                                            @endcan
                                        </div>

                                    </td>
                                    @include('layouts.delete',['id' => $item->id,'route'=>'permission.delete'])
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                    <div style="float: right;">
                        {!! Form::open(['route' => 'permission.delete', 'method' => 'delete', 'id' => 'delete_form']) !!}
                        {{ Form::hidden('id', 0) }}
                        {!! Form::close() !!}
                        {{ $permissions->links() }} <!-- for paginate link -->
                    </div>

                @endif


            </div>


        </div>
    </div>


    <!-- Edit Form -->
    <div class="modal fade" id="editModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Permission</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {!! Form::open(['route' => 'permission.update', 'method' => 'POST', 'id' => 'editForm']) !!}
                    <input type="hidden" name="id" id="id">

                    {{ method_field('patch') }}
                    {{ csrf_field() }}
                    @include('permissions.editform')
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="updateBtn">Update</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div>
    <!-- End Edit Form -->

    <!-- Create Form -->
    <div class="modal fade" id="createModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Permission</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    {!! Form::open(['route' => 'permission.save', 'method' => 'POST', 'id' => 'create_permission']) !!}
                    <div class="form-group">
                        <label for="name">Name</label>
                        {{ Form::text('name', '', ['class' => 'form-control', 'id' => 'name']) }}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveBtn">Save</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div>
    <!-- End Create Form -->


@endsection
@section('js')
    <script>
        // EDIT
        $('#editModal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            modal = $(this);
            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #name').val(name);
        });
        $('#updateBtn').click(function() {
            $('#editForm').submit();
        });

    </script>

    <script>
        // create
        $('#createModal').on('show.bs.modal', function(e) {});
        $('#saveBtn').click(function() {
            $('#create_permission').submit();
        });

    </script>


    <!-- Form Validation -->
    <script>
        jQuery(document).ready(function($) {

            $('#create_permission').validate({ // initialize the plugin
                ignore: [],
                errorElement: "span",
                errorClass: "error-help-block",
                rules: {
                    name: {
                        required: true,
                        maxlength: 100
                    },

                },
            });
            $('#editForm').validate({ // initialize the plugin
                ignore: [],
                errorElement: "span",
                errorClass: "error-help-block",
                rules: {
                    name: {
                        required: true,
                        maxlength: 100
                    },

                },
            });

        });

    </script>
@endsection
