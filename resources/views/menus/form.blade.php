<!-- Create Form -->
<div class="modal fade" id="createModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Menu</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'menus.save', 'method' => 'POST', 'id' => 'createFrom']) !!}
                @csrf
                <div class="form-group">
                    <label for="name">Menu Name</label>
                    {{ Form::text('name', '', ['class' => 'form-control','autocomplete'=>'off']) }}
                </div>
                <div class="form-group">
                    <label for="icon">Icon</label>
                    {{ Form::text('icon', '', ['class' => 'form-control','autocomplete'=>'off']) }}
                </div>
                <div class="form-group">
                    <label for="route">Url</label>
                    {{ Form::text('route', '', ['class' => 'form-control','autocomplete'=>'off']) }}
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn "
                    data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary akoneya-add-btn" id="saveBtn">Save</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>
<!-- End Create Form -->


<!-- Edit Form -->
<div class="modal fade" id="editModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Menu</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'menus.update', 'method' => 'PUT', 'id' => 'editForm']) !!}
                @csrf
                <input type="hidden" name="id" id="update_id">
                <div class="form-group">
                    <label for="name">Menu Name</label>
                    {{ Form::text('name', '', ['class' => 'form-control','autocomplete'=>'off', 'id' => 'update_name']) }}
                </div>
                <div class="form-group">
                    <label for="icon">Icon</label>
                    {{ Form::text('icon', '', ['class' => 'form-control','autocomplete'=>'off', 'id' => 'update_icon']) }}
                </div>
                <div class="form-group">
                    <label for="route">Url</label>
                    {{ Form::text('route', '', ['class' => 'form-control','autocomplete'=>'off', 'id' => 'update_route']) }}
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn "
                    data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary akoneya-add-btn" id="updateBtn">Update</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>
<!-- End Edit Form -->
