@extends('adminlte::page')
@section('content-header-title')
    {{$menu['name']}}'s submenus
@stop
@section('content')
    @if (session('error'))
        <div class="alert alert-danger" role="alert" id="dialog">
            {{ session('error') }}
        </div>
    @endif
    @if (session('info'))
        <div class="alert alert-success" role="alert" id="dialog">
            {{ session('info') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-default akoneya-edit-btn" href="{{ route('menus.index')}}"><i class="fas fa-reply-all"
                        aria-hidden="true"></i>
                    <span class="akoneya-back-btn-text"> Back</span></a>
                </div>
                <div class="col-md-6"> 
                    <button data-toggle="modal" data-target="#createModal" type="button"
                            class="btn btn-link role-create akoneya-add-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>
                            <span class="add-new-btn-text">{{config('enums.buttonLabel.addNewText')}}</span>
                    </button>
                </div>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="table-responsive">
                <table id="submenutable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Submenu</th>
                            <th>Icon</th>
                            <th>Url</th>
                            <th>Sequence Number</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($submenus as $key => $submenu)
                            <tr class="position_id" menuId="{{$submenu->id}}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $submenu->name}}</td>
                                <td>{{ $submenu->icon }}</td>
                                <td>{{ $submenu->route }}</td>
                                <td>{{ $submenu->sequence_number }}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#editModal" 
                                        data-id="{{ $submenu->id }}"
                                        data-icon="{{ $submenu->icon }}" 
                                        data-route="{{ $submenu->route }}" 
                                        data-name="{{ $submenu->name }}" 
                                        class="btn btn-sm action-btn akoneya-edit-btn ">
                                        <i class="fa fa-edit"></i><span class="pl-1">Edit</span>
                                    </button>
                                    <button type="button" class="btn btn-danger btn-sm action-btn" data-toggle="modal"
                                            data-target="#deleteModal" data-id="{{ $submenu->id }}">
                                            <i class="fa fa-trash"></i><span class="pl-1">Delete<span></button>
                                </td>
                                @include('layouts.delete',['id' => $submenu->id,'route'=>'submenus.delete'])
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('menus.submenus.form')
@endsection
@section('js')
<script>
        var fixHelperModified = function(e, button) {
                var $originals = button.children();
                var $helper = button.clone();

                $helper.children().each(function(index) {
                    $(this).width($originals.eq(index).width())
                });
                return $helper;
            },
            updateIndex = function(e, ui) {
                var submenuIds = new Array();
                $('.position_id').each(function() {
                    submenuIds.push($(this).attr("menuId"));
                });
                updateSubmenuTable(submenuIds);
            };

        function updateSubmenuTable(submenuIds) {
            $.ajax({
                url: '{!! route("submenus.sort") !!}',
                type: 'post',
                data: {
                    "submenuIds": submenuIds,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    toastr.success(data.success);
                    setInterval('location.reload()', 1000);
                }
            })
        }

        $("#submenutable tbody").sortable({
            helper: fixHelperModified,
            stop: updateIndex
        }).disableSelection();

        $("tbody").sortable({
            distance: 5,
            delay: 100,
            opacity: 0.6,
            cursor: 'move',
            update: function() {}
        });

        $('#editModal').on('hidden.bs.modal', function () {
            location.reload();
        });
        $('#createModal').on('hidden.bs.modal', function () {
            location.reload();
        });

        jQuery(document).ready(function($) {

            $('#createForm').validate({
                errorElement: "span",
                errorClass: "error-help-block",
                rules: {
                    name: {
                        required: true,
                    },
                    icon: {
                        required: true,
                    },
                    route: {
                        required: true,
                    }
                }
            });
            $('#editForm').validate({
                errorElement: "span",
                errorClass: "error-help-block",
                rules: {
                    name: {
                        required: true,
                    },
                    icon: {
                        required: true,
                    },
                    route: {
                        required: true,
                    }
                }
            });
        });

        $('#saveBtn').click(function() {
            $('#createForm').submit();
        });

        $('#editModal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var icon = button.data('icon');
            var route = button.data('route');
  
            modal = $(this);
            modal.find('.modal-body #update_id').val(id);
            modal.find('.modal-body #update_name').val(name);
            modal.find('.modal-body #update_icon').val(icon);
            modal.find('.modal-body #update_route').val(route);
        });

        $('#updateBtn').click(function() {
            $('#editForm').submit();
        });

        //Delete
        $('#deleteModal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            modal = $(this);
            modal.find('.modal-body #item_id').val(id);
        });
        $('#deletebtn').click(function() {
            $('#deleteFrom').submit();
            $('#deleteModal').modal('hide');
        });
</script>
@endsection