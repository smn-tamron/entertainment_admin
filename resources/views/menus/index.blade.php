@extends('adminlte::page')
@section('content-header-title')
    Menu
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            @if (session('error'))
                <div class="alert alert-danger" role="alert" id="dialog">
                    {{ session('error') }}
                </div>
            @endif
            @if (session('info'))
                <div class="alert alert-success" role="alert" id="dialog">
                    {{ session('info') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            <form action="{{ route('menus.index') }}" name="search-form">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group"> 
                                            {{ Form::text('keyword', request('keyword'), ['class' => 'form-control pull-right','placeholder' => 'Search','autocomplete' => 'off']) }}
                                            <div class="input-group-btn">
                                                <button type="submit" class="btn btn-default akoneya-search-btn">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="col-md-8">
                                    </div>
                                </div> 
                            </form>
                        </div>
                        <div class="col-md-3">
                            <button data-toggle="modal" data-target="#createModal" type="button"
                                class="btn btn-link role-create akoneya-add-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>
                                <span class="add-new-btn-text">{{config('enums.buttonLabel.addNewText')}}</span>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="menutable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Name</th>
                                    <th>Icon Class</th>
                                    <th>Icon</th>
                                    <th>Prefix Url</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($menus as $key => $menu)
                                    <tr class="position_id" menuId="{{$menu->id}}">
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $menu->name }}</td>
                                        <td>{{ $menu->icon }}</td>
                                        <td><span class="{{ $menu->icon }}"></span></td>
                                        <td>{{ $menu->route }}</td>
                                        <td>
                                            <a href="{{ route('menus.submenus', $menu->id) }}" class="btn btn-sm akoneya-info-btn">
                                                <i class="fa fa-list"></i><span class="pl-1">Submenu</span></a>
                                            <button data-toggle="modal" data-target="#editModal" data-id="{{ $menu->id }}"
                                                data-menu_name="{{ $menu->name }}" 
                                                data-menu_icon="{{ $menu->icon }}" 
                                                data-menu_route="{{ $menu->route }}" 
                                                class="btn btn-sm action-btn akoneya-edit-btn ">
                                                <i class="fa fa-edit"></i><span class="pl-1">Edit</span>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-sm action-btn" data-toggle="modal"
                                            data-target="#deleteModal" data-id="{{ $menu->id }}">
                                            <i class="fa fa-trash"></i><span class="pl-1">Delete<span></button>
                                        </td>
                                        @include('layouts.delete',['id' => $menu->id,'route'=>'menus.delete'])
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    @include('menus.form')
 @endsection
 @section('js')
<script>

    $.validator.addMethod("regex", function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "URL must be start with '/'."
    );

    var fixHelperModified = function(e, button) {
                var $originals = button.children();
                var $helper = button.clone();

                $helper.children().each(function(index) {
                    $(this).width($originals.eq(index).width())
                });
                return $helper;
            },
    updateIndex = function(e, ui) {
        var menuIds = new Array();
        $('.position_id').each(function() {
            menuIds.push($(this).attr("menuId"));
        });
        updateMenuTable(menuIds);
    };

    function updateMenuTable(menuIds) {
        $.ajax({
            url: '{!! route("menus.sort") !!}',
            type: 'post',
            data: {
                "menuIds": menuIds,
                "_token": "{{ csrf_token() }}",
            },
            success: function(data) {
                toastr.success(data.success);
                setInterval('location.reload()', 1000);
            }
        })
    }

    $("#menutable tbody").sortable({
        helper: fixHelperModified,
        stop: updateIndex
    }).disableSelection();

    $("tbody").sortable({
        distance: 5,
        delay: 100,
        opacity: 0.6,
        cursor: 'move',
        update: function() {}
    });

    $('#editModal').on('hidden.bs.modal', function () {
        location.reload();
    });
    
    $('#createModal').on('hidden.bs.modal', function () {
        location.reload();
    });

    $('#saveBtn').click(function() {
        $('#createFrom').submit();
    });

    $('#editModal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        var id = button.data('id');
        var name = button.data('menu_name');
        var icon = button.data('menu_icon');
        var route = button.data('menu_route');

        modal = $(this);
        modal.find('.modal-body #update_id').val(id);
        modal.find('.modal-body #update_name').val(name);
        modal.find('.modal-body #update_icon').val(icon);
        modal.find('.modal-body #update_route').val(route);
    });

    $('#updateBtn').click(function() {
        $('#editForm').submit();
    });

    jQuery(document).ready(function($) {
        $('#createFrom').validate({
            ignore: [],
            errorElement: "span",
            errorClass: "error-help-block",
            rules: {
                name: {
                    required: true,
                },
                icon: {
                    required: true,
                },
                route: {
                    required: false,
                    regex: /^\/([a-z][A-Z])*/
                }
            }
        });
        $('#editForm').validate({
            ignore: [],
            errorElement: "span",
            errorClass: "error-help-block",
            rules: {
                name: {
                    required: true,
                },
                icon: {
                    required: true,
                },
                route: {
                    required: false,
                    regex: /^\/([a-z][A-Z])*/
                }
            },

        });
    });

    //Delete
    $('#deleteModal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        var id = button.data('id');
        modal = $(this);
        modal.find('.modal-body #item_id').val(id);
    });
    $('#deletebtn').click(function() {
        $('#deleteFrom').submit();
        $('#deleteModal').modal('hide');
    });
</script>
@endsection