@extends('adminlte::page')
@section('content-header-title')
    Roles
@stop
@section('content')
    @if (session('error'))
        <div class="alert alert-danger" role="alert" id="dialog">
            {{ session('error') }}
        </div>
    @endif
    @if (session('info'))
        <div class="alert alert-success" role="alert" id="dialog">
            {{ session('info') }}
        </div>
    @endif
        <div class="card">
            <div class="card-header">
                <a href="{{route('role.create')}}">
                    <button type="button" class="btn btn-link role-create akoneya-add-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Role</button>
                </a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if(isset($roles))
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Roles</th>
                                <th>Main Menu Permissions</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                            <tr>
                                <td>{{$role->name}}</td>
                                <td>
                                    @if($role->roleHasMainMenus->count() > 0)
                                    <ul class="list-unstyled">
                                        @foreach($role->roleHasMainMenus as $roleHasMainMenu)
                                            <li><span class="fa fa-check-circle text-success"></span> {{$roleHasMainMenu->menu->name}}</li>
                                        @endforeach
                                    </ul>
                                    @else
                                        <span class="fa fa-times-circle text-danger"></span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('role.edit', $role->id) }}"class="btn btn-sm akoneya-edit-btn">
                                        <i class="fa fa-edit"></i><span class="pl-1">Edit</span></a>
                                    <button type="button" class="btn btn-danger btn-sm action-btn" data-toggle="modal"
                                        data-target="#deleteModal" data-id="{{ $role->id }}">
                                        <i class="fa fa-trash"></i><span class="pl-1">Delete<span></button>
                                </td>
                                @include('layouts.delete',['id' => $role->id,'route'=>'role.delete'])
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
            <div class="row ">
                <div class="col-md-6">
                </div>
                <div class="col-md-6 text-right">
                    <div class="pagination">
                        {{ $roles->appends($_GET)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    //Delete
    $('#deleteModal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        var id = button.data('id');
        modal = $(this);
        modal.find('.modal-body #item_id').val(id);
    });
    $('#deletebtn').click(function() {
        $('#deleteFrom').submit();
        $('#deleteModal').modal('hide');
    });
</script>
@endsection