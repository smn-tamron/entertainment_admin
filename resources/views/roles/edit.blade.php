@extends('adminlte::page')
@section('content-header-title')
    Update Role
@stop
@section('content')
    <div class="card">
     
        <form method="POST" action="{{route('role.update')}}" enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <input type="hidden" value="{{$role->id}}" name="id">
        <div class="card-body create-wrapper">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name">Role Name</label>
                        {{ Form::text('name',$role->name, ['class' => 'form-control', 'id' => 'name','required','autocomplete' => 'off']) }}
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <table class="table table-bordered permission-table">
                            <thead>
                                <tr>
                                    <th>Main Menus</th>
                                    <th>Action</th>
                                    <th>Sub Menus</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($menus as $menu)
                                    <tr>
                                        <td>
                                            <label>{{$menu->name}}</label>
                                        </td>
                                        <td>
                                            <div id="menus">
                                                <label class="switch">
                                                    <input type="checkbox" class="togBtn" name="menus[{{$menu->id}}]" id="menu{{$menu->id}}" {{in_array($menu->id, $roleMenuIds) ? "checked" : ""}}>
                                                    <span class="slider2 round"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <ul class="list-unstyled">
                                                @foreach ($menu->subMenus as $subMenu)
                                                <li>
                                                    <label class="switch">
                                                        <span class="checkbox">
                                                            <strong> {{$subMenu->name}}</strong>
                                                        </span>
                                                    </label>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="list-unstyled" id="submenus">
                                                @foreach ($menu->subMenus as $subMenu)
                                                <li>
                                                    <label class="switch">
                                                        <input type="checkbox" class="togBtn menu{{$menu->id}}-submenu" data-menu_id="{{$menu->id}}"  name="sub_menus[{{$subMenu->id}}]" {{in_array($subMenu->id, $roleSubMenuIds) ? "checked" : ""}}>
                                                        <span class="slider2 round"></span>
                                                    </label>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
            <div class="row mt-3">
                <div class="col-md-12">
                    <a href="{{route('role.index')}}" class="btn btn-default akoneya-edit-btn">
                        <i class="fas fa-reply-all"></i>Back
                    </a>
                    <button type="submit" class="btn btn-primary akoneya-add-btn">
                        <i class="far fa-save"></i> Update
                    </button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@endsection
@section('js')
<script>
    $('#menus input[type=checkbox]').change(function() {
        if(this.checked){
            $("."+this.id+"-submenu").prop('checked', true);
        }
        else{
            $("."+this.id+"-submenu").prop('checked', false);
        }
    });

    $('#submenus input[type=checkbox]').change(function() {
        var menuId = $(this).attr('data-menu_id');
        if(this.checked){
            $("#menu"+menuId).prop('checked', true);
        }
        else {
            var lenchk = $(this).closest('ul').find(':checkbox');
            var lenchkChecked = $(this).closest('ul').find(':checkbox:checked');
            if (lenchkChecked.length < 1) {
                $("#menu"+menuId).prop('checked', false);
            }
            else {
                $("#menu"+menuId).prop('checked', true);
            }
        }
    });
</script>
@endsection
