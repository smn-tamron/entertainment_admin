<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
        @yield('title', config('adminlte.title', 'AdminLTE 3'))
        @yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    @if (!config('adminlte.enabled_laravel_mix'))
        <!-- for favicon -->
        <link rel="icon" type="image/png" href="{{ asset('img/sot-slot-logo.png') }}">

        <!-- for fontawesome-icon -->
        <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.css') }}">

        <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">

        <!-- for summernote -->
        <link rel="stylesheet" href="{{ asset('vendor/summernote/summernote-bs4.css') }}">

        <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.css') }}">

        <!-- for toastr -->
        <link rel="stylesheet" href="{{ asset('vendor/toastr/toastr.min.css') }}">

        <!-- for select2 -->
        <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

        <!-- for image upload -->
        <link href="{{ asset('css/uploadfile.css') }}" rel="stylesheet">
        <!-- for datatable -->
        <link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
        <!-- for datetimepicker -->
        <link href="{{ asset('vendor/bootstrap/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        @include('adminlte::plugins', ['type' => 'css'])

        @yield('adminlte_css_pre')

        @yield('adminlte_css')


        <!-- <link rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->

    @else

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @endif

    <!-- <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.6.55/css/materialdesignicons.min.css"> -->

</head>

<body class="@yield('classes_body')" @yield('body_data') >

    @yield('body')

    @if (!config('adminlte.enabled_laravel_mix'))
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
       
        <!-- for validation -->
        <script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
        <!-- <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>  -->
        <script src="{{ asset('vendor/jquery-validation/additional-methods.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-ui/jquery-ui.js') }}"></script>

        <!-- for select2 -->
        <script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
        <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>

        <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
        <script src="{{ asset('vendor/adminlte/dist/js/adminlte.js') }}"></script>

        <!-- for dropdown -->
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- for summernote -->
        <script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>

        <!-- for toastr alert -->
        <script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script>

        <!-- for datetimepicker -->
        <script src="{{ asset('vendor/bootstrap/js/moment.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap-datetimepicker.min.js') }}"></script>

         <!-- overlayScrollbars -->
        <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script> 

        <!-- for plotly -->
        <script src="{{ asset('js/plotly-latest.min.js') }}"></script>

        <!-- for datatable -->
        <script src="{{ asset('js/datatables.min.js') }}"></script>
        <!-- for dialog -->
        <script src="{{ asset('js/dialog.js') }}"></script>
        <!-- sidebar -->
        <script src="{{ asset('js/sidebar.js') }}"></script>

        <script src="{{ asset('js/custom.js') }}"></script>

        <script>
            //dynamic photo
            function dynamicPhoto() {
                var randomId = randomString(8);
                var defaultImgUrl = "{!! asset('img/logo.jpg') !!}";
                var html ='<div class="col-md-3 mt-3  akoneya-image-area">'
                    + '<img src="' + defaultImgUrl + '" id="photoUrl_' + randomId + '"  alt="your image">'
                    + '<input name="images[]" type="file" accept="image/png,image/gif,image/jpeg" id="' + randomId + '" onChange="uploadPhoto(this.id)">'
                    + '<span class="akoneya-remove-image remove-image-btn">&#215;</span>'
                    + '</div>';
                $('.photo_dynamic').append(html);
                $('.akoneya-image-area').addClass('remove-image');
            };
            
        $(document).ready(function(){
            var add_clicks = 1;
            var survey_info_list = [];
            $('#add_survey_info').click(function(){
                var survey_info_id =   add_clicks++; 
                $('.submit_survey').removeClass('hide');

                if(!survey_info_list.includes(survey_info_id)) 
                {
                    var html ='<div class="col-sm-6"><div class="card mt-3 mr-3" id="'+survey_info_id+'">'
                                +'<div class="card-header" style="display: flex"><div class="col-sm-7"><h5 style="float: left">Survey Information('+survey_info_id+')</h5></div><div class="col-sm-5"><div class="btn-toolbar float-right"><a href="javascript:;" id="close" data-id="'+survey_info_id+'" class="btn btn-danger btn-sm"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" data-original-title="Delete"></i></a></div></div></div>'
                                    +'<div class="card-body">'
                                        +'<div class="form-group"><label class="col-lg-6 control-label">Free Text</label>'
                                        +'<div class="col-lg-10"><input type="checkbox" class="free_text" name="free_text['+survey_info_id+']" id="free_text" required></div></div>'

                                        +'<div class="form-group"><label class="col-lg-6 control-label">Description</label>'
                                        +'<div class="col-md-12"><input type="text" class="form-control" placeholder="Description" name="description['+survey_info_id+']" required></div></div>'

                                        +'<div class="form-group"><label class="col-lg-6 control-label">Quantity</label>'
                                        +'<div class="col-md-10"><input type="number" class="form-control" placeholder="Quantity" name="quantity['+survey_info_id+']" required></div></div>'
                                        +'<div class="form-group"><label class="col-lg-6 control-label">Remark</label>'
                                        +'<div class="col-md-10"><input type="text" class="form-control" placeholder="Remark" name="remark['+survey_info_id+']" required></div></div>'
                                    +'</div>'
                             +'</div></div>';
                    $('.survey_dynamic').append(html);
                    survey_info_list.push(survey_info_id);
                }
            });

            $(document).on('click','a#close',function(){
                var id = $(this).data('id');
                var index = survey_info_list.indexOf(id);
                console.log(index);
                $('#'+id).remove();
                survey_info_list.splice(survey_info_list.indexOf(id.toString()), 1);
                console.log(index);
                if(index == '0')
                {
                	$('.submit_survey').addClass('hide');
                }
            });
        });
        

            //Start Delete Media Ajax
            function deleteMediaAjax(id, url) {
                if (id != null && url != null) {
                    var token = "{!! csrf_token() !!}";
                    $.ajax({
                        type: "delete",
                        dataType: "json",
                        url: url,
                        data: {
                            'id': id,
                            "_token": token,
                        },
                        success: function (data) {
                            toastr.options.closeButton = true;
                            toastr.options.closeMethod = 'fadeOut';
                            toastr.options.fadeOut = 10;
                            toastr.options.fadeIn = 10;
                            toastr.options.closeDuration = 10;
                            if (data.statusCode == 200) {
                                toastr.success(data.message);
                                $('#old_images' + id).remove();
                            } else {
                                toastr.error(data.message);
                            }
                        }
                    });
                }
            }
        </script>
        @include('adminlte::plugins', ['type' => 'js'])

        @yield('adminlte_js')

    @else
        <script src="{{ asset('js/app.js') }}"></script>
    @endif

</body>

</html>
