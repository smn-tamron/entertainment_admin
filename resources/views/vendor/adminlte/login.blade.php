<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Game</title>
    <!-- for favicon -->
    <link rel = "icon" type = "image/png" href = "{{ asset('img/sot-slot-logo.png') }}">
    
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    
    <!-- for fontawesome-icon -->
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.css') }}">

    <!--Custom styles-->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <script src="https://www.google.com/recaptcha/api.js"></script>
    
</head>
<body class="login-page">
    <div class="login-form shadow">

        <div class="login-logo shadow">
            <img src="{{asset('img/sot-slot-logo.png')}}">
        </div>
                                                        
        <div class="login-sub-heading mt-2 mb-4">Sign In</div> 

                <form method="POST" action="{{ route('login') }}" id="login">
                @csrf
                                                               
                    <div class="input-group mb-3">                                             
                        <div class="input-group-append" >
                            <div class="input-group-text">
                                <span class="fas fa-envelope {{ config('adminlte.classes_auth_icon', '') }}"></span>
                            </div>
                        </div>
                        <input type="text" name="email" id="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email') }}" placeholder="Email" autofocus style="width: 80px;">
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                        <div id="invalid-feedback1">

                        </div>
                                                      
                    </div>
                   
                                                 
                    <div class="input-group mb-3">
                                                            
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock {{ config('adminlte.classes_auth_icon', '') }}"></span>
                            </div>
                        </div>

                        <input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="{{ __('adminlte::adminlte.password') }}" style="width: 80px;">
                        @if ($errors->has('password'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                        <div id="invalid-feedback2">

                        </div>

                    </div>
                   

                    <!--recaptcha-->
                    <div class="g-recaptcha recap" id="reca" style="padding-bottom: 5px;" data-callback="recaptchaCallback"
                            data-sitekey="{{env('G_RECAPTCHA_SITE_KEY')}}">
                    </div>
                              
                    <button type=submit class="button custom-submit-btn" id="btnlogin">LOGIN</button>
                </form>
    </div>

</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
       $(document).ready(function() {
            
            $('#login').submit(function(event) {
                var email = $('#email').val();
                var password = $("#password").val();
                var result1 = "";
                var result2 = "";
                $.ajax({

                    type: "post",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('users.check') }}",
                    dataType: "json",
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        email: email,
                        password: password
                    },

                    success: function(data) {
                        console.log(data);

                        if (data['valid'] == "false") {

                            $('div#invalid-feedback1').empty();
                            $('div#invalid-feedback2').empty();

                            result1 +=
                                "<strong style='color:red;'>The email is required.</strong>";

                            result2 +=
                                "<strong style='color:red;'>The password is required.</strong>";

                            $('#email').addClass('is-invalid');
                            $('#password').addClass('is-invalid');
                            $('div#invalid-feedback1').append(result1);
                            $('div#invalid-feedback2').append(result2);



                        } else if (data['valid'] == "empty") {
                            $('div#invalid-feedback1').empty();
                            $('div#invalid-feedback2').empty();
                            $('#email').addClass('is-invalid');
                            $('#password').removeClass('is-invalid');
                            result1 +=
                                "<strong style='color:red;'>Your email was not found.</strong>";
                            $('div#invalid-feedback1').append(result1);

                        } else if (data['valid'] == "psw") {

                            $('div#invalid-feedback1').empty();
                            $('div#invalid-feedback2').empty();
                            $('#email').removeClass('is-invalid');
                            $('#password').addClass('is-invalid');
                            result2 +=
                                "<strong style='color:red;'>The password is required.</strong>";
                            $('div#invalid-feedback2').append(result2);

                        } else if (data['valid'] == "error") {
                            console.log(data['wrong']);
                            if(data['wrong']==3){
                                $('#reca').addClass('wrong-display');
                                $("#btnlogin").attr("disabled", true);
                            }else{
                                $('#login').unbind('submit').submit();
                            }
                        } else {

                            $('#login').unbind('submit').submit();


                        }

                    },
                    error: function(data) {

                    }

                })

                return false;

            })
        })

        window.recaptchaCallback = undefined;

        jQuery(document).ready(function($) {

        window.recaptchaCallback = function recaptchaCallback(response) {
            console.log("ok click")
            $("#btnlogin").attr("disabled", false);
        }

        });

    </script>




