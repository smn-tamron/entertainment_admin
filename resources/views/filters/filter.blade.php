<div class="input-group">
    @if (isset($search_data))
        {!! Form::text("search_data","$search_data", ['class' => 'form-control']) !!}
    @else 
        {!! Form::text("search_data","", ['class' => 'form-control']) !!}
    @endif
    
    <div class="input-group-append">
        <button class="btn btn-default" type="submit">
            <i class="fas fa-search"></i>
          </button>
    </div>
</div>

