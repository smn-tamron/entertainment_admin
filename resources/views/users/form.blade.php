<!-- Create Form -->
<div class="modal fade" id="createModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'users.store', 'method' => 'POST', 'id' => 'createFrom']) !!}
                @csrf

                <div class="form-group">
                    <label for="name">Name</label>
                    {{ Form::text('name', '', ['class' => 'form-control','autocomplete'=>'off']) }}

                </div>
                <div class="form-group">
                    <label for="name">Email</label>
                    {{ Form::email('email', '', ['class' => 'form-control','autocomplete' => 'off']) }}
                </div>
                <div class="form-group">
                    <label for="name">Role</label>
                    {{ Form::select('roleId', $role, 0, ['class' => 'form-control', 'placeholder' => 'Select Role','id' => 'role_id']) }}
                </div>
                <div class="form-group">
                    <label for="name">Password</label>
                    {!! Form::password('password', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label for="name">Confirm Password</label>
                    {!! Form::password('passwordConfirmation', ['class' => 'form-control']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn "
                    data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary akoneya-add-btn" id="saveBtn">Save</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>
<!-- End Create Form -->


<!-- Edit Form -->
<div class="modal fade" id="editModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'users.update', 'method' => 'PATCH', 'id' => 'editForm']) !!}
                @csrf
                <input type="hidden" name="id" id="id">
                <div class="form-group">
                    <label for="name">Name</label>
                    {{ Form::text('name', '', ['class' => 'form-control', 'id' => 'name']) }}

                </div>
                <div class="form-group">
                    <label for="name">Email</label>
                    {{ Form::email('email', '', ['class' => 'form-control', 'id' => 'email']) }}
                </div>
                <div class="form-group" id= "select-role">
                    <label for="name">Role</label>
                    {{ Form::select('roleId', $role, 0, ['class' => 'form-control', 'placeholder' => 'Select Role>', 'id' => 'edit_role_id']) }}
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn "
                    data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary akoneya-add-btn" id="updateBtn">Update</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>
<!-- End Edit Form -->
