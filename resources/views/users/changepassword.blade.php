<div class="modal fade" id="changePasswordModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'users.change_password', 'method' => 'POST', 'id' => 'changePswFrom']) !!}
                 @csrf
                <div class="modal-body">
                    
                    <input type="hidden" name="id" value="{{ \Crypt::encrypt(Auth::user()->id) }}" >
                    <div class="form-group">
                        <label for="name">Current Password</label>
                        {!! Form::password('currentPassword', ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <label for="name">New Password</label>
                        {!! Form::password('newPassword', ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label for="name">Confirm New Password</label>
                        {!! Form::password('newPasswordConfirmation', ['class' => 'form-control']) !!}
                    </div>
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left akoneya-edit-btn "
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary akoneya-add-btn" id="changePswBtn">Save</button>
                </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>
