@extends('adminlte::page')
@section('content-header-title')
    Users
@stop
@section('content')
    @if (session('info'))
        <div class="alert alert-success" role="alert" id="dialog">
            {{ session('info') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert" id="dialog">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">

            <button data-toggle="modal" data-target="#createModal" type="button"
                class="btn btn-link role-create akoneya-add-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>
                Add User</button>
            @include('users.form')
        </div>

        <div class="card-body">
            @if (isset($users))
                <div class="table-responsive">
                    <table class="table table-striped table-bordered task-table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $key => $user)
                                <tr>
                                    <td>{{ $users->firstItem() + $key }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role->name }}</td>
                                    <td>


                                        <button data-toggle="modal" data-target="#editModal" data-id="{{ $user->id }}"
                                            data-name="{{ $user->name }}" data-email="{{ $user->email }}"
                                            data-role_id="{{ $user->role_id }}"
                                            data-role_name="{{ $user->role->name }}"
                                            class="btn btn-sm action-btn akoneya-edit-btn ">
                                            <i class="fa fa-edit"></i><span class="pl-1">Edit</span>
                                        </button>
                                        @if(!$user->isSuperAdmin())
                                        <button type="button" class="btn btn-danger btn-sm action-btn" data-toggle="modal"
                                            data-target="#deleteModal" data-id="{{ $user->id }}">
                                            <i class="fa fa-trash"></i><span class="pl-1">Delete</span></button>
                                        @endif
                                    </td>
                                    @include('layouts.delete',['id' => $user->id,'route'=>'users.delete'])
                                </tr>

                            @endforeach
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                {!! Form::open(['route' => 'users.delete', 'method' => 'delete', 'id' => 'delete_form']) !!}
                {{ Form::hidden('id', 0) }}
                {!! Form::close() !!}



            @endif
        </div>
        <div class="card-footer">
            <div class="col-md-6 offset-md-6 text-right">
                <div class="pagination">
                    {{ $users->links() }}
                </div>
            </div>
        </div>

    </div>
    </div>
@endsection
@section('js')
    <script>
        // create
        $('#createModal').on('show.bs.modal', function(e) {
            $('input[type*="email"]').val("");
            $('input[type*="password"]').val("");
        });

        $('#saveBtn').click(function() {
            $('#createFrom').submit();
        });

        $("#edit_role_id").find("option[value='']").remove();

        $('select[name*="roleId"]').change(function() {
            $("#role_id").find("option[value='']").remove();
        })

        $('#editModal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var email = button.data('email');
            var role_id = button.data('role_id');
            var role_name = button.data('role_name');

            modal = $(this);
        
            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #name').val(name);
            modal.find('.modal-body #email').val(email);
            modal.find('.modal-body #edit_role_id').val(role_id);
        });
        $('#updateBtn').click(function() {
            $('#editForm').submit();
        });

        jQuery(document).ready(function($) {

            $('#createFrom').validate({
                ignore: [],
                errorElement: "span",
                errorClass: "error-help-block",
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    roleId: {
                        required: true,
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    passwordConfirmation: {
                        required: true,
                        equalTo: '[name="password"]'
                    },


                },

            });
        });
        jQuery(document).ready(function($) {

            $('#editForm').validate({
                ignore: [],
                errorElement: "span",
                errorClass: "error-help-block",
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    roleId: {
                        required: true,
                    },
                },

            });
        });

        $('#deleteModal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            modal = $(this);
            modal.find('.modal-body #item_id').val(id);

        });
        $('#deletebtn').click(function() {
            $('#deleteFrom').submit();
            $('#deleteModal').modal('hide');
        });

    </script>
@endsection
