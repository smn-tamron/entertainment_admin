@extends('adminlte::page')
@section('title', 'Game')
@section('content-header-title')
    Dashboard
@stop

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div id='slot_pie'></div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <h5 class="text-dark mb-2 mt-1"><i class="fa fa-users mr-2"></i>Daily Reward Claimed Player List For Today</h5>
                        <table id="rewardtbl" class="table table-striped table-bordered nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <td class="text-center text-uppercase"><strong>No</strong></td>
                                    <td class="text-center text-uppercase"><strong>Player Name</strong></td>
                                    <td class="text-center text-uppercase"><strong>Day Number</strong></td>
                                    <td class="text-center text-uppercase"><strong>Reward Coins</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dailyRewards as $key => $dailyReward)
                                    <tr>
                                        <td class="text-center">{{$key + 1}}</td>
                                        <td class="text-center">
                                            <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                                <i class="fa fa-user mx-2"></i><p class="mx-2 mb-1">{{$dailyReward->player->name}} </p>
                                            </div>
                                        </td>
                                        <td class="text-center text-uppercase">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card text-info">
                                                <p class="mx-2 mb-1 text-bold">Day {{$dailyReward->day_no}} </p>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card text-danger">
                                                <i class="fa fa-gift mx-2"></i><p class="mx-2 mb-1 text-bold">{{$dailyReward->claim_coin}}</p>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('home') }}" name="search-form">
                                <div class="row justify-content-start">
                                    <div class="col-3">
                                        <div class="input-group">
                                            <select class="form-control results" id="batchNo" name="batchNo">
                                                <option selected="" value="">Select Batch No</option>
                                                @foreach ($batchNos as $key => $batchNo)
                                                <option value="{{$batchNo->batch_no}}"  @if(request("batchNo") == $batchNo->batch_no) selected @endif>No. {{ $batchNo->batch_no }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <select class="form-control results" id="betTypeId" name="betTypeId">
                                                <option selected="" value="">Select Bet Type</option>
                                                @foreach ($slotBets as $key => $bet)
                                                <option value="{{$bet->id}}"  @if(request("betTypeId") == $bet->id) selected @endif>{{ $bet->coin_usage }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <!-- <h5 class="text-dark mb-2 mt-1"><i class="fa fa-users mr-2"></i>Daily Reward Claimed Player List For Today</h5> -->
                        <table id="batchtbl" class="table table-striped table-bordered nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <td class="text-center text-uppercase"><strong>No</strong></td>
                                    <td class="text-center text-uppercase"><strong>Batch No</strong></td>
                                    <td class="text-center text-uppercase"><strong>Bet Type</strong></td>
                                    <td class="text-center text-uppercase"><strong>Number of players</strong></td>
                                    <td class="text-center text-uppercase"><strong>Win Rate</strong></td>
                                    <td class="text-center text-uppercase"><strong>Loss Rate</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($results as $key => $result)
                                    <tr>
                                        <td class="text-center">{{$key + 1}}</td>
                                        <td class="text-center">
                                            <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                                <p class="mx-2 mb-1 text-bold">{{ $result->batch_no }}</p>
                                            </div>
                                            <h5 class="mb-0"></h5>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                                <i class="fa fa-coins mx-2"></i><p class="mx-2 mb-1">{{ $result->coin_usage }}</p>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                                <i class="fa fa-users mx-2"></i><p class="mx-2 mb-1 text-bold">{{$result->customer_count}}</p>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card text-success">
                                                <p class="mx-2 mb-1 text-bold">{{number_format($result->winRate, 2, '.', '')}} %</p>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card text-danger">
                                                <p class="mx-2 mb-1 text-bold">{{number_format($result->lossRate, 2, '.', '')}} %</p>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="col-md-6 offset-md-6 text-right">
                        <div class="pagination">
                        {{ $results->appends($_GET)->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

@section('js')
<script>
    $('.results').on('change', function() {
        document.forms["search-form"].submit();
    });

    var data = [{
        type: "pie",
        values: JSON.parse('{{ json_encode($amount) }}'),
        labels: ["Loss", "Profit"],
        textinfo: "percent+value",
        insidetextorientation: "radial",
        hoverinfo: 'none',
    }];

    var layout = {
        showlegend: true,
        legend: {"orientation": "h"},
        title:"Slot Game's Statistic",
    };

    Plotly.newPlot('slot_pie', data, layout, {displayModeBar: false})

    $(document).ready(function() {
        $('#rewardtbl').DataTable({
            "bLengthChange": false,
            "searching": false,
            "bInfo": false,
            "pageLength" : 5
        });
    });

</script>
@endsection