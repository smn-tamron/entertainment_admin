@extends('errors::layout')

@section('title', __('Too Many Requests'))
@section('content')
	<div class="errors-content-title">
		<h1>Oops!</h1>
		<h2>429 - Too Many Requests</h2>
	</div>
@stop