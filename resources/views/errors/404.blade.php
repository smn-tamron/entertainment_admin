@extends('errors::layout')

@section('title', __('Not Found'))
@section('content')
	<div class="errors-content-title">
		<h1>Oops!</h1>
		<h2>404 - Not Found</h2>
	</div>
@stop	


