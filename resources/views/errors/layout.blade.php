<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
        * {
          -webkit-box-sizing: border-box;
                  box-sizing: border-box;
        }

        body {
          padding: 0;
          margin: 0;
        }

        #errors-content {
          position: relative;
          height: 100vh;
        }

        #errors-content .errors-div-1 {
          position: absolute;
          left: 50%;
          top: 50%;
          -webkit-transform: translate(-50%, -50%);
              -ms-transform: translate(-50%, -50%);
                  transform: translate(-50%, -50%);
        }

        .errors-div-1 {
          max-width: 520px;
          width: 100%;
          line-height: 1.4;
          text-align: center;
        }

        .errors-div-1 .errors-content-title {
          position: relative;
          height: 200px;
          margin: 0px auto 20px;
          z-index: -1;
        }

        .errors-div-1 .errors-content-title h1 {
          font-family: 'Montserrat', sans-serif;
          font-size: 236px;
          font-weight: 200;
          margin: 0px;
          color: aquamarine;
          text-transform: uppercase;
          position: absolute;
          left: 50%;
          top: 50%;
          -webkit-transform: translate(-50%, -50%);
              -ms-transform: translate(-50%, -50%);
                  transform: translate(-50%, -50%);
        }

        .errors-div-1 .errors-content-title h2 {
          font-family: 'Montserrat', sans-serif;
          font-size: 28px;
          font-weight: 400;
          text-transform: uppercase;
          color: #211b19;
          background: #fff;
          padding: 10px 5px;
          margin: auto;
          display: inline-block;
          position: absolute;
          bottom: 0px;
          left: 0;
          right: 0;
        }

        .errors-div-1 a {
          font-family: 'Montserrat', sans-serif;
          display: inline-block;
          font-weight: 700;
          text-decoration: none;
          color: #fff;
          text-transform: uppercase;
          padding: 13px 23px;
          background: #ff6300;
          font-size: 18px;
          -webkit-transition: 0.2s all;
          transition: 0.2s all;
        }

        .errors-div-1 a:hover {
          color: #ff6300;
          background: #211b19;
        }

        @media only screen and (max-width: 767px) {
          .errors-div-1 .errors-content-title h1 {
            font-size: 148px;
          }
        }

        @media only screen and (max-width: 480px) {
          .errors-div-1 .errors-content-title {
            height: 148px;
            margin: 0px auto 10px;
          }
          .errors-div-1 .errors-content-title h1 {
            font-size: 86px;
          }
          .errors-div-1 .errors-content-title h2 {
            font-size: 16px;
          }
          .errors-div-1 a {
            padding: 7px 15px;
            font-size: 14px;
          }
        }
</style>
    </head>
    <body>
        <div id="errors-content">
            <div class="errors-div-1">
                @yield('content')
            
                <a href="{{ app('router')->has('home') ? route('home') : url('/') }}">Home</a>
            </div>
        </div>
    </body>
</html>
