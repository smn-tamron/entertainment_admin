@extends('errors::layout')

@section('title', __('Unauthorized'))
@section('content')
	<div class="errors-content-title">
		<h1>Oops!</h1>
		<h2>401 - Unauthorized</h2>
	</div>
@stop	
