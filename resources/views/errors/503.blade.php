@extends('errors::layout')

@section('title', __('Service Unavailable'))
@section('content')
	<div class="errors-content-title">
		<h1>Oops!</h1>
		<h2>503 - Service Unavailable</h2>
	</div>
@stop