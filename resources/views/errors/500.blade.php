@extends('errors::layout')

@section('title', __('Internal Server Error'))
@section('content')
	<div class="errors-content-title">
		<h1>Oops!</h1>
		<h2>500 - Internal Server Error</h2>
	</div>
@stop	


