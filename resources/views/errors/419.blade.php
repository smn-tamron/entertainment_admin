@extends('errors::layout')

@section('title', __('Page Expired'))
@section('content')
	<div class="errors-content-title">
		<h1>Oops!</h1>
		<h2>419 - Page Expired</h2>
	</div>
@stop