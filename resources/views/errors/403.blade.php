@extends('errors::layout')

@section('title', __('Forbidden'))
@section('content')
	<div class="errors-content-title">
		<h1>Oops!</h1>
		<h2>403 - Forbidden</h2>
	</div>
@stop	
