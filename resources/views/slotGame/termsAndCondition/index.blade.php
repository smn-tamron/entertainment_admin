@extends('adminlte::page')
@section('content-header-title')
<i class="fa fa-dice mr-2"></i>Slot Game
@stop
@section('content')
<div class="card border-0">
    <ul class="nav nav-tabs akoneya-nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link border-left-0" href="{{ route('slotReel.index') }}" role="tab" aria-controls="custom-tabs-three-home"><span class="nav-title">Slot Reels</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotBet.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Bets</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotComboReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Combo Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotRewardType.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Reward Types</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('dailyReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Daily Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link active" href="{{ route('slotTermsAndCondition.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Terms And Conditions</span></a>
        </li>
    </ul>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane fade active show" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                @if (session('info'))
                    <div class="alert alert-success" role="alert" id="dialog">
                        {{ session('info') }}
                    </div>
                @endif
                @if(isset($data['id']))
                    {!! Form::open(['route' => 'slotTermsAndCondition.update', 'method' => 'PUT','files' => true, 'enctype'=>'multipart/form-data']) !!}
                    {!! Form::hidden('id', $data['id']) !!}
                    {!! Form::hidden('game_category_id', $data['game_category_id']) !!}
                @else
                    {!! Form::open(['route' => 'slotTermsAndCondition.save', 'method' => 'POST','files' => true, 'enctype'=>'multipart/form-data']) !!}
                    {!! Form::hidden('game_category_id', config('enums.gameCategory.slotReel')) !!}
                @endif
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group reference">
                            @if(isset($data['id']))
                                {{ Form::label('description', 'Update Description', ['class' => 'control-label description']) }}
                                <div class="float-right">
                                    @if(isset($data['id']))
                                    <strong>Last Updated: </strong> {{clientDateFormat($data['updated_at'])}}
                                    @endif
                                </div>
                                {{ Form::textarea('description', $data['description'], ['class' => 'form-control', 'required' => 'required']) }}
                            @else
                                {{ Form::label('description', 'Create Description', ['class' => 'control-label description']) }}
                                {{ Form::textarea('description', '', ['class' => 'form-control', 'required' => 'required']) }}
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary akoneya-add-btn">
                                <i class="far fa-save"></i>
                                @if(isset($data['id']))Update
                                @else Save @endif
                            </button>
                        </div>
                           
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection