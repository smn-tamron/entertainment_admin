<div class="modal fade" id="createModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create Daily Reward</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['dailyReward.save'], 'method' => 'POST','id' => 'createForm']) !!}
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 1</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 2</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 3</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 4</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 5</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 6</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 7</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary akoneya-add-btn" id="saveBtn">Save</button>
            </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="editModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Daily Reward Coin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['dailyReward.edit'], 'method' => 'PUT','id' => 'editForm']) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="id" id="update_id">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 1</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off', 'id' => 'update_day1','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 2</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off', 'id' => 'update_day2','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 3</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off', 'id' => 'update_day3','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 4</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off', 'id' => 'update_day4','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 5</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off', 'id' => 'update_day5','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 6</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off', 'id' => 'update_day6','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="reward_coin">Day 7</label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::number('reward_coin[]','', ['class' => 'form-control', 'autocomplete' => 'off', 'id' => 'update_day7','pattern'=>'^[1-9]\d*$']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary akoneya-add-btn" id="editBtn">Update</button>
            </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>

  