@extends('adminlte::page')
@section('content-header-title')
    <i class="fa fa-dice mr-2"></i>Slot Game
@stop
@section('content')
<div class="card border-0">
    <ul class="nav nav-tabs akoneya-nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link border-left-0" href="{{ route('slotReel.index') }}" role="tab" aria-controls="custom-tabs-three-home"><span class="nav-title">Slot Reels</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotBet.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Bets</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotComboReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Combo Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotRewardType.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Reward Types</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link active" href="{{ route('dailyReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Daily Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotTermsAndCondition.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Terms And Conditions</span></a>
        </li>
    </ul>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane fade active show" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                @if (session('error'))
                    <div class="alert alert-danger" role="alert" id="dialog">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('info'))
                    <div class="alert alert-success" role="alert" id="dialog">
                        {{ session('info') }}
                    </div>
                @endif
                <div class="row mb-4">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button data-toggle="modal" data-target="#createModal"
                            class="btn btn-link role-create akoneya-add-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i><span class="add-new-btn-text">{{config('enums.buttonLabel.addNewText')}}</span></button>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="table-responsive">
                        <table id="catetbl" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center text-uppercase">No</th>
                                    <th class="text-center text-uppercase">
                                        Reward Coins
                                        <div class="row mx-1">
                                            <div class="col d-flex flex-row justify-content-center pt-2 border border-left-0 border-right-0 px-2">
                                                <h6>Day 1</h6>
                                            </div>
                                            <div class="col d-flex flex-row justify-content-center pt-2 border border-left-0 border-right-0 px-2">
                                                <h6>Day 2</h6>
                                            </div>
                                            <div class="col d-flex flex-row justify-content-center pt-2 border border-left-0 border-right-0 px-2">
                                                <h6>Day 3</h6>
                                            </div>
                                            <div class="col d-flex flex-row justify-content-center pt-2 border border-left-0 border-right-0 px-2">
                                                <h6>Day 4</h6>
                                            </div>
                                            <div class="col d-flex flex-row justify-content-center pt-2 border border-left-0 border-right-0 px-2">
                                                <h6>Day 5</h6>
                                            </div>
                                            <div class="col d-flex flex-row justify-content-center pt-2 border border-left-0 border-right-0 px-2">
                                                <h6>Day 6</h6>
                                            </div>
                                            <div class="col d-flex flex-row justify-content-center pt-2 border border-left-0 border-right-0 px-2">
                                                <h6>Day 7</h6>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="text-center text-uppercase">Created Date</th>
                                    <th class="text-center text-uppercase">Updated Date</th>
                                    <th class="text-center text-uppercase">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rewards as $key => $reward)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>
                                            <div class="row justify-content-left mx-1">
                                            @foreach ($reward->coin as $key => $coin)
                                                <div class="col d-flex flex-row justify-content-between pt-2 text-danger mx-1 slot-card">
                                                    <i class="fa fa-gift"></i><p class="text-bold mb-1">{{ $coin }}</p>
                                                </div>
                                            @endforeach
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <small>{{$reward->created_at ? diffForHumanFormat($reward->created_at) : ''}}</small>
                                            <br>
                                            <span class="text-bold">{{$reward->created_at ? clientDateFormat($reward->created_at) : ''}}</span>
                                        </td>
                                        <td class="text-center">
                                            <small>{{$reward->updated_at ? diffForHumanFormat($reward->updated_at) : ''}}</small>
                                            <br>
                                            <span class="text-bold">{{$reward->updated_at ? clientDateFormat($reward->updated_at) : ''}}</span>
                                        </td>
                                        <td>
                                            <button data-toggle="modal" data-target="#editModal" class="btn btn-sm action-btn akoneya-edit-btn" data-update_id="{{$reward->id}}"
                                            data-update_reward_coin="{{json_encode($reward->coin)}}" > 
                                                <i class="fa fa-edit"></i><span class="pl-1">Edit</span>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-sm action-btn" data-toggle="modal" data-target="#deleteModal" data-id="{{$reward->id}}">
                                                <i class="fa fa-trash"></i><span class="pl-1">Delete<span>
                                            </button>
                                        </td>
                                        @include('layouts.delete',['route'=>'dailyReward.delete'])
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="pagination">
           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('slotGame.dailyReward.form')
@endsection

@section('js')
<script>
    // Create
    $('#createModal').on('show.bs.modal', function(e) {});
    $('#saveBtn').click(function() {
        $('#createForm').submit();
    });

    //Form Validation 
    $('#createForm').validate({
        ignore: [],
        errorElement: "span",
        errorClass: "error-help-block",
        rules: {
            "reward_coin[]": "required"
        }
    });

    $('#editBtn').click(function() {
        $('#editForm').submit();
    });

    //Form Validation 
    $('#editForm').validate({
        ignore: [],
        errorElement: "span",
        errorClass: "error-help-block",
        rules: {
            "reward_coin[]":"required"
        }
    });

    // Edit
    $('#editModal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        var reward = button.data('update_reward_coin');
        
        modal = $(this);
        modal.find('.modal-body #update_id').val(button.data('update_id'));
        modal.find('.modal-body #update_day1').val(reward[0]);
        modal.find('.modal-body #update_day2').val(reward[1]);
        modal.find('.modal-body #update_day3').val(reward[2]);
        modal.find('.modal-body #update_day4').val(reward[3]);
        modal.find('.modal-body #update_day5').val(reward[4]);
        modal.find('.modal-body #update_day6').val(reward[5]);
        modal.find('.modal-body #update_day7').val(reward[6]);

    });

    //Delete
    $('#deleteModal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        var id = button.data('id');
        modal = $(this);
        modal.find('.modal-body #item_id').val(id);
    });

    $('#deletebtn').click(function() {
        $('#deleteFrom').submit();
        $('#deleteModal').modal('hide');
    });


</script>
@endsection