@extends('adminlte::page')
@section('content-header-title')
    <i class="fa fa-users mr-2"></i>Players Result
@stop
@section('content')
    @if (session('info'))
        <div class="alert alert-success" role="alert" id="dialog">
            {{ session('info') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert" id="dialog">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('playerResult.index') }}" name="search-form">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control pull-right" placeholder="Search"
                                        value="{{ request('keyword') }}" autocomplete="off">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default akoneya-search-btn">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">
            @if (isset($results))
                <div class="table-responsive">
                    <table class="table table-striped table-bordered task-table">
                        <thead>
                            <tr>
                                <th class="text-center text-uppercase">No.</th>
                                <th class="text-center text-uppercase">Player Name</th>
                                <th class="text-center text-uppercase">Device ID</th>
                                <th class="text-center text-uppercase">Current Coin</th>
                                <th class="text-center text-uppercase">Win Rate</th>
                                <th class="text-center text-uppercase">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($results as $key => $result)
                                <tr>
                                    <td class="text-center">{{ $results->firstItem() + $key }}</td>
                                    <td class="text-center">{{ $result->player->name }}</td>
                                    <td class="text-center">
                                        @php 
                                            $deviceToken = $result->player->deviceTokens->last();
                                        @endphp
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                            <p class="mx-2 mb-1">{{ $deviceToken ?  $deviceToken->device_id : '-' }}</p>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                            <i class="fa fa-coins mx-2"></i><p class="mx-2 mb-1">{{ number_format($result->player->coin) }}</p>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        @php
                                            $playerResultCount = $result->player->slotResults->count();
                                            $winCount = $result->player->slotResults->where('win_status', 1)->count();
                                            $winRate =  ($winCount / $playerResultCount) * 100;
                                        @endphp
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card text-danger">
                                            <p class="text-bold mx-1 mb-1">{{ number_format($winRate, 2) }}</p><i class="fa fa-percent fa-sm mx-1"></i>
                                        </div>
                                        
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('playerResult.detail', $result->customer_id) }}" class="btn btn-sm akoneya-detail-btn">
                                            <i class="fa fa-eye"></i><span class="pl-1">{{config('enums.buttonLabel.viewDetail')}}</span>
                                        </a>
                                    </td>
                                </tr>

                            @endforeach
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                {{ Form::hidden('id', 0) }}
                {!! Form::close() !!}
            @endif
        </div>
        <div class="card-footer">
            <div class="col-md-6 offset-md-6 text-right">
                <div class="pagination">
                {{ $results->appends($_GET)->links() }}
                </div>
            </div>
        </div>

    </div>
    </div>
@endsection

