@extends('adminlte::page')
@section('content-header-title')
    <i class="fa fa-users mr-2"></i>Players Result
@stop
@section('content')
    @if (session('info'))
        <div class="alert alert-success" role="alert" id="dialog">
            {{ session('info') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert" id="dialog">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('playerResult.index') }}" name="search-form">
                        <div class="row justify-content-start">
                            <div class="col-md-2">
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control pull-right" placeholder="Search"
                                        value="{{ request('keyword') }}" autocomplete="off">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default akoneya-search-btn">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <select class="form-control results" id="batchNo" name="batchNo">
                                        <option selected="" value="">Select Batch No</option>
                                        @foreach ($batchNos as $key => $batchNo)
                                        <option value="{{$batchNo->batch_no}}"  @if(request("batchNo") == $batchNo->batch_no) selected @endif>No. {{ $batchNo->batch_no }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <select class="form-control results" id="betTypeId" name="betTypeId">
                                        <option selected="" value="">Select Bet Type</option>
                                        @foreach ($slotBets as $key => $bet)
                                        <option value="{{$bet->id}}"  @if(request("betTypeId") == $bet->id) selected @endif>{{ $bet->coin_usage }} coins</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-9">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">
            @if (isset($results))
                <div class="table-responsive">
                    <table class="table table-striped table-bordered task-table">
                        <thead>
                            <tr>
                                <th class="text-center text-uppercase">No.</th>
                                <th class="text-center text-uppercase">Batch No</th>
                                <th class="text-center text-uppercase">Bet Type</th>
                                <th class="text-center text-uppercase">Players</th>
                                <th class="text-center text-uppercase">Total Win Coin</th>
                                <th class="text-center text-uppercase">Limit Amount</th>
                                <!-- <th class="text-center text-uppercase">Actions</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($results as $key => $result)
                                <tr>
                                    <td class="text-center">{{ $results->firstItem() + $key }}</td>
                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                            <p class="mx-2 mb-1 text-bold">{{ $result->batch_no }}</p>
                                        </div>
                                        <h5 class="mb-0"></h5>
                                    </td>
                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                            <i class="fa fa-coins mx-2"></i><p class="mx-2 mb-1">{{ $result->coin_usage }}</p>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                            <i class="fa fa-user mx-2"></i><p class="mx-2 mb-1">{{ $result->name }}</p>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card text-danger">
                                            <i class="fa fa-coins mx-2"></i><p class="mx-2 mb-1 text-bold">{{ number_format($result->total_coin) }}</p>
                                        </div>
                                    </td>

                                    <td class="text-center">
                                        <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                            <i class="fa fa-coins mx-2"></i><p class="mx-2 mb-1 text-bold">{{ $result->limit_reward_amount }}</p>
                                        </div>
                                    </td>
                                    <!-- <td class="text-center">
                                        <a href="{{ route('playerResult.detail', $result->batch_no) }}" class="btn btn-sm akoneya-detail-btn">
                                            <i class="fa fa-eye"></i><span class="pl-1">{{config('enums.buttonLabel.viewDetail')}}</span>
                                        </a>
                                    </td> -->
                                </tr>

                            @endforeach
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                {{ Form::hidden('id', 0) }}
                {!! Form::close() !!}
            @endif
        </div>
        <div class="card-footer">
            <div class="col-md-6 offset-md-6 text-right">
                <div class="pagination">
                {{ $results->appends($_GET)->links() }}
                </div>
            </div>
        </div>

    </div>
    </div>
@endsection
@section('js')
<script>

    $('.results').on('change', function() {
        document.forms["search-form"].submit();
    });
</script>
@endsection

