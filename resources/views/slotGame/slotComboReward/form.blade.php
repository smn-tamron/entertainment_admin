<div class="modal fade" id="gameSlotModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-coins"></i> Create Slot Combo Reward</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['slotComboReward.save'], 'method' => 'POST','id' => 'slotComboRewardSaveForm']) !!}
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            {{ Form::label('reelOne', 'Choose Combo: ', ['class' => 'control-label']) }}
                            <div class="row px-3">
                                <div class="col-md-4 px-1">
                                    {{ Form::select('firstReelId', $firstReels->pluck('name', 'id'), '', ['class' => 'form-control', 'placeholder'=>'Select A Reel','id' => 'reelOne', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-4 px-1">
                                    {{ Form::select('secondReelId', $secondReels->pluck('name', 'id'), '', ['class' => 'form-control', 'placeholder'=>'Select A Reel','id' => 'reelTwo', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-4 px-1">
                                    {{ Form::select('thirdReelId', $thirdReels->pluck('name', 'id'), '', ['class' => 'form-control', 'placeholder'=>'Select A Reel','id' => 'reelThree', 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>

                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" >
                                    <label class="control-label" for="betType">Bet Type:<span class="text-danger show_desc">*</span></label>
                                    {{ Form::select('betTypeId', $slotBets->pluck('coin_usage', 'id'), '', ['class' => 'form-control betType', 'placeholder'=>'Select A Bet Type','id' => 'betType', 'required' => 'required']) }}
                                </div>
                            </div>
                        </div> -->

                        <div class="row">
                            <div class="col-md-12 d-flex flex-row justify-content-between pt-2">
                                <div class="form-group">
                                    <label class="control-label">Win Type:</label>
                                    <select class="form-control" id="win_type" name="win_type">
                                        <option selected value="1">Normal Win</option>
                                        <option value="2">Mega Win</option>
                                        <option value="3">Super Win</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Reward Coin: <span class="text-danger show_desc">*</span></label>
                                    <input type="number" name="reward_coin" id="reward_coin" class="form-control reward_coin" placeholder="Reward Coin" required>
                                </div>
                            </div>
                        </div>


                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Reward will be: </label>
                                    <input type="number" id="rewards" class="form-control" placeholder="Calculated Rewards" disabled>
                                </div>
                            </div>
                        </div> -->
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn pull-left akoneya-edit-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn akoneya-add-btn" id="saveSlotComboReward">Save</button>
            </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="editGameSlotModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Slot Bet</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&reward_coin;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['slotComboReward.edit'], 'method' => 'PUT','id' => 'editSlotComboRewardSaveForm']) !!}
            @csrf
            <div class="modal-body">
                <input type="hidden" name="id" id="update_id">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            {{ Form::label('reelOne', 'Choose Combo: ', ['class' => 'control-label']) }}
                            <div class="row px-3">
                                <div class="col-md-4 px-1">
                                    {{ Form::select('firstReelId', $firstReels->pluck('name', 'id'), '', ['class' => 'form-control', 'placeholder'=>'Select A Reel','id' => 'update_first_id', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-4 px-1">
                                    {{ Form::select('secondReelId', $secondReels->pluck('name', 'id'), '', ['class' => 'form-control', 'placeholder'=>'Select A Reel','id' => 'update_second_id', 'required' => 'required']) }}
                                </div>
                                <div class="col-md-4 px-1">
                                    {{ Form::select('thirdReelId', $thirdReels->pluck('name', 'id'), '', ['class' => 'form-control', 'placeholder'=>'Select A Reel','id' => 'update_third_id', 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 d-flex flex-row justify-content-between pt-2">
                                <div class="form-group">
                                    <label class="control-label">Win Type:</label>
                                    <select class="form-control" id="update_win_type" name="win_type">
                                        <option selected value="1">Normal Win</option>
                                        <option value="2">Mega Win</option>
                                        <option value="3">Super Win</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" >
                                    <label class="control-label" for="update_bet_type">Bet Type:<span class="text-danger show_desc">*</span></label>
                                    {{ Form::select('betTypeId', $slotBets->pluck('coin_usage', 'id'), '', ['class' => 'form-control', 'placeholder'=>'Select A Bet Type','id' => 'update_bet_type', 'required' => 'required']) }}
                                </div>
                            </div>
                        </div> -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Reward Coins: <span class="text-danger show_desc">*</span></label>
                                    <input type="number" name="reward_coin" id="update_reward_coin" class="form-control" placeholder="Enter Reward Coin" required>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Reward will be: </label>
                                    <input type="number" id="update_rewards" class="form-control update_rewards" placeholder="Calculated Rewards" disabled>
                                </div>
                            </div>
                        </div> -->
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary akoneya-add-btn" id="editSlotComboReward">Update</button>
            </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>
  