@extends('adminlte::page')
@section('content-header-title')
<i class="fa fa-dice mr-2"></i>Slot Game
@stop
@section('content')
<div class="card border-0">
    <ul class="nav nav-tabs akoneya-nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotReel.index') }}" role="tab" aria-controls="custom-tabs-three-home"><span class="nav-title">Slot Reels</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotBet.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Bets</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link active" href="{{ route('slotComboReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Combo Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotRewardType.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Reward Types</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('dailyReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Daily Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotTermsAndCondition.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Terms And Conditions</span></a>
        </li>
    </ul>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane fade active show" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                @if (session('error'))
                    <div class="alert alert-danger" role="alert" id="dialog">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('info'))
                    <div class="alert alert-success" role="alert" id="dialog">
                        {{ session('info') }}
                    </div>
                @endif

                <div class="row justify-content-between px-2">
                    <div class="d-flex flex-column align-middle ml-3">
                        <form action="{{ route('slotComboReward.index') }}" name="search-form" method="GET">
                            <div class="row mb-2">
                                <!-- <div class="col-md-7">
                                    <div class="input-group"> 
                                        {{ Form::text('keyword', request('keyword'), ['class' => 'form-control pull-right','placeholder' => 'Search','autocomplete' => 'off']) }}
                                        <div class="input-group-btn">
                                            <button type="submit" class="btn btn-default akoneya-search-btn">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col">
                                    <div class="input-group">
                                        <select class="form-control reels" id="firstReelId" name="firstReelId">
                                            <option selected="" value="">Select 1st Reel</option>
                                            @foreach ($firstReels as $key => $reel)
                                                <option value="{{$reel->id}}"  @if(request("firstReelId") == $reel->id) selected @endif>{{ $reel->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="input-group">
                                        <select class="form-control reels" id="secondReelId" name="secondReelId">
                                            <option selected="" value="">Select 2nd Reel</option>
                                            @foreach ($secondReels as $key => $reel)
                                                <option value="{{$reel->id}}"  @if(request("secondReelId") == $reel->id) selected @endif>{{ $reel->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="input-group">
                                        <select class="form-control reels" id="thirdReelId" name="thirdReelId">
                                            <option selected="" value="">Select 3rd Reel</option>
                                            @foreach ($thirdReels as $key => $reel)
                                            <option value="{{$reel->id}}"  @if(request("thirdReelId") == $reel->id) selected @endif>{{ $reel->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <select class="form-control reels" id="winType" name="winType">
                                            <option selected value="">Select Win Type</option>
                                            <option value="1" @if(request("winType") == 1) selected @endif>Normal Win</option>
                                            <option value="2" @if(request("winType") == 2) selected @endif>Mega Win</option>
                                            <option value="3" @if(request("winType") == 3) selected @endif>Super Win</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="d-flex flex-column align-middle">
                        <button data-toggle="modal" data-target="#gameSlotModal" class="btn akoneya-add-btn mr-3">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            <span class="add-new-btn-text">{{config('enums.buttonLabel.addNewText')}}</span>
                        </button>
                    </div>
                </div>
                <div class="row justify-content-center slot-container p-2 mx-1 my-2">
                    @php
                        $betArr = [];
                        foreach($slotBets as $key => $bet) {
                            $adminUse = $slotAllComboRewards->where('slot_bet_type_id', $bet->id)->sum('reward_coin');
                            $usedPercent = 100 - (($adminUse / $bet->limit_reward_amount) * 100);
                            $betArr[$bet->coin_usage] = [
                                'id' => $bet->id,
                                'usedPercent' => number_format($usedPercent, 2, '.', '')
                            ];
                        }
                    @endphp
                    @foreach ($betArr as $key => $bet)
                    <div class="d-flex flex-column align-middle slot-card mx-2 px-4 py-2">
                        <p class="text-center mb-0">
                            [ Bet <strong>{{ $key }}</strong> ] -
                            <strong class="text-red">  {{ $bet['usedPercent'] }} %</strong>
                        </p>
                    </div>
                    @endforeach
                </div>
                <div class="table-responsive">
                    <div class="table-responsive">
                        <table id="catetbl" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <td class="text-center text-uppercase"><strong>No</strong></td>
                                    <td class="text-center text-uppercase" colspan="3"><strong>Combo</strong></td>
                                    <!-- <td class="text-center text-uppercase"><strong>Bet Type</strong></td> -->
                                    <td class="text-center text-uppercase">
                                        <strong>Rewards</strong>
                                        <div class="row mx-1">
                                            @foreach ($slotBets as $key => $bet)
                                                <div class="col d-flex flex-row justify-content-center pt-2 border border-left-0 border-right-0 px-2">
                                                    <i class="fa fa-coins"></i>&nbsp;<h5>{{$bet->coin_usage}}</h5>
                                                </div>
                                            @endforeach
                                        </div>
                                    </td>
                                    <td class="text-center text-uppercase">
                                        <strong>Win Type</strong>
                                    </td>
                                    <td class="text-center text-uppercase">
                                        <div class="d-flex flex-column align-items-center">
                                            <strong>Actions</strong>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($slotComboRewards as $key => $slotComboReward)
                                    <tr>
                                        <td class="text-center">{{$key + 1}}</td>
                                        <td class="text-center">
                                            <div class="d-flex flex-row justify-content-center">
                                                @if (isset($slotComboReward->combo->firstReel) && $slotComboReward->combo->firstReel->image)
                                                    <div class="slotRewardReel-img" data-toggle="tooltip" data-placement="top" title="{{$slotComboReward->combo->firstReel->name}}">
                                                        <img src="{{ getImageFromAkoneyaMedia($slotComboReward->combo->firstReel->image) }}" alt="" title="{{$slotComboReward->combo->firstReel->name}}">
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex flex-row justify-content-center">
                                                @if (isset($slotComboReward->combo->secondReel) && $slotComboReward->combo->secondReel->image)
                                                    <div class="slotRewardReel-img" data-toggle="tooltip" data-placement="top" title="{{$slotComboReward->combo->secondReel->name}}">
                                                        <img src="{{ getImageFromAkoneyaMedia($slotComboReward->combo->secondReel->image) }}" alt="" title="{{$slotComboReward->combo->secondReel->name}}">
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex flex-row justify-content-center">
                                                @if (isset($slotComboReward->combo->thirdReel) && $slotComboReward->combo->thirdReel->image)
                                                    <div class="slotRewardReel-img" data-toggle="tooltip" data-placement="top" title="{{$slotComboReward->combo->thirdReel->name}}">
                                                        <img src="{{ getImageFromAkoneyaMedia($slotComboReward->combo->thirdReel->image) }}" alt="" title="{{$slotComboReward->combo->thirdReel->name}}">
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-center p-2">
                                            <div class="row mx-1">
                                                @foreach ($slotBets as $key => $bet)
                                                    @php 
                                                        $betReward = $slotAllComboRewards->where('slot_bet_type_id', $bet->id)->where('slot_combo_id', $slotComboReward->slot_combo_id)->first();
                                                    @endphp
                                                    <div class="col d-flex flex-row justify-content-between pt-2 text-danger mx-2 slot-card">
                                                        <i class="fa fa-gift"></i><p class="text-bold mb-1">{{$betReward->reward_coin}}</p>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex flex-row justify-content-center slot-card">
                                                @if ($slotComboReward->win_type == 3)
                                                    <p class="py-2 mb-0 text-bold">SUPER</p>
                                                @elseif ($slotComboReward->win_type == 2)
                                                    <p class="py-2 mb-0 text-bold">MEGA</p>
                                                @else
                                                    <p class="py-2 mb-0 text-bold">NORMAL</p>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <button
                                                class="btn btn-sm action-btn akoneya-edit-btn"
                                                data-toggle="modal" 
                                                data-target="#editGameSlotModal" 
                                                data-update_id="{{$slotComboReward->id}}"
                                                data-update_first_id="{{ $slotComboReward->combo->firstReel->id ?? '' }}"
                                                data-update_second_id="{{ $slotComboReward->combo->secondReel->id ?? '' }}"
                                                data-update_third_id="{{ $slotComboReward->combo->thirdReel->id ?? ''}}"
                                                data-update_bet_type_id="{{ $slotComboReward->bet->id ?? ''}}"
                                                data-update_reward_coin="{{ $slotComboReward->reward_coin }}"
                                                data-update_win_type="{{ $slotComboReward->win_type }}"
                                            >
                                                <i class="fa fa-edit"></i><span class="pl-1">Edit</span>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-sm action-btn akoneya-delete-btn" data-toggle="modal" data-target="#deleteModal" data-id="{{$slotComboReward->slot_combo_id}}">
                                                <i class="fa fa-trash"></i><span class="pl-1">Delete<span>
                                            </button>
                                        </td>
                                        @include('layouts.delete',['route'=>'slotComboReward.delete'])
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="pagination">
                            {{ $slotComboRewards->appends($_GET)->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('slotGame.slotComboReward.form')
@endsection

@section('js')
<script>

    $('.reels').on('change', function() {
        document.forms["search-form"].submit();
    });

    $('.reset').on('click', function() {
        document.forms["search-form"].reset();
    });

    $('#saveSlotComboReward').click(function() {
        $('#slotComboRewardSaveForm').submit();
    });

    //Form Validation 
    $('#slotComboRewardSaveForm').validate({
        ignore: [],
        errorElement: "span",
        errorClass: "error-help-block",
        rules: {
            firstReelId: {
                required: true,
            },
            secondReelId: {
                required: true,
            },
            thirdReelId: {
                required: true,
            },
            betTypeId: {
                required: true,
            },
        }
    });

    $('#editSlotComboReward').click(function() {
        $('#editslotComboRewardSaveForm').submit();
    });

    //Form Validation 
    $('#slotComboRewardSaveForm').validate({
        ignore: [],
        errorElement: "span",
        errorClass: "error-help-block",
        rules: {
            firstReelId: {
                required: true,
            },
            secondReelId: {
                required: true,
            },
            thirdReelId: {
                required: true,
            },
            betTypeId: {
                required: true,
            },
        }
    });

    // Edit
    $('#editGameSlotModal').on('show.bs.modal', function(e) {
        var imagePath;
        var button = $(e.relatedTarget);
        modal = $(this);
        console.log(button.data('update_id'));
        modal.find('.modal-body #update_id').val(button.data('update_id'));
        modal.find('.modal-body #update_first_id').val(button.data('update_first_id'));
        modal.find('.modal-body #update_second_id').val(button.data('update_second_id'));
        modal.find('.modal-body #update_third_id').val(button.data('update_third_id'));
        modal.find('.modal-body #update_bet_type').val(button.data('update_bet_type_id'));
        modal.find('.modal-body #update_win_type').val(button.data('update_win_type'));
        // modal.find('.modal-body #update_reward_coin').val(button.data('update_reward_coin'));
        // modal.find('.modal-body #update_bet_type option[value="' + button.data('update_bet_type_id') + '"]').prop("selected", true);
    });

    //Delete
    $('#deleteModal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        var id = button.data('id');
        modal = $(this);
        modal.find('.modal-body #item_id').val(id);
    });

    $('#deletebtn').click(function() {
        $('#deleteFrom').submit();
        $('#deleteModal').modal('hide');
    });

     //Change image
    $('#image').change(function() {
        $('#imgUrl')[0].src = (window.URL ? URL : webkitURL).createObjectURL(this.files[0]);
    });

    $('#newImage').change(function() {
        $('#update_imagepath')[0].src = (window.URL ? URL : webkitURL).createObjectURL(this.files[0]);
    });

</script>
@endsection