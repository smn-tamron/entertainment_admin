@extends('adminlte::page')
@section('content-header-title')
    <i class="fa fa-dice mr-2"></i>Slot Game
@stop
@section('content')
<div class="card border-0">
    <ul class="nav nav-tabs akoneya-nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link border-left-0 active" href="{{ route('slotReel.index') }}" role="tab" aria-controls="custom-tabs-three-home"><span class="nav-title">Slot Reels</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotBet.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Bets</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotComboReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Combo Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotRewardType.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Reward Types</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('dailyReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Daily Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotTermsAndCondition.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Terms And Conditions</span></a>
        </li>
    </ul>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane fade active show" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                @if (session('error'))
                    <div class="alert alert-danger" role="alert" id="dialog">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('info'))
                    <div class="alert alert-success" role="alert" id="dialog">
                        {{ session('info') }}
                    </div>
                @endif
                <div class="row mb-4">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button data-toggle="modal" data-target="#gameSlotModal"
                            class="btn btn-link role-create akoneya-add-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i><span class="add-new-btn-text">{{config('enums.buttonLabel.addNewText')}}</span></button>
                    </div>
                </div>
                <div class="row d-flex flex-row justify-content-center px-4">
                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table id="catetbl" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center text-uppercase">No</th>
                                        <th class="text-center text-uppercase">Image</th>
                                        <th class="text-center text-uppercase">Name</th>
                                        <th class="text-center text-uppercase">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($slotReels as $key => $slotReel)
                                        <tr>
                                            <td class="text-center">{{$key + 1}}</td>
                                            <td>
                                                @if ($slotReel->image)
                                                    <!-- <div class="slotReel-img">
                                                        <img src="{{ getImageFromAkoneyaMedia($slotReel->image) }}">
                                                    </div> -->
                                                    <div class="d-flex flex-row justify-content-center">
                                                        <div class="slotRewardReel-img" data-toggle="tooltip" data-placement="top" title="{{$slotReel->name}}">
                                                            <img src="{{ getImageFromAkoneyaMedia($slotReel->image) }}" alt="" title="{{$slotReel->name}}">
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                            <td class="text-center">{{$slotReel->name}}</td>
                                            <td>
                                                <button data-toggle="modal" data-target="#editGameSlotModal" class="btn btn-sm action-btn akoneya-edit-btn" data-update_id="{{$slotReel->id}}" data-update_image="{{ $slotReel->image }}"
                                                    data-update_imagepath="{{ getImageFromAkoneyaMedia($slotReel->image) }}" data-update_name="{{ $slotReel->name }}">
                                                    <i class="fa fa-edit"></i><span class="pl-1">Edit</span>
                                                </button>
                                                <button type="button" class="btn btn-danger btn-sm action-btn" data-toggle="modal" data-target="#deleteModal" data-id={{$slotReel->id}}>
                                                    <i class="fa fa-trash"></i><span class="pl-1">Delete<span>
                                                </button>
                                            </td>
                                            @include('layouts.delete',['route'=>'slotReel.delete'])
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="pagination">
                            {{ $slotReels->appends($_GET)->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('slotGame.slotReel.form')
@endsection

@section('js')
<script>

    $('#saveSlotReel').click(function() {
        $('#slotReelSaveForm').submit();
    });

    //Form Validation 
    $('#slotReelSaveForm').validate({
        ignore: [],
        errorElement: "span",
        errorClass: "error-help-block",
        rules: {
            name: {
                required: true,
            },
            image: {
                required: true,
            },
        }
    });

    $('#editSlotBet').click(function() {
        $('#editSlotReelSaveForm').submit();
    });

    //Form Validation 
    $('#editSlotReelSaveForm').validate({
        ignore: [],
        errorElement: "span",
        errorClass: "error-help-block",
        rules: {
            name: {
                required: true,
            },
        }
    });

    // Edit
    $('#editGameSlotModal').on('show.bs.modal', function(e) {
        var imagePath;
        var button = $(e.relatedTarget);
        var updateImg = button.data('update_image');
        if (!updateImg) {
            imagePath = "{{ asset('img/logo.jpg') }}";
        } else {
            imagePath = button.data('update_imagepath');
        }
        modal = $(this);
        modal.find('.modal-body #update_id').val(button.data('update_id'));
        modal.find('.modal-body #update_image').val(updateImg);
        modal.find('.modal-body #update_name').val(button.data('update_name'));
        modal.find('.modal-body #update_imagepath').attr("src", imagePath);

    });

    //Delete
    $('#deleteModal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        var id = button.data('id');
        modal = $(this);
        modal.find('.modal-body #item_id').val(id);
    });

    $('#deletebtn').click(function() {
        $('#deleteFrom').submit();
        $('#deleteModal').modal('hide');
    });

     //Change image
    $('#image').change(function() {
        $('#imgUrl')[0].src = (window.URL ? URL : webkitURL).createObjectURL(this.files[0]);
    });

    $('#newImage').change(function() {
        $('#update_imagepath')[0].src = (window.URL ? URL : webkitURL).createObjectURL(this.files[0]);
    });

</script>
@endsection