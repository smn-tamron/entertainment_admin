<div class="modal fade" id="gameSlotModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create Slot Reel</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['slotReel.save'], 'method' => 'POST','id' => 'slotReelSaveForm', 'files' => true]) !!}
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    {!! Form::text('name','', ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="locationImg">
                                    <label for="image">Image</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <img src="{{ asset('img/logo.jpg') }}" id="imgUrl" alt="image">
                                            {!! Form::file('image', ['id' => 'image', 'accept'=>'image/png,image/gif,image/jpeg']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary akoneya-add-btn" id="saveSlotReel">Save</button>
            </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="editGameSlotModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Slot Bet</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['slotReel.edit'], 'method' => 'PUT','id' => 'editSlotReelSaveForm', 'files' => true]) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="id" id="update_id">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    {!! Form::text('name','', ['class' => 'form-control', 'id' => 'update_name', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="locationImgUpdate">
                                    <label for="image">Image</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <img src="" id="update_imagepath">
                                            {{ Form::hidden('old_image','',['id' => 'update_image']) }}
                                            {!! Form::file('new_image', ['id' => 'newImage', 'accept'=>'image/png,image/gif,image/jpeg']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary akoneya-add-btn" id="editSlotReel">Update</button>
            </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>
  