<div class="modal fade" id="gameSlotModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create Slot Bet</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['slotBet.save'], 'method' => 'POST','id' => 'slotBetSaveForm']) !!}
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-lg-6 control-label">Coin Usage<span class="text-danger show_desc">*</span></label>
                                    <div class="col-md-12">
                                        <input type="number" name="coin_usage" id="coin_usage" class="form-control" placeholder="Enter Coin Usage">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-lg-6 control-label">Limit Reward Amount<span class="text-danger show_desc">*</span></label>
                                    <div class="col-md-12">
                                        <input type="number" name="limit_reward_amount" id="limit_reward" class="form-control" placeholder="Enter Limit Reward Amount">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary akoneya-add-btn" id="saveSlotBet">Save</button>
            </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="editGameSlotModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Slot Bet</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['slotBet.edit'], 'method' => 'PUT','id' => 'editSlotBetSaveForm']) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="id" id="update_id">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-lg-6 control-label">Coin Usage<span class="text-danger show_desc">*</span></label>
                                    <div class="col-md-12">
                                        <input type="number" name="coin_usage" id="update_coin_usage" class="form-control" placeholder="Enter Coin Usage">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-lg-6 control-label">Limit Reward Amount<span class="text-danger show_desc">*</span></label>
                                    <div class="col-md-12">
                                        <input type="number" name="limit_reward_amount" id="update_limit_reward" class="form-control" placeholder="Enter Limit Reward Amount">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary akoneya-add-btn" id="editSlotBet">Update</button>
            </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>
  