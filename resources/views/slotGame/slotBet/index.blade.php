@extends('adminlte::page')
@section('content-header-title')
    <i class="fa fa-dice mr-2"></i>Slot Game
@stop
@section('content')
<div class="card border-0">
    <ul class="nav nav-tabs akoneya-nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link border-left-0" href="{{ route('slotReel.index') }}" role="tab" aria-controls="custom-tabs-three-home"><span class="nav-title">Slot Reels</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link active" href="{{ route('slotBet.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Bets</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotComboReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Combo Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotRewardType.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Reward Types</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('dailyReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Daily Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotTermsAndCondition.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Terms And Conditions</span></a>
        </li>
    </ul>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane fade active show" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                @if (session('error'))
                    <div class="alert alert-danger" role="alert" id="dialog">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('info'))
                    <div class="alert alert-success" role="alert" id="dialog">
                        {{ session('info') }}
                    </div>
                @endif
                <div class="row mb-4">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button data-toggle="modal" data-target="#gameSlotModal"
                            class="btn btn-link role-create akoneya-add-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i><span class="add-new-btn-text">{{config('enums.buttonLabel.addNewText')}}</span></button>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="table-responsive">
                        <table id="catetbl" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center text-uppercase">No</th>
                                    <th class="text-center text-uppercase">Coin Usage</th>
                                    <th class="text-center text-uppercase">Limit Reward Amount</th>
                                    <th class="text-center text-uppercase">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($slotBets as $key => $slotBet)
                                    <tr>
                                        <td class="text-center">{{$key + 1}}</td>
                                        <td>
                                            <div class="d-flex flex-row justify-content-center pt-2 mx-2 slot-card">
                                                <i class="fa fa-coins mx-2"></i><p class="mx-2 mb-1">{{$slotBet->coin_usage}}</p>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-row justify-content-center text-danger pt-2 mx-2 slot-card">
                                                <i class="fa fa-gift mx-2"></i><p class="mx-2 text-bold mb-1">{{$slotBet->limit_reward_amount}}</p>
                                            </div>
                                        </td>
                                        <td>
                                            <button data-toggle="modal" data-target="#editGameSlotModal" class="btn btn-sm action-btn akoneya-edit-btn" data-update_id={{$slotBet->id}} data-update_coin_usage={{$slotBet->coin_usage}} data-update_limit_reward={{$slotBet->limit_reward_amount}}>
                                                <i class="fa fa-edit"></i><span class="pl-1">Edit</span>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-sm action-btn" data-toggle="modal" data-target="#deleteModal" data-id={{$slotBet->id}}>
                                                <i class="fa fa-trash"></i><span class="pl-1">Delete<span>
                                            </button>
                                        </td>
                                        @include('layouts.delete',['route'=>'slotBet.delete'])
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="pagination">
                            {{ $slotBets->appends($_GET)->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('slotGame.slotBet.form')
@endsection

@section('js')
<script>

    $('#saveSlotBet').click(function() {
        $('#slotBetSaveForm').submit();
    });

    //Form Validation 
    $('#slotBetSaveForm').validate({
        ignore: [],
        errorElement: "span",
        errorClass: "error-help-block",
        rules: {
            coin_usage: {
                required: true,
            },
            limit_reward_amount: {
                required: true,
            },
        }
    });

    $('#editSlotBet').click(function() {
        $('#editSlotBetSaveForm').submit();
    });

    //Form Validation 
    $('#editSlotBetSaveForm').validate({
        ignore: [],
        errorElement: "span",
        errorClass: "error-help-block",
        rules: {
            coin_usage: {
                required: true,
            },
            limit_reward_amount: {
                required: true,
            },
        }
    });

    // Edit
    $('#editGameSlotModal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        modal = $(this);
        modal.find('.modal-body #update_id').val(button.data('update_id'));
        modal.find('.modal-body #update_coin_usage').val(button.data('update_coin_usage'));
        modal.find('.modal-body #update_limit_reward').val(button.data('update_limit_reward'));
    });

    //Delete
    $('#deleteModal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        var id = button.data('id');
        modal = $(this);
        modal.find('.modal-body #item_id').val(id);
    });
    $('#deletebtn').click(function() {
        $('#deleteFrom').submit();
        $('#deleteModal').modal('hide');
    });

</script>
@endsection