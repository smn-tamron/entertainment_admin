<div class="modal fade" id="gameSlotModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create New Reward Type</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['slotRewardType.save'], 'method' => 'POST','id' => 'saveForm']) !!}
            @csrf
            <input type="hidden" name="game_category_id" value="{{config('enums.gameCategory.slotReel')}}">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="type">Reward Type</label>
                                    {!! Form::text('type','', ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="reward_coin">Reward Coin</label>
                                    {!! Form::number('reward_coin','', ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary akoneya-add-btn" id="saveBtn">Save</button>
            </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="editModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Reward Coin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => ['slotRewardType.edit'], 'method' => 'PUT','id' => 'editForm']) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="game_category_id" value="{{config('enums.gameCategory.slotReel')}}">
                        <input type="hidden" name="id" id="update_id">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="type">Reward Type</label>
                                    {!! Form::text('type','', ['class' => 'form-control', 'id' => 'update_type', 'autocomplete' => 'off', 'disabled']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="locationImgUpdate">
                                    <label for="reward_coin">Reward Coin</label>
                                        {!! Form::number('reward_coin','', ['class' => 'form-control', 'id' => 'update_reward_coin', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left akoneya-edit-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary akoneya-add-btn" id="editBtn">Update</button>
            </div>
            {!! Form::close() !!}
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>
  