@extends('adminlte::page')
@section('content-header-title')
<i class="fa fa-dice mr-2"></i>Slot Game
@stop
@section('content')
<div class="card border-0">
    <ul class="nav nav-tabs akoneya-nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link border-left-0" href="{{ route('slotReel.index') }}" role="tab" aria-controls="custom-tabs-three-home"><span class="nav-title">Slot Reels</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotBet.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Bets</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotComboReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Combo Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link active" href="{{ route('slotRewardType.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Reward Types</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('dailyReward.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Slot Daily Rewards</span></a>
        </li>
        <li class="nav-item akoneya-nav-item">
            <a class="nav-link akoneya-nav-link" href="{{ route('slotTermsAndCondition.index')}}" role="tab" aria-controls="custom-tabs-three-profile"><span class="nav-title">Terms And Conditions</span></a>
        </li>
    </ul>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane fade active show" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                @if (session('error'))
                    <div class="alert alert-danger" role="alert" id="dialog">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('info'))
                    <div class="alert alert-success" role="alert" id="dialog">
                        {{ session('info') }}
                    </div>
                @endif
                <div class="row mb-4">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button data-toggle="modal" data-target="#gameSlotModal"
                            class="btn btn-link role-create akoneya-add-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i><span class="add-new-btn-text">{{config('enums.buttonLabel.addNewText')}}</span></button>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="table-responsive">
                        <table id="catetbl" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-center">No</th>
                                    <th class="text-uppercase text-center">Reward Type</th>
                                    <th class="text-uppercase text-center">Reward Coin</th>
                                    <th class="text-uppercase text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rewards as $key => $reward)
                                    <tr>
                                        <td class="text-center">{{$key + 1}}</td>
                                        <td class="text-capitalize text-center">{{$reward->type}}</td>
                                        <td>
                                            <div class="d-flex flex-row justify-content-center text-danger pt-3 mx-2 slot-card">
                                                <i class="fa fa-gift mx-2"></i><h5 class="mx-2 text-bold">{{$reward->reward_coin}}</h5>
                                            </div>
                                        </td>
                                        <td>
                                            <button data-toggle="modal" data-target="#editModal" class="btn btn-sm action-btn akoneya-edit-btn" 
                                                data-update_id="{{$reward->id}}"
                                                data-update_type="{{ $reward->type }}"
                                                data-update_reward_coin="{{ $reward->reward_coin }}">
                                            <i class="fa fa-edit"></i><span class="pl-1">Edit</span>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-sm action-btn" data-toggle="modal" data-target="#deleteModal" data-id="{{$reward->id}}">
                                                <i class="fa fa-trash"></i><span class="pl-1">Delete<span>
                                            </button>
                                        </td>
                                        @include('layouts.delete',['route'=>'slotRewardType.delete'])
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('slotGame.slotRewardType.form')
@endsection

@section('js')
<script>

    $('#saveBtn').click(function() {
        $('#saveForm').submit();
    });

    //Form Validation 
    $('#saveForm').validate({
        ignore: [],
        errorElement: "span",
        errorClass: "error-help-block",
        rules: {
            type: {
                required: true,
            },
            reward_coin: {
                required: true,
            },
        }
    });

    $('#editBtn').click(function() {
        $('#editForm').submit();
    });

    //Form Validation 
    $('#editForm').validate({
        ignore: [],
        errorElement: "span",
        errorClass: "error-help-block",
        rules: {
            reward_coin: {
                required: true,
            },
        }
    });

    // Edit
    $('#editModal').on('show.bs.modal', function(e) {

        var button = $(e.relatedTarget);
        modal = $(this);
        modal.find('.modal-body #update_id').val(button.data('update_id'));
        modal.find('.modal-body #update_type').val(button.data('update_type'));
        modal.find('.modal-body #update_reward_coin').val(button.data('update_reward_coin'));

    });

    //Delete
    $('#deleteModal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        var id = button.data('id');
        modal = $(this);
        modal.find('.modal-body #item_id').val(id);
    });

    $('#deletebtn').click(function() {
        $('#deleteFrom').submit();
        $('#deleteModal').modal('hide');
    });


</script>
@endsection