<div id="deleteModal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Are you sure?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <div class="modal-body">
                <p>Do you really want to delete this record permanently?</p>
                {!! Form::open(['route' => $route, 'method' => 'delete', 'id' => 'deleteFrom']) !!}

                <input type="hidden" name="id" id="item_id" value="">
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn akoneya-edit-btn" data-dismiss="modal">Cancel</button>
                <a href="" id="url"><button type="submit" class="btn btn-danger" id="deletebtn">Delete</button></a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
