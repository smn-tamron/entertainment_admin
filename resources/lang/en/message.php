<?php

return [
    'create_success_msg' => 'Saved successfully.',
    'edit_success_msg' => 'Updated successfully.',
    'delete_success_msg' => 'Deleted successfully.',
    'status_change_msg' => 'Status change successfully.',
    'apiServerDown' => 'Api Server is down.',
    'dbServerDown' => 'DB Server is down.',
    'adminServerDown' => 'DB Server is down.'

];
