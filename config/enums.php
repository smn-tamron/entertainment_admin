<?php
return [
    'loginTypes' => [
        'email' => 'email',
        'phone' => 'phone'
    ],
    'enableActiveStatus'=>'active',
    "fakeProfile" => "uaa/profile/fake-profile.png",
    'homePageImgSlider' => 'public/imgs/news/sliders',
    'mainUrl' => env('APP_URL'),
    'itemPerPage' => 10,
    'buttonLabel'=>[
        'addNewText' => 'Add New',
        'uploadNewImage' => 'New Upload Image',
        'viewDetail' => 'View',
        'searchData' => 'Search',
        'viewSurvey' => 'Survey',
        'viewSurveyVoucher' => 'Voucher'
    ],
    'statusTableHeader'=>[
        'publishedStatus'=>'Published Status',
        'banStatus'=>'Ban Status',
        'fetchStatus'=>'Fetch Status',
        'customerAgreeStatus'=>'Customer Agree Status',
    ],
    'disableDeletedStatus' => 0,
    'enableDeletedStatus' => 1,
    '' => 1,
    'firstSequenceNumber'=> 1,
    'gameCategory' => [
        "covid" => 1,
        "slotReel" => 2,
    ],
    'gameUser' => [
        "guest" => "Guest",
        "register" => "Registered"
    ],
    "guestUser" => "guest",
    'winStatus' => [
        "win"=> 1,
        "loss"=> 0
    ]
];
