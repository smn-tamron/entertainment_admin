<?php
return [
    'alertSuccess' => 'success',
    'alertInfo' => 'info',
    'alertDanger' => 'danger',
    'alertError' => 'error',
    'saveSuccessMsg' => 'Saved successfully.',
    'editSuccessMsg' => 'Updated successfully.',
    'deleteSuccessMsg' => 'Deleted successfully.',
    'statusChangeMsg' => 'Changed successfully.',
    'alreadyEmailExist' => 'The email has already been taken.',
    'invalidCurrentPsw' => 'Invalid current password.',
    'successChangePsw' => 'Changed password successfully.',
    'invalidFileSize' => 'File size must be less than 5Mb.',
    'alreadyNameExist' => 'This Name is already exist.',
    'alreadyRewardTypeExist' => 'This Reward Type has already been used.',
    'notFoundCombo' => 'Combo was not found.',
    'notAllowtoEdit' => 'Combo not Allow! You will need to keep same combo.',
];
