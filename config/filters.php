<?php

return [

    "activeStatus" => [
        [
            "name" => "All",
            "value" => ""
        ],
        [
            "name" => "Active",
            "value" => "active"
        ],
        [
            "name" => "Pending",
            "value" => "pending"
        ]
    ],
    "availableStatus" => [
        [
            "name" => "All products",
            "value" => ""
        ],
        [
            "name" => "Available",
            "value" => "available"
        ],
        [
            "name" => "Sold out",
            "value" => "soldOut"
        ]
    ],
    "sellingType" => [
        [
            "name" => "For Rent",
            "value" => "forRent"
        ],
        [
            "name" => "For Sale",
            "value" => "forSale"
        ]
    ],
    "currencyType" => [
        [
            "name" => "MMK",
            "value" => "mmk"
        ],
        [
            "name" => "USD",
            "value" => "usd"
        ]
    ],
    "transactionStatus" => [
        [
            "name" => "Pending",
            "value" => "pending"
        ],
        [
            "name" => "Done",
            "value" => "done"
        ],
        [
            "name" => "Rejected",
            "value" => "rejected"
        ]
    ],
    "holidayStatus" => [
        [
            "name" => "Public Holiday",
            "value" => "holiday"
        ],
        [
            "name" => "Special Day",
            "value" => "specialday"
        ]
    ],
    "userTypes" => [
        [
            "name" => "Guest",
            "value" => "guest"
        ],
        [
            "name" => "Registered",
            "value" => "registered"
        ]
    ],
    "winStatus" => [
        [
            "name" => "Win",
            "value" => "1"
        ],
        [
            "name" => "Loss",
            "value" => "0"
        ]
    ],
];
