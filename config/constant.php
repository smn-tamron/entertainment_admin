<?php
return [ 
    'image' => 'image',
    'images' => 'images',
    'validateImages' => 'images.*',
    'newImage' => 'new_image',
    'newImages' => 'new_images',
    'validateNewImages' => 'new_images.*',
    'mediaLogo' => 'media_logo',
    'newMediaLogo' => 'new_media_logo',
    'bankLogo' => 'bank_logo',
    'providerLogo' => 'provider_logo',
];