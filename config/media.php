 
<?php

return [
	'local' => [
		'storagePath' => env('LOCAL_MEDIA_STORAGE_PATH', '/media/akoneya'),
		'secondsToCache' => env('LOCAL_MEDIA_CACHE_SECONDS', 600)
	],
	"news" => [
		"sliders" => [
			"directory" => "news/sliders"
		],
		"media" => [
			"directory" => "news/media"
		]
	],
	"rates" => [
		"banks" => [
			"directory" => "rates/exchanges/banks"
		],
		"fuelproviders" => [
			"directory" => "rates/fuelprices/fuelproviders"
		],
		"locations" => [
			"directory" => "rates/goldprices/locations"
		],
		"internetproviders" => [
			"directory" => "rates/internetprices/internetproviders"
		],
		""
	],
	'privacyName' => [
		1 => 'Public',
		2 => 'Friends',
		3 => 'Only me'
	],
	'mediaTypeName' => [
		1 => 'image',
		2 => 'video'
	],
	'mediaType' => [
		'image'=> 1,
		'video'=> 2
	],
	"directories" => [
		"categories" => [
			"directory" => "directories/categories"
		],
		"travel" => [
			"directory" => "directories/travelBlogs"
		],
		"food" => [
			"directory" => "directories/foodBlogs"
		]
	],
	"vendor" => [
		"profile" => "vendors/userProfile",
		"businessLogo" => "vendors/businessLogos",
		"licensePhoto" => "vendors/licensePhotos",
		"houseKeepingImage" => "vendors/houseKeepingImages"
	],
	"carGate" => [
		"directory" => "carGates"
	],
	"authorizedHK" => [
		"subServiceCat" => "lifeStyle/housekeeping"
	],
	"brandnewcars" => [
		"cars" => [
			"directory" => "brandnewcars/cars"
		],
		"brands" => [
			"directory" => "brandnewcars/brands"
		]
	],
	"foodShop"	=> [
		"directory" => "lifeStyle/foodshops"
	],
	'airCon' => [
        'directory' => 'lifeStyle/airCon',
	],
	"appTutorial" => [
		"photos" => [
			"directory" => "uaa/apptutorials/photos"
		],
		"videos" => [
			"directory" => "uaa/apptutorials/videos"
		]
	],
	"rider" => [
		"profile" => "riders/profiles",
	],
	"coupon" => [
		"sliders" => [
			"directory" => "coupon/sliders"
		],
	],
	"games" => [
		"slotReels" => [
			"directory" => "games/slotReels"
		]
	],
];
