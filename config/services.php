<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'facebook' => [
        'app_id' => env('FACEBOOK_APP_KEY'),
        'app_secret' => env('FACEBOOK_APP_SECRET'),
        'default_graph_version' => env('FACEBOOK_DEFAULT_GRAPH_VERSION','v2.5'),
        'access_token' => env('FACEBOOK_ACCESS_TOKEN'),
        'page_id' => env('FACEBOOK_PAGE_ID')
    ],
    'googleMap' => [
        'api_key' => env('GOOGLE_MAP_API_KEY')
    ],
    'viber' => [
        'msgApi' => env('VIBER_MSG_API',"https://chatapi.viber.com/pa/send_message"),
        'authToken' => env('VIBER_AUTH_TOKEN',"4d625bd32567dc3d-b7c73f7becf08a32-617b8f65a5e32f5a"),

    ],
    'server' => [
        'apiServer' => env('API_SERVER_HOST',"47.241.192.207"),
        'dbServer' => env('DB_SERVER_HOST',"47.241.198.101"),
        'adminServer'=> env('ADMIN_SERVER_HOST',"47.241.122.109"),
    ],
    'akoneyaServices' => [
        [
            "name"=>"uaa","url"=>  env('UAA_URL',"http://dev1.tamrontech.com/akoneya/uaa/api/v1.0/checkServiceStatus")
        ],
        [
            "name"=>"news","url" => env('NEWS_URL',"http://dev1.tamrontech.com/akoneya/news/api/v1.0/checkServiceStatus")
        ],
        [
            "name"=>"jobs","url"=> env('JOB_URL',"http://dev1.tamrontech.com/akoneya/job/api/v1.0/checkServiceStatus")
        ],
        [
            "name"=>'shopping',"url"=> env('SHOP_URL',"http://dev1.tamrontech.com/akoneya/shopping/api/v1.0/checkServiceStatus")
        ],
        [
            "name"=>"newsfeed","url"=> env('NEWSFEED_URL',"http://dev1.tamrontech.com/akoneya/newsfeed/api/v1.0/checkServiceStatus")
        ],
        [
            "name"=>"lifestyle","url"=> env('LIFESTYLE_URL',"http://dev1.tamrontech.com/akoneya/lifestyle/api/v1.0/checkServiceStatus")
        ],
        [
            "name"=>'rate',"url"=> env('RATE_URL',"http://dev1.tamrontech.com/akoneya/rates/api/v1.0/checkServiceStatus")
        ],
        [
            "name"=>"directory","url"=> env('DIRECTORY_URL',"http://dev1.tamrontech.com/akoneya/directory/api/v1.0/checkServiceStatus")
        ],
        [
            "name"=>"chat","url"=> env('CHAT_URL',"http://dev1.tamrontech.com/akoneya/chat/api/v1.0/checkServiceStatus")
        ],
        [
            "name"=>"vendor","url"=> env('VENDOR_URL',"http://dev1.tamrontech.com/akoneya/vendor/api/v1.0/checkServiceStatus")
        ],
        [
            "name"=>'siman',"url"=> env('SIMAN_URL',"http://dev1.tamrontech.com/akoneya/admin/public/api/v1.0/checkServiceStatus")
        ]
    ]

];
