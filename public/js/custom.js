$('#image').change(function (e) {
    $('#imgUrl')[0].src = URL.createObjectURL(e.target.files[0]);
});


function randomString(length) {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz'.split('');

    if (!length) {
        length = Math.floor(Math.random() * chars.length);
    }

    var str = '';
    for (var i = 0; i < length; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
}

function uploadPhoto(clicked_id) {
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#photoUrl_' + clicked_id).attr('src', e.target.result);
    }
    reader.readAsDataURL(document.getElementById(clicked_id).files[0]);
}

$(document).on('click', '.remove-image-btn', function (e) {
    $(this).parent('.remove-image').remove();
    e.preventDefault();
});

$(document).on('click', '.remove-field', function (e) {
    $(this).parent().parent('.remove').remove();
    e.preventDefault();
});
