/** 
 * add active class and stay opened when selected 
*/
var pathName = window.location.href;
var pathArray = pathName.split("/");

$('.sidebar ul.nav-sidebar a').filter(function(e) {
    return checkRoute(this.dataset.route);
}).addClass('active');

$('ul.nav-treeview a').filter(function() {
    return checkRoute(this.dataset.route);
}).addClass("active");

$('ul.nav-treeview a').filter(function() {
    return checkRoute(this.dataset.route);
}).parent().parent().parent().addClass('menu-open');

$('ul.nav-treeview li.menu-open .nav-treeview a').filter(function() {
    return checkRoute(this.dataset.route);
}).parent().parent().parent().parent().parent().addClass('menu-open');

$('ul.nav-treeview li.menu-open .nav-treeview a').filter(function() {
    return checkRoute(this.dataset.route);
}).addClass("active");

function checkRoute(route){
    var routeArray = route.split("/");
    if(routeArray.length > 2) {
        return pathArray.includes(routeArray[1]) && pathArray.includes(routeArray[2]);
    }
    else{
        return pathArray.includes(routeArray[1]);
    }
}