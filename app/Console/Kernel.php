<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Model\ScheduleSetting;

use Signifly\SchedulingTasks\Facades\TaskLoader;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $tasks = ScheduleSetting::all();

        foreach($tasks as $task){
            switch ($task->module_code) {
                case 'R1':
                        $schedule->call('App\Http\Controllers\ViberMonitoring\CheckUpdateInfoController@goldRate')->cron(str_replace(',', ' ', $task->cron_tab));
                    break;
                case 'R2':
                    $schedule->call('App\Http\Controllers\ViberMonitoring\CheckUpdateInfoController@exchangeRate')->cron(str_replace(',', ' ', $task->cron_tab));
                    break;
                case 'R3':
                    $schedule->call('App\Http\Controllers\ViberMonitoring\CheckUpdateInfoController@fuelRate')->cron(str_replace(',', ' ', $task->cron_tab));
                    break;
                case 'N1':
                    $schedule->call('App\Http\Controllers\ViberMonitoring\CheckUpdateInfoController@news')->cron(str_replace(',', ' ', $task->cron_tab));
                    break;
                case 'AP':
                    $schedule->call('App\Http\Controllers\ViberMonitoring\CheckUpdateInfoController@apiServer')->cron(str_replace(',', ' ', $task->cron_tab));
                    break;
                case 'DB':
                    $schedule->call('App\Http\Controllers\ViberMonitoring\CheckUpdateInfoController@dbServer')->cron(str_replace(',', ' ', $task->cron_tab));
                    break;
                case 'AD':
                    $schedule->call('App\Http\Controllers\ViberMonitoring\CheckUpdateInfoController@adminServer')->cron(str_replace(',', ' ', $task->cron_tab));
                    break;
                case 'LO':
                    $schedule->call('App\Http\Controllers\ViberMonitoring\CheckUpdateInfoController@lotteryBatch')->cron(str_replace(',', ' ', $task->cron_tab));
                    break;
                case 'AKONEYA':
                    $schedule->call('App\Http\Controllers\ViberMonitoring\CheckAllServiceController@checkAkoneyaServices')->cron(str_replace(',', ' ', $task->cron_tab));
                    break;
                default:
                    return "";
                    break;
            }
        }
        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
