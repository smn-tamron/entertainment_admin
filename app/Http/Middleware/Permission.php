<?php

namespace App\Http\Middleware;
use App\Model\Menu\Menu;
use App\Model\Menu\SubMenu;
use Closure;
use Illuminate\Support\Facades\Session;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->segment(2))
        {
            return $this->checkSubMenu($request,$next);
        }

        if($request->segment(1))
        {
            return $this->checkMenu($request,$next);
        }
        
        return abort(403);
    }

    protected function checkMenu($request,$next)
    {
        $route = $request->segment(1);
        $menu = Menu::where('route','/'.$route)->first();
        if($menu){
            $permissionMenuIds = Session::get('permissionMenuIds');
            if(in_array($menu->id,$permissionMenuIds)) {
                return $next($request);
            }
        }

        return abort(403);
    }

    protected function checkSubMenu($request,$next)
    {
        $route = $request->segment(1)."/".$request->segment(2);
        $subMenu = SubMenu::where('route','like','/'.$route.'%')->first();
        if($subMenu){
            $permissionSubMenuIds = Session::get('permissionSubMenuIds');
            if(in_array($subMenu->id,$permissionSubMenuIds)) {
                return $next($request);
            }
        }
        else{
            return $this->checkMenu($request,$next);
        }

        return abort(403);
    }

}
