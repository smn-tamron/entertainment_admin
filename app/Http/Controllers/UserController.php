<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AdminUser\UserRepository;
use App\Repositories\AdminUser\RoleRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use App\User;
use Hash;

class UserController extends Controller
{
    public function __construct(UserRepository $userRepo, RoleRepository $roleRepo)
    {
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepo;
        $this->itemPerPage = config('enums.itemPerPage');
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->errorAlertMsg = config('alertmessage.alertError');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userRepo->getPaginated($this->itemPerPage);
        $role = $this->roleRepo->getRole();
        return view('users.index', compact('users', 'role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =  validator(request()->all(), [
            'name'=>'required',
            'email' => 'required|unique:users',
            'roleId' => 'required',
            'password'=> 'required|required_with:password_confirmation|min:6',
            'passwordConfirmation'=> 'required|same:password|min:6'
        ]);
        if ($validator->fails()) {
            $error_text="";

            foreach ($validator->errors()->all() as $error) {
                $error_text .= $error;
            }
            return redirect()->route('users.index')->with([$this->errorAlertMsg=>$error_text])->withInput();
        }

        try {
            $data = $request->all();
            $data['role_id'] = $request->roleId;
            $this->userRepo->create($data);
        } catch (\Exception $e) {
            return redirect()->route('users.index')->with([$this->errorAlertMsg=>$e->getMessage()])->withInput();
        }
        return redirect()->route('users.index')->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $checkEmail = $this->userRepo->checkEmailForUpdate($request->email, $request->id);
        if ($checkEmail) {
            return redirect()->route('users.index')->with([$this->errorAlertMsg=>config('alertmessage.alreadyEmailExist')])->withInput();
        }
        $data = $request->all();
        $data['role_id'] = $request->roleId;

        try {
            $this->userRepo->update($data, $data['id']);
        } catch (\Exception $e) {
            return redirect()->route('users.index')->with([$this->errorAlertMsg=>$e->getMessage()])->withInput();
        }
        return redirect()->route('users.index')->with([$this->successAlertMsg => config('alertmessage.editSuccessMsg')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->userRepo->delete($request->id);
        return redirect()->route('users.index')->with([$this->successAlertMsg => config('alertmessage.deleteSuccessMsg')]);
    }

    public function changePassword(Request $request)
    {
        $validator =  validator(request()->all(), [
            'currentPassword' => 'required',
            'newPassword'=> 'required|required_with:newPasswordConfirmation|min:6',
            'newPasswordConfirmation'=> 'required|same:newPassword|min:6'
        ]);
        if ($validator->fails()) {
            $error_text="";

            foreach ($validator->errors()->all() as $error) {
                $error_text .= $error;
            }
            return redirect()->route('home')->with([$this->errorAlertMsg=>$error_text])->withInput();
        }
        $data = $this->userRepo->getById(\Crypt::decrypt($request->id));
        if (!Hash::check($request->currentPassword, $data->password)) {
            return redirect()->route('home')->with([$this->errorAlertMsg=>config('alertmessage.invalidCurrentPsw')])->withInput();
        } else {
            $data->update(['password'=>bcrypt($request->newPassword)]);
            return redirect()->route('home')->with([$this->successAlertMsg => config('alertmessage.successChangePsw')]);
        }
    }

    public function checkUser(Request $request)
    {
        if ($request->email == null && $request->password == null) {
            return response()->json(['valid' => "false"]);
        } else {
            if ($request->email != null) {
                $data = User::where('email', $request->email)->first();
                if (!empty($data)) {
                    if (empty($request->password)) {
                        return response()->json(['valid' => "psw"]);
                    } else {
                        
                        $userdata = User::where('email', $request->email)->first();
                            
                            if(Hash::check($request->password, $userdata->password)) { 
                                $wrongCount = 0;
                                $wrongTime = Cache::put('wrong', $wrongCount , 30);
                                return response()->json(['valid' => "all_success"]);
                            }
                            else{
                                $wrongTime = Cache::increment('wrong', 1 , 30);
                                $value = Cache::get('wrong');
                                return response()->json(['valid' => "error", 'wrong' =>  $value]);
                            }
                    
                    }
                } else {
                    return response()->json(['valid' => "empty"]);
                }
            } 
        }
    }
}
