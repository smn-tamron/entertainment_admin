<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AdminUser\SubMenuRepository;

class SubmenuController extends Controller
{
    public function __construct(SubMenuRepository $submenuRepo)
    {
        $this->submenuRepo = $submenuRepo;
        $this->firstSequenceNumber = config('enums.firstSequenceNumber');
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->errorAlertMsg = config('alertmessage.alertError');
        $this->successStatusAlert = config('alertmessage.alertSuccess');
    }

    public function sort(Request $request)
    {
        foreach ($request->submenuIds as $submenuId) {
            $this->submenuRepo->findAndUpdateSequenceNumber($submenuId, $this->firstSequenceNumber);
            $this->firstSequenceNumber++;
        }
        return response()->json([$this->successStatusAlert => config('alertmessage.statusChangeMsg')]);
    }

    public function store(Request $request)
    {
        $existingSubmenu = $this->submenuRepo->checkSubmenu($request->name, $request->menu_id, null);
        if ($existingSubmenu) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.alreadySubmenuExist')])->withInput();
        }
        $this->submenuRepo->create($request->all());
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }
   
    public function update(Request $request)
    {
        $data =$request->all();
        $existingSubmenu = $this->submenuRepo->checkSubmenu($data['name'], $data['menu_id'], $data['id']);
        if ($existingSubmenu) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.alreadySubmenuExist')])->withInput();
        }
        $this->submenuRepo->update($data, $data['id']);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.editSuccessMsg')]);
    }

    public function destroy(Request $request)
    {
        $this->submenuRepo->delete($request->id);
        $this->submenuRepo->deleteSubmenuPermissionBySubmenuId($request->id);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.deleteSuccessMsg')]);
    }
}
