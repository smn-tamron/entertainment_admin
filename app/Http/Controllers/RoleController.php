<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Model\Menu\SubMenu;
use App\Model\Menu\RoleHasSubMenu;
use App\Repositories\AdminUser\RoleRepository;
use App\Repositories\AdminUser\MenuRepository;
use App\Repositories\AdminUser\SubMenuRepository;

class RoleController extends Controller
{
    public function __construct(RoleRepository $roleRepo, MenuRepository $menuRepo, SubMenuRepository $submenuRepo)
    {
        $this->roleRepo = $roleRepo;
        $this->menuRepo = $menuRepo;
        $this->submenuRepo = $submenuRepo;
        $this->itemPerPage = config('enums.itemPerPage');
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->errorAlertMsg = config('alertmessage.alertError');
    }

    public function index() 
    {
        $roles = $this->roleRepo->index($this->itemPerPage);
        return view('roles.index', compact('roles'));
    }

    public function create()
    {
        $menus = $this->menuRepo->getAllBySequenceNumber();
        return view('roles.create', compact('menus'));
    }

    public function store(Request $request)
    {
        $existingRole = $this->roleRepo->checkRole($request->name,null);
        if ($existingRole) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.alreadyRoleExist')])->withInput();
        }
        $role = $this->roleRepo->create(['name'  => $request->name]);

        if($request->menus != null){
            foreach($request->menus as $menuId => $menu){
                $this->menuRepo->createMenuPermission($role->id, $menuId);
            }
        }
        if($request->sub_menus != null){
            foreach($request->sub_menus as $subMenuId => $subMenu){
                RoleHasSubMenu::create([
                    'role_id'=> $role->id,
                    'sub_menu_id' => $subMenuId
                ]);
            }
        }
       return redirect()->route('role.index')->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    public function edit(Request $request)
    {
        $role = $this->roleRepo->getById($request->id);
        $menus = $this->menuRepo->getAllBySequenceNumber();
        $roleMenuIds = $this->menuRepo->getMenuIdsByRoleId($role->id);
        $roleSubMenuIds = $this->submenuRepo->getSubMenuIdsByRoleId($role->id);
        return view('roles.edit', compact('role','menus','roleMenuIds','roleSubMenuIds'));
    }

    public function update(Request $request)
    {
        $existingRole = $this->roleRepo->checkRole($request->name,$request->id);
        if ($existingRole) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.alreadyCategoryExist')])->withInput();
        }
        $this->roleRepo->updateRole($request->name, $request->id);

        if($request->menus != null){
            $this->menuRepo->deleteMenuPermissionsByRoleId($request->id);
            foreach($request->menus as $menuId => $menu){
                $this->menuRepo->createMenuPermission($request->id, $menuId);
            }
        }
        if($request->sub_menus != null){
            $this->submenuRepo->deleteSubMenuPermissionsByRoleId($request->id);
            foreach($request->sub_menus as $subMenuId => $subMenu){
                RoleHasSubMenu::create([
                    'role_id'=> $request->id,
                    'sub_menu_id' => $subMenuId
                ]);
            }
        }

        return redirect()->route('role.index')->with([$this->successAlertMsg => config('alertmessage.editSuccessMsg')]);
    }

    public function destroy(Request $request)
    {
        $relatedUser = $this->roleRepo->checkRelatedUser($request->id);
        if ($relatedUser) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.hasRelationRole')]);
        } else {
            $this->roleRepo->delete($request->id);
            $this->roleRepo->deleteMenuPermissionByRoleId($request->id);
            $this->roleRepo->deleteSubMenuPermissionByRoleId($request->id);
            return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.deleteSuccessMsg')]);
        }
    }
}
