<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Client\ClientUserRepository;
use App\Filters\ClientUserFilter;
use App\Filters\ClientUserDetailFilter;
use App\Repositories\SlotGame\SlotPlayerResultRepository;
use App\Repositories\SlotGame\SlotBetRepository;
use App\Repositories\SlotGame\SlotComboRewardRepository;

class ClientUserController extends Controller
{
	public function __construct(ClientUserRepository $clientUserRepo, SlotPlayerResultRepository $playerResultRepo, SlotComboRewardRepository $slotComboRewardRepo, SlotBetRepository $slotBetRepo)
	{
        $this->clientUserRepo = $clientUserRepo;
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->successBannedStatusAlert = config('alertmessage.alertSuccess');
        $this->alertMsgError = config('alertmessage.alertError');
        $this->errorAlertMsg = config('alertmessage.alertError');
        $this->playerResultRepo = $playerResultRepo;
        $this->slotComboRewardRepo = $slotComboRewardRepo;
        $this->slotBetRepo = $slotBetRepo;
        $this->itemPerPage = config('enums.itemPerPage');
	}

    public function index(ClientUserFilter $filter)
    {
        $clientUsers = $this->clientUserRepo->getClientUserList($filter);
        $comboRewards = $this->slotComboRewardRepo->getAll();
        $betTypes = $this->slotBetRepo->getAll();
        $userTypes = config('filters.userTypes');
        return view('client.client_users.index',compact('clientUsers','userTypes', 'comboRewards', 'betTypes'));
    }

    public function bannedStatusChange(Request $request)
    {
        $this->clientUserRepo->statusChange($request->bannedStatus, $request->id);
        return response()->json([$this->successBannedStatusAlert => config('alertmessage.statusChangeMsg')]);
    }

    public function show(ClientUserDetailFilter $filter, $id)
    {
        $clientUser = $this->clientUserRepo->getClientUserDetails($id);
        $clientPersonalInformation = $this->clientUserRepo->getClientPersonalInformation($clientUser->id);
        $results = $this->playerResultRepo->getAllByFilterWithPaginationAndUserId($id, $filter, $this->itemPerPage);
        // dd($results);
        $comboRewards = $this->slotComboRewardRepo->getAll();
        $betTypes = $this->slotBetRepo->getAll();
        return view('client.client_users.detail',compact('clientUser','clientPersonalInformation', 'results','comboRewards', 'betTypes'));
    }
}
