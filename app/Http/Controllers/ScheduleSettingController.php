<?php

namespace App\Http\Controllers;

use App\Http\Requests\ScheduleSetting\ScheduleSettingRequest;
use Illuminate\Http\Request;
use App\Repositories\ScheduleSettingRepository;

class ScheduleSettingController extends Controller
{
    public function __construct(ScheduleSettingRepository $scheduleSettingRepo)
    {
        $this->scheduleSettingRepo = $scheduleSettingRepo;
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->errorAlertMsg = config('alertmessage.alertError');
        $this->alertMsgError = config('alertmessage.alertError');
    }

    public function index()
    {
        $scheduleSettings = $this->scheduleSettingRepo->getAll();
        return view('scheduleSetting.index', compact('scheduleSettings'));
    }

    public function create()
    {
        $scheduleDatas = config('enums.moduleCode');
        return view('scheduleSetting.create', compact('scheduleDatas'));
    }

    public function store(ScheduleSettingRequest $request)
    {
        $data = $request->all();
        $this->scheduleSettingRepo->createSchedule($data);

        return redirect()->route('scheduleSetting.index')->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    public function edit($id)
    {
        $data = $this->scheduleSettingRepo->getById($id);
        $data['cron_tab'] = explode(',', $data['cron_tab']);
        $scheduleDatas = config('enums.moduleCode');
        return view('scheduleSetting.edit', compact('scheduleDatas', 'data'));
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $this->scheduleSettingRepo->updateScheduleSetting($data);
        return redirect()->route('scheduleSetting.index')->with([$this->successAlertMsg => config('alertmessage.editSuccessMsg')]);
    }

    public function destroy(Request $request)
    {
        $this->scheduleSettingRepo->delete($request->id);
        return redirect()->route('scheduleSetting.index')->with([$this->successAlertMsg => config('alertmessage.deleteSuccessMsg')]);
    }
}
