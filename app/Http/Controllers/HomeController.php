<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filters\SlotGame\PlayerResultFilter;
use App\Repositories\SlotGame\SlotBetRepository;
use App\Repositories\SlotGame\SlotPlayerResultRepository;
use App\Repositories\SlotGame\SlotUserDailyRewardRepository;

class HomeController extends Controller
{
    public function __construct(SlotBetRepository $slotBetRepo, SlotPlayerResultRepository $playerResultRepo,SlotUserDailyRewardRepository $userdailyRewardRepo)
    {
        $this->middleware('auth');
        $this->slotBetRepo = $slotBetRepo;
        $this->playerResultRepo = $playerResultRepo;
        $this->userdailyRewardRepo = $userdailyRewardRepo;
        $this->winStatus = config('enums.winStatus.win');
        $this->lossStatus = config('enums.winStatus.loss');
        $this->itemPerPage = config('enums.itemPerPage');
    }

    public function index(PlayerResultFilter $filter)
    {
        $totalAmount = $this->slotBetRepo->getTotalLimitAmount();
        $lossAmount = $this->playerResultRepo->getTotalAmountByWinStatus($this->lossStatus);
        $winAmount = $this->playerResultRepo->getTotalAmountByWinStatus($this->winStatus);
        $dailyRewards = $this->userdailyRewardRepo->getLatestDailyReward();
        $results = $this->playerResultRepo->getAllResults($filter, $this->itemPerPage);
        $slotBets = $this->slotBetRepo->getAllByAsc();
        $batchNos = $this->playerResultRepo->getAllBatch();

        if( $totalAmount > $winAmount){
            $loss = $totalAmount - $winAmount;
            $amount =[$winAmount,$loss];
        } else{
            $loss = $winAmount - $totalAmount;
            $amount =[$loss,$totalAmount];
        }

        return view('welcome', compact('amount','dailyRewards','results','slotBets','batchNos'));
    }
}
