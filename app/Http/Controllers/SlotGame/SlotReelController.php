<?php

namespace App\Http\Controllers\SlotGame;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ImageController;
use App\Repositories\SlotGame\SlotReelRepository;

class SlotReelController extends Controller
{
    public function __construct(SlotReelRepository $slotReelRepo,ImageController $imgController)
    {
        $this->slotReelRepo = $slotReelRepo;
        $this->imgController = $imgController;
        $this->itemPerPage = config('enums.itemPerPage');
        $this->uploadFolder = config('media.games.slotReels.directory');
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->errorAlertMsg = config('alertmessage.alertError');
    }

    public function index()
    {
        $slotReels = $this->slotReelRepo->getAllWithPagination($this->itemPerPage);
        return view('slotGame.slotReel.index', compact('slotReels'));
    }

    public function save(Request $request)
    {   
        $data = $request->all();
        $hasReel = $this->slotReelRepo->checkName($data['name']);
        if($hasReel) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.alreadyNameExist')]);
        }
        if ($request->file('image')) {
            try {
                $this->imgController->checkImageSizeLimitaion('image');
            } catch (\Exception $e) {
                return redirect()->back()->with([$this->errorAlertMsg => $e->validator])->withInput();
            }
            $imagePath = uploadToAkoneyaMedia($request->file('image'), $this->uploadFolder);
            $data['image'] = $imagePath;
        }
        $this->slotReelRepo->create($data);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    public function update(Request $request)
    {   
        $data = [];
        $data['id'] = $request->id;
        $data['name'] = $request->name;
        $hasReel = $this->slotReelRepo->checkName($data['name'], $data['id']);
        if($hasReel) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.alreadyNameExist')]);
        }
        if ($request->file('new_image')) {
            try {
                $this->imgController->checkImageSizeLimitaion('new_image');
            } catch (\Exception $e) {
                return redirect()->back()->with([$this->errorAlertMsg => $e->validator])->withInput();
            }
            deleteFileFromAkoneyaMedia($request->old_image);
            $imagePath = uploadToAkoneyaMedia($request->file('new_image'), $this->uploadFolder);
            $data['image'] = $imagePath;
        } else {
            $data['image'] = $request->old_image;
        }
        $this->slotReelRepo->update($data);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    public function delete(Request $request)
    {
        $this->slotReelRepo->delete($request->id);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.deleteSuccessMsg')]);
    }
}
