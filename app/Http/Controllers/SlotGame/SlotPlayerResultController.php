<?php

namespace App\Http\Controllers\SlotGame;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Filters\SlotGame\PlayerResultFilter;
use App\Repositories\Client\ClientUserRepository;
use App\Repositories\SlotGame\SlotPlayerResultRepository;
use App\Repositories\SlotGame\SlotBetRepository;

class SlotPlayerResultController extends Controller
{
    public function __construct(SlotPlayerResultRepository $playerResultRepo, ClientUserRepository $userRepo, SlotBetRepository $slotBetRepo)
    {
        $this->playerResultRepo = $playerResultRepo;
        $this->userRepo = $userRepo;
        $this->slotBetRepo = $slotBetRepo;
        $this->itemPerPage = config('enums.itemPerPage');
    }

    public function index(PlayerResultFilter $filter)
    {
        $results = $this->playerResultRepo->getAllByFilterWithPagination($filter, $this->itemPerPage);
        $slotBets = $this->slotBetRepo->getAllByAsc();
        $batchNos = $this->playerResultRepo->getAllBatch();
        return view('slotGame.playerResult.index', compact('results', 'slotBets', 'batchNos'));
    }

    public function show(PlayerResultFilter $filter, Request $request)
    {
        $results = $this->playerResultRepo->getByCustomerId($filter, $request->id, $this->itemPerPage);
        $playerName = $this->userRepo->getById($request->id)->name;
        $winStatus = config('filters.winStatus');
        $playerId = $request->id;
        return view('slotGame.playerResult.detail',compact('results','playerName','winStatus','playerId'));
    }
}