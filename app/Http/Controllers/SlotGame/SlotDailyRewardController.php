<?php

namespace App\Http\Controllers\SlotGame;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SlotGame\SlotDailyRewardRepository;

class SlotDailyRewardController extends Controller
{
    public function __construct(SlotDailyRewardRepository $dailyRewardRepo)
    {
        $this->dailyRewardRepo = $dailyRewardRepo;
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->errorAlertMsg = config('alertmessage.alertError');
    }

    public function index()
    {
        $rewards = $this->dailyRewardRepo->getAll();
        foreach($rewards as $reward){
            $reward->coin =  unserialize($reward->reward_coin);
        }
        return view('slotGame.dailyReward.index', compact('rewards'));
    }

    public function save(Request $request)
    {   
        $data = $request->all();
        $data['reward_coin'] = serialize($request->reward_coin);
        $this->dailyRewardRepo->create($data);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    public function update(Request $request)
    {   
        $data = $request->all();
        $data['reward_coin'] = serialize($request->reward_coin);
        $this->dailyRewardRepo->update($data);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    public function delete(Request $request)
    {
        $this->dailyRewardRepo->delete($request->id);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.deleteSuccessMsg')]);
    }
}
