<?php

namespace App\Http\Controllers\SlotGame;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SlotGame\SlotComboRewardRepository;
use App\Repositories\SlotGame\SlotReelRepository;
use App\Repositories\SlotGame\SlotBetRepository;
use App\Repositories\SlotGame\SlotComboRepository;
use App\Filters\SlotGame\ComboRewardFilter;

class SlotComboRewardController extends Controller
{
    public function __construct(SlotComboRewardRepository $slotComboRewardRepo, SlotReelRepository $slotReelRepo, SlotBetRepository $slotBetRepo, SlotComboRepository $slotComboRepo)
    {
        $this->slotComboRewardRepo = $slotComboRewardRepo;
        $this->slotReelRepo = $slotReelRepo;
        $this->slotBetRepo = $slotBetRepo;
        $this->slotComboRepo = $slotComboRepo;
        $this->itemPerPage = config('enums.itemPerPage');
        $this->uploadFolder = config('media.games.slotReels.directory');
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->errorAlertMsg = config('alertmessage.alertError');
    }

    public function index(ComboRewardFilter $filter)
    {
        $slotComboRewards = $this->slotComboRewardRepo->customComboRewardList($filter, $this->itemPerPage);
        $slotAllComboRewards = $this->slotComboRewardRepo->getAllComboReward();
        [ $firstReels, $secondReels, $thirdReels ] = $this->prepareReels();
        $slotBets = $this->slotBetRepo->getAllByAsc();
                                            
        return view('slotGame.slotComboReward.index', compact('slotComboRewards', 'firstReels', 'secondReels', 'thirdReels', 'slotBets', 'slotAllComboRewards'));
    }

    public function save(Request $request)
    {   
        $requestData = $request->all();
        $comboId = $this->slotComboRepo->getComboIdByReelIds($requestData['firstReelId'], $requestData['secondReelId'], $requestData['thirdReelId'])->id ?? null;
        if(!$comboId) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.notFoundCombo')]);
        }

        $slotBets = $this->slotBetRepo->getAllByAsc();
        $initBet = 0;
        $data = [];
        foreach ($slotBets as $value) {
            if ($initBet == 0) $initBet = $value->coin_usage;
            $times = $value->coin_usage / $initBet;
            $rewardCoins = ($requestData['reward_coin'] ?? 0) * $times;

            $isAlreadyExist = $this->slotComboRewardRepo->checkAlreadyExist($value->id, $comboId);
            if($isAlreadyExist) {
                $this->slotComboRewardRepo->update([
                    'id' => $isAlreadyExist->id,
                    'slot_bet_type_id' => $isAlreadyExist->slot_bet_type_id,
                    'slot_combo_id' => $isAlreadyExist->slot_combo_id,
                    'win_type' => $isAlreadyExist->win_type,
                    'reward_coin' => $rewardCoins
                ]);
            } else {
                $data [] = [
                    'slot_bet_type_id' => $value->id,
                    'slot_combo_id' => $comboId,
                    'reward_coin' => $rewardCoins,
                    'win_type' => $requestData['win_type'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
        }
        $this->slotComboRewardRepo->bulkInsert($data);

        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    public function update(Request $request)
    {   
        $requestData = $request->all();
        $comboId = $this->slotComboRepo->getComboIdByReelIds($requestData['firstReelId'], $requestData['secondReelId'], $requestData['thirdReelId'])->id ?? null;
        if(!$comboId) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.notFoundCombo')]);
        }

        $slotBets = $this->slotBetRepo->getAllByAsc();
        $initBet = 0;
        foreach ($slotBets as $value) {
            if ($initBet == 0) $initBet = $value->coin_usage;
            $times = $value->coin_usage / $initBet;
            $rewardCoins = ($requestData['reward_coin'] ?? 0) * $times;
            $isAlreadyExist = $this->slotComboRewardRepo->checkAlreadyExist($value->id, $comboId);

            if($isAlreadyExist) {
                $this->slotComboRewardRepo->update([
                    'id' => $isAlreadyExist->id,
                    'slot_bet_type_id' => $isAlreadyExist->slot_bet_type_id,
                    'slot_combo_id' => $isAlreadyExist->slot_combo_id,
                    'reward_coin' => $rewardCoins,
                    'win_type' => $requestData['win_type'],
                ]);
            } else {
                $this->slotComboRewardRepo->create([
                    'slot_bet_type_id' => $value->id,
                    'slot_combo_id' => $comboId,
                    'reward_coin' => $rewardCoins,
                    'win_type' => $requestData['win_type'],
                ]);
                // return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.notAllowtoEdit')]);
            }

        }
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.editSuccessMsg')]);
    }

    public function delete(Request $request)
    {
        $slotBets = $this->slotBetRepo->getAllByAsc();
        foreach ($slotBets as $value) {
            $isAlreadyExist = $this->slotComboRewardRepo->checkAlreadyExist($value->id, $request->id);
            if($isAlreadyExist) {
                $this->slotComboRewardRepo->softDelete($isAlreadyExist->id);
            }
        }
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.deleteSuccessMsg')]);
    }

    protected function prepareReels()
    {
        $firstReelsArr = $this->slotReelRepo->getAll();
        $secondReelsArr = $firstReelsArr->whereNotIn('id', $firstReelsArr->count()-1);
        $thirdReelsArr = $secondReelsArr->whereNotIn('id', [$firstReelsArr->count()-1, $secondReelsArr->count()-1]);

        return [
            $firstReelsArr,
            $secondReelsArr,
            $thirdReelsArr,
        ];
    }
}
