<?php

namespace App\Http\Controllers\SlotGame;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SlotGame\SlotRewardTypeRepository;

class SlotRewardTypeController extends Controller
{
    public function __construct(SlotRewardTypeRepository $slotRewardTypeRepo)
    {
        $this->slotRewardTypeRepo = $slotRewardTypeRepo;
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->errorAlertMsg = config('alertmessage.alertError');
    }

    public function index()
    {
        $rewards = $this->slotRewardTypeRepo->getAll();
        return view('slotGame.slotRewardType.index', compact('rewards'));
    }

    public function save(Request $request)
    {   
        $data = $request->all();
        $hasType = $this->slotRewardTypeRepo->checkRewardType($data['type']);
        if($hasType) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.alreadyRewardTypeExist')]);
        }
     
        $this->slotRewardTypeRepo->create($data);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    public function update(Request $request)
    {   
        $this->slotRewardTypeRepo->update($request->all());
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    public function delete(Request $request)
    {
        $this->slotRewardTypeRepo->delete($request->id);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.deleteSuccessMsg')]);
    }
}
