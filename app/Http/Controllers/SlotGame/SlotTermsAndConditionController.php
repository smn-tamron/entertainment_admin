<?php

namespace App\Http\Controllers\SlotGame;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\TermsAndConditionRepository;

class SlotTermsAndConditionController extends Controller
{
    public function __construct(TermsAndConditionRepository $termAndConditionRepo)
    {
        $this->termAndConditionRepo = $termAndConditionRepo;
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->slotGameId = config('enums.gameCategory.slotReel');
    }

    public function index()
    {
        $data = $this->termAndConditionRepo->getTermAndConditionByGameCategoryId($this->slotGameId);
        if ($data){
            return view('slotGame.termsAndCondition.index', compact('data'));
        } else{
            return view('slotGame.termsAndCondition.index');
        }
    }

    public function save(Request $request)
    {
        $this->termAndConditionRepo->create($request->all());
        return redirect()->route('slotTermsAndCondition.index')->with([$this->successAlertMsg=> config('alertmessage.saveSuccessMsg')]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $this->termAndConditionRepo->update($data, $data['id']);
        return redirect()->route('slotTermsAndCondition.index')->with([$this->successAlertMsg=> config('alertmessage.editSuccessMsg')]);
    }
}
