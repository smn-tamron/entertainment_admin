<?php

namespace App\Http\Controllers\SlotGame;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SlotGame\SlotBetRepository;

class SlotBetController extends Controller
{
    public function __construct(SlotBetRepository $slotBetRepo)
    {
        $this->slotBetRepo = $slotBetRepo;
        $this->itemPerPage = config('enums.itemPerPage');
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->errorAlertMsg = config('alertmessage.alertError');
    }

    public function index()
    {
        $slotBets = $this->slotBetRepo->index($this->itemPerPage);
        return view('slotGame.slotBet.index', compact('slotBets'));
    }

    public function save(Request $request)
    {   
        $this->slotBetRepo->create($request->all());
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }

    public function update(Request $request)
    {   
        $this->slotBetRepo->update($request->all());
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.editSuccessMsg')]);
    }

    public function delete(Request $request)
    {   
        $this->slotBetRepo->softDelete($request->id);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.deleteSuccessMsg')]);
    }
}
