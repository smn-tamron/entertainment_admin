<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ImageController extends Controller
{
    public function __construct()
    {
        $this->alertMsgError = config('alertmessage.alertError');
    }

    public function getFileFromLocalStoragePath($directoryPath,$fileExtention) {

        $localStoragePath = config('media.local.storagePath');
        $filePath = $localStoragePath."/".$directoryPath;
        if(file_exists($filePath)) {
            $content = Storage::disk('akoneya')->get($directoryPath);
            $contentType = getimagesize($filePath)['mime'];
            $base64Content = base64_encode($content);
            return "data:$contentType;base64,$base64Content";
            
        }

        return null;
    }

    public function getAsset(Request $request,$directoryPath = null)
    {
        if ($request->is('assets/*')) {

            $validExtentions = ["png","jpg","jpeg","svg","gif","ico","PNG","JPG","JPEG","SVG","GIF","ICO","mp4"];

            if($directoryPath) {

                if(strpos($directoryPath, "..") !== FALSE) {

                    return null;
                }

                $params = explode("/", $directoryPath);
                $fileName = end($params);
                $fileExtention = pathinfo($fileName, PATHINFO_EXTENSION);

                if(in_array($fileExtention,$validExtentions)){


                    return $this->getFileFromLocalStoragePath($directoryPath,$fileExtention);

                }
                
            }
        }

        return null;
        
    }

    public function checkImageSizeLimitaion($image)
    {
        $validator =  validator(request()->all(), [
            $image => 'max:'.config("filesystems.imageSizeLimit"),
        ],[$image.'.max' => config('message.invalidFileSize')]);
        if ($validator->fails()) {
            $error_text="";

            foreach ($validator->errors()->all() as $error) {
                $error_text .= $error;
            }
            throw new ValidationException($error_text);
        }

        return null;

    }

    public function checkVideoSizeLimitaion($video)
    {
        $validator =  validator(request()->all(), [
            $video => 'max:'.config("filesystems.videoSizeLimit"),
        ],[$video.'.max' => config('alertmessage.invalidFileSize')]);
        if ($validator->fails()) {
            $error_text="";
            foreach ($validator->errors()->all() as $error) {
                $error_text .= $error;
            }
            throw new ValidationException($error_text);
        }

        return null;

    }
}
