<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Repositories\AdminUser\MenuRepository;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MenuRepository $menuRepo)
    {
        $this->menuRepo = $menuRepo;
        $this->middleware('guest')->except('logout');
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Session::forget('permissionMenuIds');
        Session::forget('permissionSubMenuIds');
        Session::forget('menuInfos');

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect()->route('login');
    }

    /**
     * The user has been authenticated.
     *
     * @param mixed $user
     *
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $permissionMenuIds = $user->role->roleHasMainMenus()->pluck('menu_id')->toArray();
        $permissionSubMenuIds = $user->role->roleHasSubMenus()->pluck('sub_menu_id')->toArray();
        $menuInfos = $this->menuRepo->getPermissionMenu($user->id);

        Session::put('permissionMenuIds',$permissionMenuIds);
        Session::put('permissionSubMenuIds',$permissionSubMenuIds);
        Session::put('menuInfos',$menuInfos);
    }
}
