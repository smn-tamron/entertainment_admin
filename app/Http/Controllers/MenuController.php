<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filters\MenuFilter;
use App\Repositories\AdminUser\MenuRepository;
use App\Repositories\AdminUser\SubMenuRepository;
use Illuminate\Support\Facades\Session;

class MenuController extends Controller
{
    public function __construct(MenuRepository $menuRepo,SubMenuRepository $submenuRepo)
    {
        $this->menuRepo = $menuRepo;
        $this->submenuRepo = $submenuRepo;
        $this->firstSequenceNumber = config('enums.firstSequenceNumber');
        $this->successAlertMsg = config('alertmessage.alertInfo');
        $this->errorAlertMsg = config('alertmessage.alertError');
        $this->successStatusAlert = config('alertmessage.alertSuccess');
    }

    public function index(MenuFilter $filter)
    {
        $menus = $this->menuRepo->getByFilterWithSequenceNumber($filter);
        return view('menus.index', compact('menus'));
    }
    
    public function sort(Request $request)
    {
        foreach ($request->menuIds as $menuId) {
            $this->menuRepo->findAndUpdateSequenceNumber($menuId, $this->firstSequenceNumber);
            $this->firstSequenceNumber++;
        }
        return response()->json([$this->successStatusAlert => config('alertmessage.statusChangeMsg')]);
    }

    public function store(Request $request)
    {
        $existingMenu = $this->menuRepo->checkMenu($request->name, null);
        if ($existingMenu) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.alreadyMenuExist')])->withInput();
        }
        $this->menuRepo->create($request->all());
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.saveSuccessMsg')]);
    }
   
    public function update(Request $request)
    {
        $data =$request->all();
        $existingMenu = $this->menuRepo->checkMenu($data['name'], $data['id']);
        if ($existingMenu) {
            return redirect()->back()->with([$this->errorAlertMsg => config('alertmessage.alreadyMenuExist')])->withInput();
        }
        $this->menuRepo->update($data, $data['id']);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.editSuccessMsg')]);
    }
    
    public function show(Request $request)
    {
        $menu = $this->menuRepo->getById($request->id);
        $submenus = $this->submenuRepo->getAllSubmenusByMenuId($menu);
        return view('menus.submenus.index',compact('menu','submenus'));
    }

    public function destroy(Request $request)
    {
        $menu = $this->menuRepo->getById($request->id);
        $submenus = $this->menuRepo->getSubmenusByMenuId($request->id);
        foreach($submenus as $submenu){
            $this->submenuRepo->deleteSubmenuPermissionBySubmenuId($submenu->id);
        }
        $this->menuRepo->deleteMenuPermissionByMenuId($request->id);
        $this->submenuRepo->deleteSubmenuByMenuId($menu);
        $this->menuRepo->delete($request->id);
        return redirect()->back()->with([$this->successAlertMsg => config('alertmessage.deleteSuccessMsg')]);
    }   
}
