<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

/**
 * Upload File To Akoneya Media
 */
if (!function_exists('base64ImageUploadToAkoneyaMedia')) {
    function base64ImageUploadToAkoneyaMedia($base64String, $directoryName)
    {
        $file = base64_decode($base64String);
        $fileExtension = ".png";
        $fileName = time().Str::random(6).$fileExtension;
        $mediaPath = $directoryName."/".$fileName;
        if (Storage::put($mediaPath, $file)) {
            return $mediaPath;
        }
        return null;
    }
}
if (!function_exists('uploadToAkoneyaMedia')) {
    function uploadToAkoneyaMedia($file, $directoryName)
    {
        $fileExtension = $file->extension();
        $fileName = time().Str::random(10).".$fileExtension";
        $filePath = Storage::putFileAs($directoryName, new File($file), $fileName);
        //$fileUrl = Storage::disk("akoneya")->url($filePath);

        return  $filePath;
    }
}

/**
 * Delete File From Akoneya Media
 */

if (!function_exists('deleteFileFromAkoneyaMedia')) {
    function deleteFileFromAkoneyaMedia($filePath)
    {
         
        //$filePath = explode(Storage::disk("akoneya")->url("/"),$fileUrl);
        Storage::delete($filePath);
    }
}

/*
 * Get Fiel URL from Akoneya Medai
 */
if (!function_exists('getFileUrlFromAkoneyaMedia')) {
    function getFileUrlFromAkoneyaMedia($filePath)
    {
        return Storage::url($filePath);
    }
}

/*
 * Get Fiel URL from Akoneya Media
 */
if (!function_exists('getImageFromAkoneyaMedia')) {
    function getImageFromAkoneyaMedia($filePath)
    {
        return Storage::url($filePath);
        // $content = file_get_contents($fileUrl);
        // return $content;
    }
}

if (!function_exists('getZoneNameById')) {
    function getZoneNameById($zoneId)
    {
        return config("zones.zoneSelections.$zoneId.name");
    }
}
if (!function_exists('getCarRentalTypeById')) {
    function getCarRentalTypeById($carTypeId)
    {
        return config("carRentalType.carRentalTypeSelection.$carTypeId.name");
    }
}
if (!function_exists('getCarTypeById')) {
    function getCarTypeById($carTypeId)
    {
        return config("carTypes.carTypeSelection.$carTypeId.name");
    }
}
if (!function_exists('getDrivingTypeById')) {
    function getDrivingTypeById($drivingTypeId)
    {
        return config("drivingType.carDrivingTypeSelection.$drivingTypeId.name");
    }
}
if (!function_exists('getReasonTypeById')) {
    function getReasonTypeById($reportType)
    {
        return config("enums.reportType.$reportType.name");
    }
}
if (!function_exists('getShoppingFeedReasonTypeById')) {
    function getShoppingFeedReasonTypeById($reportType)
    {
        return config("enums.shoppingFeedReportType.$reportType.name");
    }
}
if (!function_exists('str_plural')) {
    function str_plural($value)
    {
        return Str::plural($value);
    }
}
if (!function_exists('change24HrFormatTo12HrFormat')) {
    function change24HrFormatTo12HrFormat($value)
    {
        return date("g:iA", strtotime($value));
    }
}
if (!function_exists('getOrderStatusName')) {
    function getOrderStatusName($status)
    {
        return config("enums.orderStatus.$status.name");
    }
}


if (!function_exists('sendTxtMsgWithViber')) {
    function sendTxtMsgWithViber($receiverId, $receiverName, $txtMsg)
    {
        Http::post(config('services.viber.msgApi'), [
            "auth_token"=> config('services.viber.authToken'),
            "receiver"=>$receiverId,
            "min_api_version"=>1,
            "sender"=>[
                "name"=>$receiverName,
                "avatar"=>"http=>//avatar.example.com"
            ],
            "type"=>"text",
            "text"=>$txtMsg
        ]);
    }
}


if (!function_exists('sendImageWithViber')) {
    function sendImageWithViber($receiverId, $receiverName, $txtMsg, $imgUrl)
    {
        Http::post(config('services.viber.msgApi'), [
            "auth_token"=> config('services.viber.authToken'),
            "receiver"=>$receiverId,
            "min_api_version"=>1,
            "sender"=>[
                "name"=>$receiverName,
                "avatar"=>"http=>//avatar.example.com"
            ],
            "type"=>"picture",
            "text"=>$txtMsg,
            "media"=>$imgUrl,
            "thumbnail"=>$imgUrl

        ]);
    }
}

if (!function_exists('diffForHumanFormat')) {
    function diffForHumanFormat($value)
    {
        return Carbon::parse($value)->diffForHumans();
    }
}

if (!function_exists('clientDateFormat')) {
    function clientDateFormat($value)
    {
        return date('d M Y - h:i A', strtotime($value));
    }
}

if (!function_exists('birthdayDateFormat')) {
    function birthdayDateFormat($value)
    {
        return date('d M Y', strtotime($value));
    }
}