<?php

namespace App\Filters;

class MenuFilter extends Filter
{
    protected $filters = ['keyword'];

    public function keyword($value) 
    {
        return $this->builder->where('name', 'LIKE', "%{$value}%") ;
    }
}
