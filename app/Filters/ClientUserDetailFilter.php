<?php

namespace App\Filters;

class ClientUserDetailFilter extends Filter
{

    protected $filters = ['keyword'];

    public function keyword($value) 
    {
        return $this->builder
            ->where(function ($query) use ($value) {
                $query->whereRaw("CONCAT(UNIX_TIMESTAMP(slot_customer_results.created_at), slot_customer_results.id) LIKE '%{$value}%'");
            });
    }
}
