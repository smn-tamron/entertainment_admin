<?php

namespace App\Filters;

class ClientUserFilter extends Filter
{

    protected $filters = ['keyword','user_type'];

    public function keyword($value) 
    {
        return $this->builder
            ->where(function ($query) use ($value) {
                $query->where('personal_information.name', 'LIKE', "%{$value}%")  
                      ->orWhere('client_users.name', 'LIKE', "%{$value}%");
            });
    }

    public function user_type($value)
    {
      return $this->builder->where('user_type',$value);
    }
}
