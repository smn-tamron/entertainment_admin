<?php

namespace App\Filters\SlotGame;

use App\Filters\Filter;

class ComboRewardFilter extends Filter
{
    protected $filters = ['keyword','betTypeId', 'firstReelId', 'secondReelId', 'thirdReelId', 'winType'];

    public function keyword($value)
    {
        return $this->builder->where(function ($query) use ($value) {
                $query->where('name', 'LIKE', "$value"); 
                $query->orwhere('name_mm', 'LIKE', "$value");
                $query->orwhere('name_zh', 'LIKE', "$value");       
            });
    }

    public function betTypeId($value) 
    {
        return $this->builder->where('slot_bet_type_id', $value);
    }

    public function firstReelId($value)
    {
        return $this->builder->where('slot_combos.first_reel_id', $value);
    }

    public function secondReelId($value)
    {
        return $this->builder->where('slot_combos.second_reel_id', $value);
    }

    public function thirdReelId($value)
    {
        return $this->builder->where('slot_combos.third_reel_id', $value);
    }

    public function winType($value)
    {
        return $this->builder->where('slot_combo_rewards.win_type', $value);
    }
}