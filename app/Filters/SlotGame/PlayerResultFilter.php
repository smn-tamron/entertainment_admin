<?php

namespace App\Filters\SlotGame;

use App\Filters\Filter;

class PlayerResultFilter extends Filter
{
    protected $filters = ['keyword', 'batchNo', 'betTypeId'];

    public function keyword($value)
    {
      return $this->builder->where('client_users.name', 'like', '%' .$value. '%');
    }
    
    public function batchNo($value)
    {
      return $this->builder->where('slot_customer_results.batch_no', $value);
    }
    
    public function betTypeId($value)
    {
      return $this->builder->where('slot_combo_rewards.slot_bet_type_id', $value);
    }
}
