<?php

namespace App\Services;

class PostalCodeCSVParser extends CSVParser
{
    public function __construct()
    {
        $this->columnNames = [
            'zone','township','area_name','area_postal_code'
        ];
    }
}
