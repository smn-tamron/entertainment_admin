<?php

namespace App\Services;

abstract class CSVParser
{
    public $columnNames;
    
    public function __construct($columnNames)
    {
        $this->columnNames = $columnNames;
    }

    public function csvToArray($csvFile)
    {
        if (! file_exists($csvFile) || ! is_readable($csvFile)) 
        {
            return false;
        }

        ob_end_clean();

        $columnsCount = count($this->columnNames);
        $header = null;
        $data = [];

        if (($handle = fopen($csvFile,'r')) !== false) {
            while (($row = fgetcsv($handle,",")) !== false) {
                if(array(null) !== $row) {
                    if (! $header) {
                        $header = $row;
                        $headerColumns = array_intersect($this->columnNames, $header);
                        sort($headerColumns);
                        sort($this->columnNames);
                        if($headerColumns != $this->columnNames) {
                            return false;
                        }
                    }
                    else{
                        if(count($row) == $columnsCount){
                            $data[] = array_combine($header, $row);
                        }
                        else{
                            return false;
                        }
                    }    
                }
            }
            fclose($handle);
        }
        return $data;
    }
}
