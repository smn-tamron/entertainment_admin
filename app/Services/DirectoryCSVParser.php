<?php

namespace App\Services;

class DirectoryCSVParser extends CSVParser
{
    public function __construct()
    {
        $this->columnNames = [
            'name','name_mm','name_zh','phone','address','address_mm','address_zh','zone','township'
        ];
    }
}
