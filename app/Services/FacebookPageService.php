<?php

namespace App\Services;

use App\Repositories\FacebookPageRepository;
use Facebook\Facebook;

class FacebookPageService implements FacebookPageRepository
{
    public function __construct()
    {
        $this->appId = config('services.facebook.app_id');
        $this->appSecret = config('services.facebook.app_secret');
        $this->defaultGraphVersion = config('services.facebook.default_graph_version');
        $this->accessToken = config('services.facebook.access_token');
        $this->pageId = config('services.facebook.page_id');
    }
    public function getFacebookInstance()
    {
        $facebook = new Facebook([
            'app_id' => $this->appId,
            'app_secret' => $this->appSecret,
            'default_graph_version' => $this->defaultGraphVersion
        ]);

        $longLivedToken = $facebook->getOAuth2Client()->getLongLivedAccessToken($this->accessToken);

        $pageId = $this->pageId;

        $facebook->setDefaultAccessToken($longLivedToken);

        $response = $facebook->sendRequest('GET', $pageId, ['fields' => 'access_token'])
        ->getDecodedBody();

        $foreverPageAccessToken = $response['access_token'];

        $facebook->setDefaultAccessToken($foreverPageAccessToken);

        return $facebook;
    }
    public function postToFacebook($photosUrl, $status)
    {
        $facebook = $this->getFacebookInstance();
        $pageId = $this->pageId;

        $photoIds = [];

        foreach ($photosUrl as $photoUrl) {
            $params = array(
                "url" => $photoUrl,
                "published" => false
            );

            try {
                $result = $facebook->sendRequest("POST", "$pageId/photos", $params)->getDecodedBody();

                if (!empty($result["id"])) {
                    $photoIds[] = $result["id"];
                }
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                return $e->getMessage();
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                return $e->getMessage();
            }
        }

        $params = ["message" => $status];
        
        foreach ($photoIds as $key => $photoId) {
            $params["attached_media"][$key] = '{"media_fbid":"' . $photoId . '"}';
        }

        try {
            $result = $facebook->sendRequest("POST", "$pageId/feed", $params)->getDecodedBody();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            return $e->getMessage();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            return $e->getMessage();
        }

        return $result;
    }
}
