<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Model\TermsAndCondition;

class TermsAndConditionRepository extends BaseRepository
{
    public function __construct(TermsAndCondition $model)
    {
        $this->model = $model;
    }

    public function getTermAndConditionByGameCategoryId($gameId)
    {
        $data = [];
        $objs = $this->model->where('game_category_id', $gameId)->get();
        foreach ($objs as $value) {
            $data['id'] = $value->id;
            $data['game_category_id'] = $value->game_category_id;
            $data['description'] = $value->description;
            $data['updated_at'] = $value->updated_at;
        }
        return $data;
    }
}
