<?php
namespace App\Repositories\Client;

use App\Model\Client\ClientUser;
use App\Model\Client\ClientPersonalInformation;
use Illuminate\Support\Facades\DB;

class ClientUserRepository extends BaseRepository
{
    protected $model;

    public function __construct(ClientUser $model, ClientPersonalInformation $clientPersonalInformation)
    {
        $this->model = $model;
        $this->clientPersonalInformation = $clientPersonalInformation;
        $this->itemPerPage = config('enums.itemPerPage');
        $this->placeholder_image_url = config('enums.fakeProfile');
        $this->enableActiveStatus = config('enums.enableActiveStatus');
    }

    public function checkExistingEmail($email)
    {
    	return $this->model->where('active_status', "=", $this->enableActiveStatus)
                                ->where('email', $email)
                                ->first();
    }

    public function checkExistingPhone($phone)
    {
    	return $this->model->where('active_status', "=", $this->enableActiveStatus)
                                ->where('phone', $phone)
                                ->first();
    }

    public function getClientUserList($filter)
    {
        return $this->model
            ->select('client_users.*', 'personal_information.name as userName')
            ->leftJoin('personal_information', 'client_users.id', '=', 'personal_information.client_user_id')
            ->filter($filter)
            ->orderBy('id','desc')
            ->paginate($this->itemPerPage);
    }

    public function getClientUserListBk($filter)
    {
        return $this->model
            ->select('client_users.*', 'personal_information.name as userName')
            ->leftJoin('personal_information', 'client_users.id', '=', 'personal_information.client_user_id')
            ->filter($filter)
            ->orderBy('id','desc')
            ->paginate($this->itemPerPage);
    }

    public function getClientUserDetails($id)
    {
    	return $this->getById($id);
    }

    public function getClientPersonalInformation($clientUserId)
    {
    	return $this->clientPersonalInformation->where('client_user_id',$clientUserId)->first();
    }

    public function createClientuser($data)
    {
        $clientUser = $this->create($data);
        $this->cloneToProfile($data, $clientUser->id);
        return $clientUser;
    }

    protected function cloneToProfile($data, $userId)
    {
        $personalInfo = $this->clientPersonalInformation->where('client_user_id', $userId)->first();
     
        if (!$personalInfo) {
            $createData = [
               'client_user_id'=> $userId,
               'name'=> $data['name'],
               'image' => $this->placeholder_image_url
            ];
            if ($data['login_type'] == 'phone') {
                $createData['phone'] = $data['phone'];
            }else {
                $createData['email'] = $data['email'];
            }
            $this->clientPersonalInformation->create($createData);
            // $this->insertMongodb($createData);
        }
    }

    // protected function insertMongodb($createData)
    // {
    //     $url = getFileUrlFromAkoneyaMedia($createData['image']);

    //     unset($createData['image']);
        
    //     $createData['image'] = $url;

    //     $this->chatUserInfoModel->create($createData);
    // }

    public function statusChange($bannedStatus, $id)
    {
        return $this->model->where('id', $id)->update(['banned_status' => $bannedStatus]);
    }

    public function getClientUserByShopLocation($foodShop)
    {
        return $this->model
                    ->join('addresses','addresses.customer_id','=','client_users.id')
                    ->select('client_users.name as client_name','addresses.*', DB::raw('6371 * acos(cos(radians('.$foodShop->lat.')) * cos(radians(addresses.lat)) 
                                * cos(radians(addresses.long) - radians('.$foodShop->long.')) 
                                + sin(radians('.$foodShop->lat.')) 
                                * sin(radians(addresses.lat))) AS distance'))
                                ->having('distance', '<=', 5)
                                ->where('client_users.is_banned',0)
                                ->get();
    }
}
