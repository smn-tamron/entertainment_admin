<?php

namespace App\Repositories;

interface FacebookPageRepository
{
    public function postToFacebook($photoUrl, $data);
}
