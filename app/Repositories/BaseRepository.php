<?php
namespace App\Repositories;

abstract class BaseRepository
{
    protected $model;
    
    public function __construct(\Model $model)
    {
        $this->model = $model;
        $this->enableDeletedStatus = config('enums.enableDeletedStatus');
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function getAll()
    {
        return $this->model->orderBy('id', 'DESC')->get();
    }

    public function getAllByAsc()
    {
        return $this->model->get();
    }

    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function update($data)
    {
        $getData = $this->getById($data['id']);
        $getData->fill($data);
        return $getData->push();
    }
    
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function getByFilterWithPagination($filter, $perPage)
    {
        return $this->model->filter($filter)->orderBy('id', 'desc')->paginate($perPage);
    }

    public function getNameList()
    {
        $data = [];
        $objs = $this->model->get();
        foreach ($objs as $value) {
            $data[$value->id] = $value->name;
        }
        return $data;
    }
    
    public function getAllWithPagination($perPage)
    {
        return $this->model->orderBy('id', 'desc')->paginate($perPage);
    }

    public function statusChange($status, $id)
    {
        $getData = $this->getById($id);
        $getData['publish_status'] = $status;
        return $getData->push();
    }

    public function bulkInsert(array $data)
    {
        return $this->model->insert($data);
    }

    public function softDelete($id)
    {
        $getData = $this->getById($id);
        $getData['deleted_status'] = $this->enableDeletedStatus;
        return $getData->push();
    }
}
