<?php
namespace App\Repositories\AdminUser;

use App\User;
use App\Role;
use App\Model\Menu\RoleHasMainMenu;
use App\Model\Menu\RoleHasSubMenu;
use App\Repositories\AdminUser\BaseRepository;

class RoleRepository extends BaseRepository
{
    protected $model;

    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    public function index($page)
    {
        return $this->model->with('roleHasMainMenus.menu')->orderBy('id', 'DESC')->paginate($page);
    }
  
    public function updateRole($name, $id)
    {
        return $this->model->find($id)->update(['name'  => $name]);
    }

    public function checkRole($name, $id)
    {
        if($id){
            return $this->model->where('id', '<>', $id)->where('name', $name)->first();
        }
        return $this->model->where('name', $name)->first();
    }

    public function getRole()
    {
        $data=[];
        $roles = $this->model->select('id', 'name')->orderby('id', 'desc')->get();
        foreach ($roles as $key => $value) {
            $data[$value->id]=$value->name;
        }
        return $data;
    }

    public function deleteMenuPermissionByRoleId($id)
    {
		return RoleHasMainMenu::where('role_id',$id)->delete();
    }
    
    public function deleteSubMenuPermissionByRoleId($id)
    {
		return RoleHasSubMenu::where('role_id',$id)->delete();
    }
    
    public function checkRelatedUser($id)
    {
		return User::where('role_id',$id)->first();
	}
}
