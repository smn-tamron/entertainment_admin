<?php
namespace App\Repositories\AdminUser;

use App\Model\Menu\Menu;
use App\Model\Menu\SubMenu;
use App\Model\Menu\RoleHasSubMenu;
use App\Repositories\AdminUser\BaseRepository;

Class SubMenuRepository extends BaseRepository
{
    protected $model;

	public function __construct(SubMenu $model)
	{
		$this->model = $model;
    }

    public function checkSubmenu($name, $menuId, $id)
    {
        if($id){
            return $this->model->where('id', '<>', $id)->where([['name', $name],['menu_id', $menuId]])->first();
        }
        return $this->model->where([['name', $name],['menu_id', $menuId]])->first();
    }

    public function getAllSubmenusByMenuId($menu)
    {
        return $menu->subMenus()->orderBy('sequence_number')->get();
    }

    public function checkSubMenuPermission($roleId, $submenuId)
    { 
        return RoleHasSubMenu::where([['role_id',$roleId],['sub_menu_id',$submenuId]])->first();
    }

    public function createSubMenuPermission($roleId, $submenuId)
    {
		return RoleHasSubMenu::create([
			'role_id'=> $roleId,
			'sub_menu_id' => $submenuId
		]);
    }

    public function deleteSubMenuPermission($roleId, $submenuIds)
    {
        foreach ($submenuIds as $id) {
            $result = RoleHasSubMenu::where('role_id',$roleId)->where('sub_menu_id',$id)->delete();
        }
        return $result;
    }

    public function getSubMenuIdsByRoleId($roleId)
    {
        return RoleHasSubMenu::where('role_id', $roleId)->pluck('sub_menu_id')->toArray();
    }

    public function deleteSubMenuPermissionsByRoleId($roleId)
    {
        RoleHasSubMenu::where('role_id',$roleId)->delete();
    }

    public function deleteSubmenuByMenuId($menu)
    {
		return $menu->subMenus()->delete();
    }

    public function deleteSubmenuPermissionBySubmenuId($id)
    {
       return RoleHasSubMenu::where('sub_menu_id',$id)->delete();
    }
}