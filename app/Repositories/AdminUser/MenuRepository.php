<?php
namespace App\Repositories\AdminUser;

use Illuminate\Support\Facades\Validator;
use App\Model\Menu\Menu;
use App\Model\Menu\RoleHasSubMenu;
use App\Model\Menu\SubMenu;
use App\Model\Menu\RoleHasMainMenu;
use App\Repositories\AdminUser\BaseRepository;
use App\User;
use Illuminate\Support\Facades\DB;

Class MenuRepository extends BaseRepository
{
    protected $model;

	public function __construct(Menu $model,User $usrModel)
	{
        $this->model = $model;
        $this->usrModel = $usrModel;
    }

    public function checkMenu($name, $id)
    {
        if($id){
            return $this->model->where('id', '<>', $id)->where('name', $name)->first();
        }
        return $this->model->where('name', $name)->first();
    }

    public function checkMenuPermission($roleId, $menuId)
    { 
        return RoleHasMainMenu::where([['role_id',$roleId],['menu_id',$menuId]])->first();
    }

    public function createMenuPermission($roleId, $menuId)
    {
		return RoleHasMainMenu::create([
			'role_id'=> $roleId,
			'menu_id' => $menuId
		]);
    }

    public function deleteMenuPermission($roleId, $menuIds)
    {
        foreach ($menuIds as $id) {
            $result = RoleHasMainMenu::where('role_id',$roleId)->where('menu_id',$id)->delete();
        }
        return $result;
    }

    public function getSubmenusByMenuId($menuId)
    {
        return SubMenu::where('menu_id',$menuId)->get();
    }

    public function getMenuIdsByRoleId($roleId)
    {
        return RoleHasMainMenu::where('role_id', $roleId)->pluck('menu_id')->toArray();
    }

    public function getPermissionMenu($userId)
    {       
        $allMenu = [];
        $mainMenus = DB::table('users')
                    ->join('role_has_main_menus', 'users.role_id', '=', 'role_has_main_menus.role_id')
                    ->join('menus', 'role_has_main_menus.menu_id', '=', 'menus.id')
                    ->where('users.id',$userId)
                    ->select('menus.id','menus.name','menus.icon','menus.route')
                    ->orderby('sequence_number')
                    ->get();
    
        foreach($mainMenus as $menu){
            $subMenus=DB::table('users')
                    ->join('role_has_sub_menus', 'users.role_id', '=', 'role_has_sub_menus.role_id')
                    ->join('sub_menus', 'role_has_sub_menus.sub_menu_id', '=', 'sub_menus.id')
                    ->where('users.id',$userId)
                    ->where('sub_menus.menu_id',$menu->id)
                    ->select('sub_menus.id','sub_menus.menu_id','sub_menus.name','sub_menus.icon','sub_menus.route')
                    ->orderby('sequence_number')
                    ->get();

            $allMenu [] =['id'=>$menu->id,'name'=>$menu->name,'route'=>$menu->route,'icon'=>$menu->icon,'subMenu'=> $subMenus];
        }
       return $allMenu;
    }

    public function deleteMenuPermissionsByRoleId($roleId)
    {
        RoleHasMainMenu::where('role_id',$roleId)->delete();
    }

    public function deleteMenuPermissionByMenuId($id)
    {
        RoleHasMainMenu::where('menu_id',$id)->delete();
    }
}
