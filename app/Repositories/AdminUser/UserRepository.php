<?php
namespace App\Repositories\AdminUser;
use App\User;
use Hash;
class UserRepository 
{
   
    public function __construct(User $model)
    {
        $this->model = $model;
    }
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }
    public function getPaginated($page)
    {
        return $this->model->with('role')->orderBy('id', 'DESC')->paginate($page);
    }
    public function create(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        return $this->model->create($data);
    }
    public function checkEmailForUpdate($email,$id)
    {
        return $this->model::where('id', '<>', $id)->where('email', '=', $email)->first();
    }
    public function update(array $data, $id)
    {
        $getData = $this->getById($id);
        $getData->fill($data);
        return $getData->push();
    }
    public function delete($id)
    {
         return $this->model->destroy($id);

    }
}