<?php
namespace App\Repositories\AdminUser;

abstract class BaseRepository
{
    protected $model;
    public $disableDeletedStatus,$enableDeletedStatus;

    public function __construct(\Model $model)
    {
        $this->model = $model;
        $this->disableDeletedStatus = config('enums.disableDeletedStatus');
        $this->enableDeletedStatus = config('enums.enableDeletedStatus');
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }
    
    public function update(array $data, $id)
    {
        $getData = $this->getById($id);
        $getData->fill($data);
        return $getData->push();
    }
    
    public function getAll()
    {
        return $this->model->orderBy('id', 'DESC')->get();
    }

    public function getAllBySequenceNumber()
    {
        return $this->model->orderBy('sequence_number', 'ASC')->get();
    }

    public function findAndUpdateSequenceNumber($submenuId, $sequenceNo)
    {
        return $this->model->where('id', $submenuId)->first()->update(['sequence_number' => $sequenceNo ]);
    }
    
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }
    
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function getPaginated($page)
    {
        return $this->model->orderBy('id', 'DESC')->paginate($page);
    }

    public function getByFilterWithSequenceNumber($filter)
    {
        return $this->model->filter($filter)->orderBy('sequence_number', 'ASC')->get();
    }
    
    public function softDelete($id)
    {
        $getData = $this->getById($id);
        $getData['deleted_status'] = $this->enableDeletedStatus;
        return $getData->push();
    }
}
