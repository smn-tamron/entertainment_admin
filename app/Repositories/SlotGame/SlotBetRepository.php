<?php

namespace App\Repositories\SlotGame;

use App\Repositories\BaseRepository;
use App\Model\SlotGame\SlotBet;

class SlotBetRepository extends BaseRepository
{
    public function __construct(SlotBet $model)
    {
        $this->model = $model;
        $this->disableDeletedStatus = config('enums.disableDeletedStatus');
        $this->enableDeletedStatus = config('enums.enableDeletedStatus');
    }

    public function index($itemPerpage)
    {
        return $this->model->where('deleted_status',$this->disableDeletedStatus)->orderBy('coin_usage', 'asc')->paginate($itemPerpage);
    }

    public function getTotalLimitAmount()
    {
        $data = $this->model->select('limit_reward_amount')->where('deleted_status',$this->disableDeletedStatus)->get();
        $total = 0;
        if(count($data) > 0){
            foreach($data as $value){
                $totalAmount[] = $value->limit_reward_amount;
            }
            $total = array_sum($totalAmount);
        }
        return $total;
    }
}