<?php

namespace App\Repositories\SlotGame;

use App\Repositories\BaseRepository;
use App\Model\SlotGame\SlotCombo;

class SlotComboRepository extends BaseRepository
{
    public function __construct(SlotCombo $model)
    {
        $this->model = $model;
    }

    public function getComboIdByReelIds($firstReelId, $secondReelId, $thirdReelId)
    {
        return $this->model->where(['first_reel_id' => $firstReelId, 'second_reel_id' => $secondReelId, 'third_reel_id' => $thirdReelId])->first();
    }
}
