<?php

namespace App\Repositories\SlotGame;

use Carbon\Carbon;
use App\Repositories\BaseRepository;
use App\Model\SlotGame\SlotUserDailyReward;

class SlotUserDailyRewardRepository extends BaseRepository
{
    public function __construct(SlotUserDailyReward $model)
    {
        $this->model = $model;
    }

    public function getLatestDailyReward()
    {
        return $this->model->whereDate('created_at', Carbon::today())->latest()->take(30)->get();
    }
}
