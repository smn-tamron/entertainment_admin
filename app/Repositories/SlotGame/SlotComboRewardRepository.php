<?php

namespace App\Repositories\SlotGame;

use App\Repositories\BaseRepository;
use App\Model\SlotGame\SlotComboReward;

class SlotComboRewardRepository extends BaseRepository
{
    public function __construct(SlotComboReward $model)
    {
        $this->model = $model;
        $this->disableDeletedStatus = config('enums.disableDeletedStatus');
        $this->enableDeletedStatus = config('enums.enableDeletedStatus');
    }

    public function checkAlreadyExist($betTypeId, $comboId = null)
    {
        if ($comboId) {
            return $this->model->where('slot_combo_id', $comboId)->where('slot_bet_type_id', $betTypeId)->first();
        } 
        return true;
    }

    public function customComboRewardList($filter, $perPage)
    {
        return $this->model
            ->select('slot_combo_rewards.*')
            ->join('slot_combos', 'slot_combos.id', '=', 'slot_combo_rewards.slot_combo_id')
            ->where('deleted_status',$this->disableDeletedStatus)
            ->groupBy('slot_combo_id')
            ->filter($filter)
            ->orderBy('id', 'desc')
            ->paginate($perPage);
    }

    public function getAllComboReward()
    {
        return $this->model->where('deleted_status',$this->disableDeletedStatus)->orderBy('id', 'DESC')->get();
    }
}
