<?php

namespace App\Repositories\SlotGame;

use App\Repositories\BaseRepository;
use App\Model\SlotGame\SlotPlayerResult;

class SlotPlayerResultRepository extends BaseRepository
{
    public function __construct(SlotPlayerResult $model)
    {
        $this->model = $model;
    }

    public function getAllByFilterWithPagination($filter, $perPage)
    {
        return $this->model
            ->select(
                'slot_customer_results.batch_no',
                'client_users.name',
                'slot_combo_rewards.slot_bet_type_id',
                'slot_bet_types.coin_usage',
                'slot_bet_types.limit_reward_amount',
                $this->model->raw('SUM(slot_customer_results.amount) as total_coin')
            )
            ->join('slot_combo_rewards', 'slot_combo_rewards.id', '=', 'slot_customer_results.slot_combo_reward_id')
            ->join('slot_bet_types', 'slot_bet_types.id', '=', 'slot_combo_rewards.slot_bet_type_id')
            ->join('client_users', 'client_users.id', '=', 'slot_customer_results.customer_id')
            ->filter($filter)
            ->groupBy('slot_customer_results.customer_id')
            ->groupBy('slot_customer_results.batch_no')
            ->groupBy('slot_combo_rewards.slot_bet_type_id')
            ->orderBy('slot_customer_results.batch_no', 'desc')
            ->orderBy('slot_combo_rewards.slot_bet_type_id', 'desc')
            ->paginate($perPage);
    }

    public function getAllByFilterWithPaginationAndUserId($id, $filter, $perPage)
    {
        return $this->model
        ->select(
            'slot_customer_results.id',
            'slot_customer_results.batch_no',
            'client_users.name',
            'slot_combo_rewards.slot_bet_type_id',
            'slot_bet_types.coin_usage',
            'slot_customer_results.amount',
            'slot_customer_results.win_status',
            'slot_customer_results.created_at',
            $this->model->raw('CONCAT(UNIX_TIMESTAMP(slot_customer_results.created_at), slot_customer_results.id) as transaction_id')
        )
        ->leftJoin('slot_combo_rewards', 'slot_combo_rewards.id', '=', 'slot_customer_results.slot_combo_reward_id')
        ->leftJoin('slot_bet_types', 'slot_bet_types.id', '=', 'slot_combo_rewards.slot_bet_type_id')
        ->leftJoin('client_users', 'client_users.id', '=', 'slot_customer_results.customer_id')
        ->filter($filter)
        ->where('slot_customer_results.customer_id', $id)
        ->orderBy('slot_customer_results.created_at', 'desc')
        ->orderBy('slot_customer_results.batch_no', 'desc')
        ->orderBy('slot_combo_rewards.slot_bet_type_id', 'desc')
        ->paginate($perPage);
    }

    public function getAllResults($filter, $perPage)
    {
        $results = $this->model->select(
                    'slot_customer_results.batch_no',
                    'slot_combo_rewards.slot_bet_type_id',
                    'slot_bet_types.coin_usage',
                    'slot_bet_types.limit_reward_amount',
                    $this->model->raw('COUNT(slot_customer_results.customer_id) as customer_count'),
                    $this->model->raw('SUM(slot_customer_results.amount) as total_coin'))
                ->join('slot_combo_rewards', 'slot_combo_rewards.id', '=', 'slot_customer_results.slot_combo_reward_id')
                ->join('slot_bet_types', 'slot_bet_types.id', '=', 'slot_combo_rewards.slot_bet_type_id')
                ->groupBy('slot_customer_results.batch_no')
                ->filter($filter)
                ->groupBy('slot_combo_rewards.slot_bet_type_id')
                ->orderBy('slot_customer_results.batch_no', 'desc')
                ->orderBy('slot_combo_rewards.slot_bet_type_id', 'desc')->paginate($perPage);

        foreach( $results as $result){
            $result->winRate = 100 - (($result->total_coin / $result->limit_reward_amount) * 100);
            $result->lossRate = 100 - $result->winRate;
        }
        return $results;
    }

    public function getAllByFilterWithPagination_bk($filter, $perPage)
    {
        return $this->model->filter($filter)->groupBy('customer_id')->orderBy('id', 'asc')->paginate($perPage);
    }

    public function getByCustomerId($filter, $customerId, $perPage)
    {
        return $this->model->filter($filter)->where('customer_id', $customerId)->orderBy('id', 'desc')->paginate($perPage);
    }

    public function getAllBatch()
    {
        return $this->model->groupBy('batch_no')->orderBy('batch_no', 'desc')->get();
    }

    public function getTotalAmountByWinStatus($status)
    {
        $data = $this->model->select('amount')->where('win_status', $status)->get();
        $total = 0;
        if(count($data) > 0){
            foreach($data as $value){
                $totalAmount[] = $value->amount;
            }
            $total =  array_sum($totalAmount);
        }
        return $total;
    }
}
