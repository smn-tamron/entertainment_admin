<?php

namespace App\Repositories\SlotGame;

use App\Repositories\BaseRepository;
use App\Model\SlotGame\SlotDailyReward;

class SlotDailyRewardRepository extends BaseRepository
{
    public function __construct(SlotDailyReward $model)
    {
        $this->model = $model;
    }
}
