<?php

namespace App\Repositories\SlotGame;

use App\Repositories\BaseRepository;
use App\Model\SlotGame\SlotReel;

class SlotReelRepository extends BaseRepository
{
    public function __construct(SlotReel $model)
    {
        $this->model = $model;
    }

    public function checkName($name, $id = null)
    {
        if ($id) {
            return $this->model->where('id', '<>', $id)->where('name', $name)->first();
        } else {
            return $this->model->where('name', $name)->first();
        }
    }
}
