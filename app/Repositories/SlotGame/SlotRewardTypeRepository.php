<?php

namespace App\Repositories\SlotGame;

use App\Repositories\BaseRepository;
use App\Model\SlotGame\SlotRewardType;

class SlotRewardTypeRepository extends BaseRepository
{
    public function __construct(SlotRewardType $model)
    {
        $this->model = $model;
    }

    public function checkRewardType($type, $id = null)
    {
        if ($id) {
            return $this->model->where('id', '<>', $id)->where('type', $type)->first();
        } else {
            return $this->model->where('type', $type)->first();
        }
    }
}
