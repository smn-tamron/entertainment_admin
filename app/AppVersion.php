<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppVersion extends Model
{
    protected $table = "app_versions";
    protected $guarded = [];
    public $timestamps = false;

    public function getForceUpdateIconAttribute()
    {
    	return $this->is_force_update == true ? "check-circle" : "times-circle"; 
    }
}
