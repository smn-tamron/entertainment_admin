<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\FacebookPageRepository;
use App\Services\FacebookPageService;

class FacebookPageServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            FacebookPageRepository::class,
            FacebookPageService::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
