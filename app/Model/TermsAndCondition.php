<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TermsAndCondition extends Model
{
    protected $table = "terms_and_conditions";
    
    protected $guarded = [];
}
