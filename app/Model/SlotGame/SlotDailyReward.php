<?php

namespace App\Model\SlotGame;

use Illuminate\Database\Eloquent\Model;

class SlotDailyReward extends Model
{
    protected $table = "daily_rewards";
    protected $guarded = [];
}