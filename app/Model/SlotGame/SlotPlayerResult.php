<?php

namespace App\Model\SlotGame;

use App\Model\Client\ClientUser;
use Illuminate\Database\Eloquent\Model;

class SlotPlayerResult extends Model
{
    protected $table = "slot_customer_results";
    protected $guarded = [];

    public function player()
    {
       return $this->belongsTo(ClientUser::class,'customer_id','id');
    }

    public function comboReward()
    {
       return $this->belongsTo(SlotComboReward::class,'slot_combo_reward_id','id');
    }

    public function scopeFilter($query,$filters)
    {
        $filters->apply($query);
    }
}