<?php

namespace App\Model\SlotGame;

use Illuminate\Database\Eloquent\Model;

class SlotRewardType extends Model
{
    protected $table = "reward_types";
    protected $guarded = [];
}
