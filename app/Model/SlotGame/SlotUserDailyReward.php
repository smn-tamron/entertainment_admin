<?php

namespace App\Model\SlotGame;

use App\Model\Client\ClientUser;
use Illuminate\Database\Eloquent\Model;

class SlotUserDailyReward extends Model
{
    protected $table = "user_daily_rewards";
    protected $guarded = [];

    public function player()
    {
       return $this->belongsTo(ClientUser::class,'client_user_id','id');
    }
}
