<?php

namespace App\Model\SlotGame;

use Illuminate\Database\Eloquent\Model;
class SlotBet extends Model
{
    protected $table = "slot_bet_types";
    protected $guarded = [];
    public $timestamps = false;

    public function currentTotal($betTypeId)
    {
        return SlotComboReward::where('slot_bet_type_id', $betTypeId)->sum('reward_coin');
    }
}
