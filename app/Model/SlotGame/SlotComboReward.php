<?php

namespace App\Model\SlotGame;

use Illuminate\Database\Eloquent\Model;

class SlotComboReward extends Model
{
    protected $table = "slot_combo_rewards";
    protected $guarded = [];
    public $timestamps = true;

    public function scopeFilter($query, $filters)
    {
        $filters->apply($query);
    }

    public function combo()
    {
       return $this->belongsTo(SlotCombo::class,'slot_combo_id','id');
    }

    public function bet()
    {
       return $this->belongsTo(SlotBet::class,'slot_bet_type_id','id');
    }

}