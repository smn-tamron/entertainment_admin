<?php

namespace App\Model\SlotGame;

use Illuminate\Database\Eloquent\Model;

class SlotReel extends Model
{
    protected $table = "slot_reels";
    protected $guarded = [];
    public $timestamps = false;
}
