<?php

namespace App\Model\SlotGame;

use Illuminate\Database\Eloquent\Model;

class SlotCombo extends Model
{
    protected $table = "slot_combos";
    protected $guarded = [];
    public $timestamps = false;

    public function firstReel()
    {
       return $this->belongsTo(SlotReel::class,'first_reel_id','id');
    }

    public function secondReel()
    {
       return $this->belongsTo(SlotReel::class,'second_reel_id','id');
    }

    public function thirdReel()
    {
       return $this->belongsTo(SlotReel::class,'third_reel_id','id');
    }
}
