<?php

namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Model;

class RoleHasSubMenu extends Model
{
    protected $table = "role_has_sub_menus";
    
    protected $guarded = [];

    public $timestamps = false;

    public function subMenu()
    {
        return $this->belongsTo(SubMenu::class,'sub_menu_id','id');
    }
}
