<?php

namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Model;

class RoleHasMainMenu extends Model
{
    protected $table = "role_has_main_menus";
    
    protected $guarded = [];

    public $timestamps = false;

    public function menu()
    {
        return $this->belongsTo(Menu::class,'menu_id','id');
    }
}
