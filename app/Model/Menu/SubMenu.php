<?php

namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    protected $table = "sub_menus";
    
    protected $guarded = [];

    public $timestamps = false;

    public function menu()
    {
        return $this->belongsTo(Menu::class,'menu_id','id');
    }
}
