<?php

namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "menus";
    
    protected $guarded = [];

    public $timestamps = false;

    public function subMenus()
    {
        return $this->hasMany(SubMenu::class,'menu_id','id');
    }
    
    public function scopeFilter($query,$filters)
    {
        $filters->apply($query);
    }

    public function roleHasSubMenus()
    {
        return $this->hasManyThrough(RoleHasSubMenu::class, SubMenu::class);
    }
}
