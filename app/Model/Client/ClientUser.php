<?php

namespace App\Model\Client;

use Illuminate\Database\Eloquent\Model;
use App\Model\SlotGame\SlotPlayerResult;

class ClientUser extends Model
{
    protected $table = "client_users";

    protected $guarded = [];

    public function getActiveStatusLabelColorAttribute()
    {
    	return $this->active_status == "active" ? "green" : "red";
    }

    public function getLoginTypeLabelIconAttribute()
    {
    	switch ($this->login_type) {
    		case 'email':
    			return "fa fa-envelope fa-lg text-gray";
    			break;
    		case 'phone':
    			return "fa fa-phone fa-lg text-green";
    			break;
            case 'google':
                return "fab fa-google fa-lg text-danger";
                break;
            case 'facebook':
                return "fab fa-facebook fa-lg text-primary";
                break;
            case 'apple':
                return "fab fa-apple fa-lg text-dark";
                break;	
    		default:
    			return "";
    			break;
    	}
    }

    public function scopeFilter($query,$filters)
    {
        $filters->apply($query);
    }

    public function clientPersonalInfo()
    {
        return $this->hasOne(ClientPersonalInformation::class, 'client_user_id','id');
    }

    public function slotResults()
    {
        return $this->hasMany(SlotPlayerResult::class, 'customer_id', 'id');
    }

    public function deviceTokens()
    {
        return $this->hasMany(ClientDeviceToken::class,'client_user_id','id');
    }


}
