<?php

namespace App\Model\Client;

use Illuminate\Database\Eloquent\Model;

class ClientDeviceToken extends Model
{
    protected $table = "client_device_tokens";
    protected $guarded = [];
    public $timestamps = false;

    public function user()
    {
    	return $this->belongsTo(ClientUser::class,'client_user_id','id');
    }
}
