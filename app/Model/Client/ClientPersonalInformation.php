<?php

namespace App\Model\Client;

use Illuminate\Database\Eloquent\Model;

class ClientPersonalInformation extends Model
{
    protected $table = "personal_information";

    protected $guarded = [];

    public function getGenderLabelIconAttribute()
    {
    	return ($this->gender == "male" ? "venus" : $this->gender == "female") ? "mars" :"genderless";
    }
}
