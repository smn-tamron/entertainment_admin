<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Model\Menu\RoleHasMainMenu;
use App\Model\Menu\RoleHasSubMenu;

class Role extends Model
{
    protected $table = "roles";
    
    protected $guarded = [];


    public function roleHasMainMenus()
    {
        return $this->hasMany(RoleHasMainMenu::class,'role_id');
    }

    public function roleHasSubMenus()
    {
        return $this->hasMany(RoleHasSubMenu::class,'role_id');
    }
}
