<?php

use Illuminate\Database\Seeder;

class SubMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_menus')->insert(
            array(
                0 =>
                array(
                        'id' => 1,
                        'menu_id' => 1,
                        'name' => 'Post',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/news/posts'
                ),
                1 =>
                array(
                        'id' => 2,
                        'menu_id' => 1,
                        'name' => 'Media',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/news/medias'
                ),
                2 =>
                array(
                        'id' => 3,
                        'menu_id' => 1,
                        'name' => 'Setting',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/news/setting/categories'
                ),
                3 =>
                array(
                        'id' => 4,
                        'menu_id' => 2,
                        'name' => 'Price',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/goldprices/rates'
                ),
                4=>
                array(
                        'id' => 5,
                        'menu_id' => 2,
                        'name' => 'Setting',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/goldprices/setting/types'
                ),
                5=>
                array(
                        'id' => 6,
                        'menu_id' => 3,
                        'name' => 'Bank',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/exchangeRates/banks'
                ),
                6=>
                array(
                        'id' => 7,
                        'menu_id' => 3,
                        'name' => 'Currency',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/exchangeRates/currencies'
                ),
                7=>
                array(
                        'id' => 8,
                        'menu_id' => 3,
                        'name' => 'Rate',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/exchangeRates/currency/rates'
                ),
                8=>
                array(
                        'id' => 9,
                        'menu_id' => 4,
                        'name' => 'Price',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/fuelprices/rates'
                ),
                9=>
                array(
                        'id' => 10,
                        'menu_id' => 4,
                        'name' => 'Provider',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/fuelprices/providers'
                ),
                10=>
                array(
                        'id' => 11,
                        'menu_id' => 5,
                        'name' => 'Price',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/internetprices/rates'
                ), 
                11=>
                array(
                        'id' => 12,
                        'menu_id' => 5,
                        'name' => 'Setting',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/internetprices/setting/serviceproviders'
                ), 
                12=>
                array(
                        'id' => 13,
                        'menu_id' => 6,
                        'name' => 'Post',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/newsfeeds/posts'
                ), 
                13=>
                array(
                        'id' => 14,
                        'menu_id' => 6,
                        'name' => 'Report',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/newsfeeds/reports/posts'
                ), 
                14=>
                array(
                        'id' => 15,
                        'menu_id' => 7,
                        'name' => 'Category',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/directory/categories'
                ), 
                15=>
                array(
                        'id' => 16,
                        'menu_id' => 7,
                        'name' => 'Service',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/directory/services'
                ), 
                16=>
                array(
                        'id' => 17,
                        'menu_id' => 12 ,
                        'name' => 'Batch',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/lottery/batches/standard-batches'
                ), 
                17=>
                array(
                        'id' => 18,
                        'menu_id' => 12,
                        'name' => 'Prize',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/lottery/prizes/standard-prizes'
                ), 
                18=>
                array(
                        'id' => 19,
                        'menu_id' => 15,
                        'name' => 'Gate',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/carGates/gates'
                ), 
                19=>
                array(
                        'id' => 20,
                        'menu_id' => 15,
                        'name' => 'City',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/carGates/cities'
                ),
                20=>
                array(
                        'id' => 21,
                        'menu_id' => 16,
                        'name' => 'Brand New Car',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/brand-new-cars/cars'
                ), 
                21=>
                array(
                        'id' => 22,
                        'menu_id' => 16,
                        'name' => 'Car Brand',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/brand-new-cars/brands'
                ), 
                22=>
                array(
                        'id' => 23,
                        'menu_id' => 16,
                        'name' => 'Specification Category',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/brand-new-cars/specification-categories'
                ), 
                23=>
                array(
                        'id' => 24,
                        'menu_id' => 17,
                        'name' => 'Bus',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/yangon-buses/bus-lines'
                ), 
                24=>
                array(
                        'id' => 25,
                        'menu_id' => 17,
                        'name' => 'Bus Stop',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/yangon-buses/bus-stops'
                ), 
                25=>
                array(
                        'id' => 26,
                        'menu_id' => 18,
                        'name' => 'Order',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/aircon/orders'
                ), 
                26=>
                array(
                        'id' => 27,
                        'menu_id' => 18,
                        'name' => 'Voucher',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/aircon/vouchers'
                ), 
                27=>
                array(
                        'id' => 28,
                        'menu_id' => 18,
                        'name' => 'Setting',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/aircon/setting/service'
                ), 
                28=>
                array(
                        'id' => 29,
                        'menu_id' => 19,
                        'name' => 'Order',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/houseKeeping/orders'
                ), 
                29=>
                array(
                        'id' => 30,
                        'menu_id' => 19,
                        'name' => 'Voucher',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/houseKeeping/vouchers'
                ), 
                30=>
                array(
                        'id' => 31,
                        'menu_id' => 19,
                        'name' => 'Setting',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/houseKeeping/setting/service'
                ), 
                31=>
                array(
                        'id' => 32,
                        'menu_id' => 20,
                        'name' => 'User',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/vendors/users'
                ), 
                32=>
                array(
                        'id' => 33,
                        'menu_id' => 20,
                        'name' => 'Group',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/vendors/groups'
                ), 
                33=>
                array(
                        'id' => 34,
                        'menu_id' => 20,
                        'name' => 'Vendor',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/vendors/vendors'
                ), 
                34=>
                array(
                        'id' => 35,
                        'menu_id' => 21,
                        'name' => 'Post',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/shoppingfeeds/posts'
                ), 
                35=>
                array(
                        'id' => 36,
                        'menu_id' => 21,
                        'name' => 'Report',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/shoppingfeeds/reports/posts'
                ), 
                36=>
                array(
                        'id' => 37,
                        'menu_id' => 22,
                        'name' => 'Post',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/job/posts'
                ), 
                37=>
                array(
                        'id' => 38,
                        'menu_id' => 22,
                        'name' => 'Report',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/job/reports/posts'
                ), 
                38=>
                array(
                        'id' => 39,
                        'menu_id' => 23,
                        'name' => 'App Version',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/setting/appversions'
                ), 
                39=>
                array(
                        'id' => 40,
                        'menu_id' => 23,
                        'name' => 'Client User',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/setting/client/users'
                ), 
                40=>
                array(
                        'id' => 41,
                        'menu_id' => 23,
                        'name' => 'Slider Image',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/setting/home-image-sliders'
                ), 
                41=>
                array(
                        'id' => 42,
                        'menu_id' => 23,
                        'name' => 'Township',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/setting/townships'
                ), 
                42=>
                array(
                        'id' => 43,
                        'menu_id' => 23,
                        'name' => 'Point Setting',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/setting/points'
                ), 
                43=>
                array(
                        'id' => 44,
                        'menu_id' => 23,
                        'name' => 'Schedule Setting',
                        'icon' => 'far fa-circle fa-sm',
                        'route' => '/setting/schedules'
                )
            )
        );
    }
}
