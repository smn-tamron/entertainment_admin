<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert(
            array(
                0 =>
                array(
                        'id'    => 1,
                        'name'  => 'News',
                        'icon'  => 'far fa-fw fa-newspaper fa-lg',
                        'route'  => null
                ),
                1 =>
                array(
                        'id' => 2,
                        'name' => 'Gold',
                        'icon'  => 'fas fa-coins fa-lg',
                        'route'  => null
                ),
                2 =>
                array(
                        'id' => 3,
                        'name' => 'Currency Exchange',
                        'icon'  => 'fas fa-money-bill-alt fa-lg',
                        'route'  => null
                ),
                3 =>
                array(
                        'id' => 4,
                        'name' => 'Fuel',
                        'icon'  => 'fas fa-gas-pump fa-lg',
                        'route'  => null
                ),
                4=>
                array(
                        'id' => 5,
                        'name' => 'Internet',
                        'icon'  => 'fa fa-wifi fa-lg',
                        'route'  => null
                ),
                5=>
                array(
                        'id' => 6,
                        'name' => 'News Feed',
                        'icon'  => 'fa fa-globe fa-lg',
                        'route'  => null
                ),
                6=>
                array(
                        'id' => 7,
                        'name' => 'Directory',
                        'icon'  => 'fa fa-question fa-lg',
                        'route'  => null
                ),
                7=>
                array(
                        'id' => 8,
                        'name' => 'Holiday',
                        'icon'  => 'fa fa-calendar-alt fa-lg',
                        'route'  => '/holidays'
                ),
                8=>
                array(
                        'id' => 9,
                        'name' => 'Travel Blog',
                        'icon'  => 'fas fa-suitcase fa-lg',
                        'route'  => '/travel-blogs'
                ),
                9=>
                array(
                        'id' => 10,
                        'name' => 'Food Blog',
                        'icon'  => 'fas fa-utensil-spoon fa-lg',
                        'route'  => '/food-blogs'
                ),
                10=>
                array(
                        'id' => 11,
                        'name' => 'Postal Code',
                        'icon'  => 'fa fa-map fa-lg',
                        'route'  => '/postal-codes'
                ),
                11=>
                array(
                        'id' => 12,
                        'name' => 'Lottery',
                        'icon'  => 'fa fa-trophy fa-lg',
                        'route'  => null
                ),
                12=>
                array(
                        'id' => 13,
                        'name' => 'Real Estate',
                        'icon'  => 'fa fa-building fa-lg',
                        'route'  => '/real-estates'
                ),
                13=>
                array(
                        'id' => 14,
                        'name' => 'Car Rental',
                        'icon'  => 'fa fa fa-car fa-lg',
                        'route'  => '/car-rentals',
                ),
                14=>
                array(
                        'id' => 15,
                        'name' => 'Car Gate',
                        'icon'  => 'fas fa-bus-alt fa-lg',
                        'route'  => null
                ),
                15=>
                array(
                        'id' => 16,
                        'name' => 'Brand New Car',
                        'icon'  => 'fas fa-car-alt fa-lg',
                        'route'  => null
                ),
                16=>
                array(
                        'id' => 17,
                        'name' => 'Yangon Bus',
                        'icon'  => 'fas fa-bus-alt fa-lg',
                        'route'  => null
                ),
                17=>
                array(
                        'id' => 18,
                        'name' => 'Aircon',
                        'icon'  => 'fas fa-fan fa-lg',
                        'route'  => null
                ),
                18=>
                array(
                        'id' => 19,
                        'name' => 'House Keeping',
                        'icon'  => 'fas fa-broom fa-lg',
                        'route'  => null
                ),
                19=>
                array(
                        'id' => 20,
                        'name' => 'Vendor',
                        'icon'  => 'fas fa-object-group fa-lg',
                        'route'  => null
                ),
                20=>
                array(
                        'id' => 21,
                        'name' => 'Shopping Feed',
                        'icon'  => 'fa fa-shopping-cart fa-lg',
                        'route'  => null
                ),
                21=>
                array(
                        'id' => 22,
                        'name' => 'Job',
                        'icon'  => 'fas fa-briefcase fa-lg',
                        'route'  => null
                ),
                22=>
                array(
                        'id' => 23,
                        'name' => 'Setting',
                        'icon'  => 'fa fa-cogs fa-lg',
                        'route'  => null
                )
            )
        );
    }
}
