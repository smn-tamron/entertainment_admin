<?php

use Illuminate\Database\Seeder;
use App\Model\SlotGame\SlotCombo;

class SlotComboSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reel1 = [1,2,3,4,5,6];
        $reel2 = [1,2,3,4,6];
        $reel3 = [3,2,1,6];

        for($i = 0; $i < count($reel1); $i++) {
            for($j = 0; $j < count($reel2); $j++) {
                for($k = 0; $k < count($reel3); $k++) {
                    SlotCombo::create([
                        'first_reel_id' => $reel1[$i],
                        'second_reel_id' => $reel2[$j],
                        'third_reel_id' => $reel3[$k],
                    ]);
                }
            }
        }
    }
}
