<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDailyRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_daily_rewards', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('client_user_id');
            $table->integer('day_no');
            $table->integer('week_no');
            $table->integer('current_year');
            $table->integer('claim_coin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_daily_rewards');
    }
}
