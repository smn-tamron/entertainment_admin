<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlotCustomerResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slot_customer_results', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('batch_no');
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('slot_combo_reward_id');
            $table->boolean('win_status')->default(0);
            $table->double('amount')->nullable();
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slot_customer_results');
    }
}
