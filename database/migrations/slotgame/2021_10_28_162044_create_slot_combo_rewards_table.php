<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlotComboRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slot_combo_rewards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('slot_bet_type_id');
            $table->unsignedBigInteger('slot_combo_id');
            $table->double('reward_coin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slot_combo_rewards');
    }
}
