<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWinTypeToSlotComboRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('slot_combo_rewards', function (Blueprint $table) {
            $table->tinyInteger('win_type')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slot_combo_rewards', function (Blueprint $table) {
            $table->dropColumn('win_type');
        });
    }
}
