<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlotCombosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slot_combos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('first_reel_id');
            $table->unsignedBigInteger('second_reel_id');
            $table->unsignedBigInteger('third_reel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slot_combos');
    }
}
