<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('assets/{directoryPath?}', 'ImageController@getAsset')->where('directoryPath', '(.*)');

    Route::get('/', function () {
        return redirect()->route('login');
    });

    Route::post('check', ['as' => 'users.check', 'uses' => 'UserController@checkUser']);

    Auth::routes(['register' => false, 'reset' => false]);

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);
        Route::post('users/change-password', ['as' => 'users.change_password', 'uses' => 'UserController@changePassword']);

        //SuperAdmin
        Route::group(['middleware' => ['superadmin']], function () {
            Route::group(['prefix' => 'users'], function () {
                Route::get('/', ['as' => 'users.index', 'uses' => 'UserController@index']);
                Route::post('/store', ['as' => 'users.store', 'uses' => 'UserController@store']);
                Route::patch('/update', ['as' => 'users.update', 'uses' => 'UserController@update']);
                Route::delete('/delete', ['as' => 'users.delete', 'uses' => 'UserController@destroy']);
            });

            Route::group(['prefix' => 'menus'], function () {
                Route::get('/', ['as' => 'menus.index', 'uses' => 'MenuController@index']);
                Route::post('/sort', ['as' => 'menus.sort', 'uses' => 'MenuController@sort']);
                Route::get('/{id}/submenus', ['as' => 'menus.submenus', 'uses' => 'MenuController@show']);
                Route::post('/save', ['as' => 'menus.save', 'uses' => 'MenuController@store']);
                Route::put('/update', ['as' => 'menus.update', 'uses' => 'MenuController@update']);
                Route::delete('/delete', ['as' => 'menus.delete', 'uses' => 'MenuController@destroy']);

                Route::group(['prefix' => 'submenus'], function () {
                    Route::post('/sort', ['as' => 'submenus.sort', 'uses' => 'SubmenuController@sort']);
                    Route::post('/', ['as' => 'submenus.save', 'uses' => 'SubmenuController@store']);
                    Route::put('/update', ['as' => 'submenus.update', 'uses' => 'SubmenuController@update']);
                    Route::delete('/delete', ['as' => 'submenus.delete', 'uses' => 'SubmenuController@destroy']);
                });
            });

            Route::group(['prefix' => 'role'], function () {
                Route::get('/', ['as' => 'role.index', 'uses' => 'RoleController@index']);
                Route::get('/create', ['as' => 'role.create', 'uses' => 'RoleController@create']);
                Route::post('/save', ['as' => 'role.save', 'uses' => 'RoleController@store']);
                Route::get('/{id}/edit', ['as' => 'role.edit', 'uses' => 'RoleController@edit']);
                Route::patch('/update', ['as' => 'role.update', 'uses' => 'RoleController@update']);
                Route::delete('/delete', ['as' => 'role.delete', 'uses' => 'RoleController@destroy']);
            });
        });

        // //permission
        Route::group(['middleware' => ['permission']], function () {

            Route::group(['prefix' => 'client-users', 'namespace' => 'Client'], function () {
                Route::get('/', ['as' => 'client.users.index', 'uses' => 'ClientUserController@index']);
                Route::get('/statusChange', ['as' => 'client.users.statuschange', 'uses' => 'ClientUserController@bannedStatusChange']);
                Route::get('{id}', ['as' => 'client.users.show', 'uses' => 'ClientUserController@show']);
            });

            Route::group(['prefix' => 'slot-game', 'namespace' => 'SlotGame'], function () {
                Route::group(['prefix' => 'slot-reels'], function () {
                    Route::get('/', ['as' => 'slotReel.index', 'uses' => 'SlotReelController@index']);
                    Route::post('/save', ['as' => 'slotReel.save', 'uses' => 'SlotReelController@save']);
                    Route::put('/update', ['as' => 'slotReel.edit', 'uses' => 'SlotReelController@update']);
                    Route::delete('/delete', ['as' => 'slotReel.delete', 'uses' => 'SlotReelController@delete']);
                });

                Route::group(['prefix' => 'slot-bets'], function () {
                    Route::get('/', ['as' => 'slotBet.index', 'uses' => 'SlotBetController@index']);
                    Route::post('/save', ['as' => 'slotBet.save', 'uses' => 'SlotBetController@save']);
                    Route::put('/update', ['as' => 'slotBet.edit', 'uses' => 'SlotBetController@update']);
                    Route::delete('/delete', ['as' => 'slotBet.delete', 'uses' => 'SlotBetController@delete']);
                });

                Route::group(['prefix' => 'slot-combo-rewards'], function () {
                    Route::get('/', ['as' => 'slotComboReward.index', 'uses' => 'SlotComboRewardController@index']);
                    Route::post('/save', ['as' => 'slotComboReward.save', 'uses' => 'SlotComboRewardController@save']);
                    Route::put('/update', ['as' => 'slotComboReward.edit', 'uses' => 'SlotComboRewardController@update']);
                    Route::delete('/delete', ['as' => 'slotComboReward.delete', 'uses' => 'SlotComboRewardController@delete']);
                });

                Route::group(['prefix' => 'slot-reward-types'], function () {
                    Route::get('/', ['as' => 'slotRewardType.index', 'uses' => 'SlotRewardTypeController@index']);
                    Route::post('/save', ['as' => 'slotRewardType.save', 'uses' => 'SlotRewardTypeController@save']);
                    Route::put('/update', ['as' => 'slotRewardType.edit', 'uses' => 'SlotRewardTypeController@update']);
                    Route::delete('/delete', ['as' => 'slotRewardType.delete', 'uses' => 'SlotRewardTypeController@delete']);
                });

                Route::group(['prefix' => 'slot-daily-rewards'], function () {
                    Route::get('/', ['as' => 'dailyReward.index', 'uses' => 'SlotDailyRewardController@index']);
                    Route::post('/save', ['as' => 'dailyReward.save', 'uses' => 'SlotDailyRewardController@save']);
                    Route::put('/update', ['as' => 'dailyReward.edit', 'uses' => 'SlotDailyRewardController@update']);
                    Route::delete('/delete', ['as' => 'dailyReward.delete', 'uses' => 'SlotDailyRewardController@delete']);
                });

                Route::group(['prefix' => 'terms-and-conditions'], function () {
                    Route::get('/', ['as' => 'slotTermsAndCondition.index', 'uses' => 'SlotTermsAndConditionController@index']);
                    Route::post('/save', ['as' => 'slotTermsAndCondition.save', 'uses' => 'SlotTermsAndConditionController@save']);
                    Route::put('/update', ['as' => 'slotTermsAndCondition.update', 'uses' => 'SlotTermsAndConditionController@update']);
                });

                Route::group(['prefix' => 'player-results'], function () {
                    Route::get('/', ['as' => 'playerResult.index', 'uses' => 'SlotPlayerResultController@index']);
                    Route::get('/{id}', ['as' => 'playerResult.detail', 'uses' => 'SlotPlayerResultController@show']);
                });
            });
        });
    });
